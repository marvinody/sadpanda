(function () {
	"use strict";
    /*jslint plusplus: true */

    var time = 0;

    function formatTime() {
        var responce = '',
            hours = 0,
            mins = 0,
            secs = time;

        while (secs >= 60) {
            secs -= 60;
            mins += 1;
        }

        while (mins >= 60) {
            mins -= 60;
            hours += 1;
        }

        if (secs === 0) {
            secs = '';
        } else if (secs === 1) {
            secs += ' second';
        } else if (secs > 1) {
            secs += ' seconds';
        }

        if (mins === 0) {
            mins = '';
        } else if (mins === 1) {
            mins += ' minute';
        } else if (mins > 1) {
            mins += ' minutes';
        }

        if (hours === 0) {
            hours = '';
        } else if (hours === 1) {
            hours += ' hour';
        } else if (hours > 1) {
            hours += ' hours';
        }

        if (hours !== '') {
            responce += hours + ' ';
        }

        if (mins !== '') {
            responce += mins + ' ';
        }

        if (secs !== '') {
            responce += secs;
        }

        return responce;
    }

    function timer() {
        time++;
        var out = formatTime();
        document.querySelector("span.time").textContent = out;
        window.setTimeout(
            function () {
                timer();
            }, 1000);
    }

    
    function volumeStuff() {
        var volumeButton = document.getElementsByClassName("volume-button")[0];
        
        //document.getElementById("temp-button").addEventListener("click",randomVolLoc);
        var style = document.createElement("style");style.type = "text/css";
        document.getElementsByTagName('head')[0].appendChild(style);
        dragThing({
            element:volumeButton,
            dragTolerance:10,
            drag:function(e){
                var bb = volumeButton.getBoundingClientRect();
                //console.log("("+(e.x-bb.left)+","+(e.y-bb.top)+")");
                changeVolume(document.querySelector("video"),e.x-bb.left,bb.width,style);
            },
            mousedown:function(e){
                var bb = volumeButton.getBoundingClientRect();
                //console.log("("+(e.x-bb.left)+","+(e.y-bb.top)+")");
                changeVolume(document.querySelector("video"),e.x-bb.left,bb.width,style);
            },
        });
    
    }
    
    function changeVolume(player, rmX, maxWidth,style){
        //relative mouse x (relative to volume window)
        var pixPerBar = maxWidth/11;
        var divs = Math.floor(rmX/pixPerBar);
        var volume = divs/10;
        player.volume = volume;
        
        while(style.hasChildNodes()){style.removeChild(style.lastChild);}
        style.appendChild(document.createTextNode(".volume-button div:nth-child(n+"+(divs+2)+"){background-color:grey;}"))
    
    }
    
    function dragThing(options){
        var dragTolerancePixels = options.dragTolerance;
        var ele = options.element;
        var mouseDown = false,
            mouseCoords = [0,0],
            isDragging = false;
        ele.addEventListener("mousedown",function(e){
            mouseDown = true;
            mouseCoords[0] = e.clientX;
            mouseCoords[1] = e.clientY;
            options.mousedown(e);
        });
        ele.addEventListener("mouseup",function(){
            mouseDown = false;
            isDragging = false;
        });
        ele.addEventListener("mouseleave",function(){
            mouseDown = false;
            isDragging = false;
        });
        ele.addEventListener("mousemove", function (e) {
            if(mouseDown){
                var distanceTraveled = Math.pow(mouseCoords[0]-e.clientX,2)+Math.pow(mouseCoords[1]-e.clientY,2)
                if(isDragging || distanceTraveled > Math.pow(dragTolerancePixels,2)){
                    isDragging = true;
                    options.drag(e);
                } else {
                    //move
                    ;
                }
            } else {
                //move
                ;
            }
        });
    }
    
    window.onload = function(){
        timer();
        
        volumeStuff();
    };
    //document.addEventListener("DOMContentLoaded", timer);
    
    
}());

    
    function randomVolLoc(){
        var volumeButton = document.getElementsByClassName("volume-button")[0];
        var vbBB = volumeButton.getBoundingClientRect();
        var maxLeft = window.innerWidth - vbBB.width;
        var maxTop = window.innerHeight - vbBB.height;
        if(maxLeft < 0 || maxTop < 0){return;}
        
        volumeButton.style.left = Math.round(Math.random()*maxLeft) + "px";
        volumeButton.style.top = Math.round(Math.random()*maxTop) + "px";
    }