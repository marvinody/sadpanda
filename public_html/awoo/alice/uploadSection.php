<?php
if (isset($_POST['key'])){
    $key = "charlie";
    $SHORT_URL_LENGTH = 8;
    $maxFileSize = 5*1024*1024;

    try{
        if (!!empty($_POST['key'])){
            throw new RuntimeException("No Key Supplied");
        }
        
        if ($_POST['key'] !== $key ){
            throw new RuntimeException("Wrong key entered");
        }
        if (!!empty($_POST['title'])){
            throw new RuntimeException("No Title Supplied");
        }
        if (!!empty($_POST['uploader'])){
            throw new RuntimeException("No Username Supplied");
        }
        if (!isset($_FILES['userfile'])){
            throw new RuntimeException("Try Uploading a file");
        }
        if (!isset($_POST['section'])){
            throw new RuntimeException("No Section Supplied");
        }
        
        
        
        $myFile = $_FILES['userfile'];
        $fileCount = count($myFile["name"]);
        $possExts = array(
                'jpg' => 'image/jpeg',
                'png' => 'image/png',
                'gif' => 'image/gif',
                'txt' => 'text/plain',
        );
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $gid = 0;
        $id = 0;
        $section = $_POST['section'];
        $pages = [];
        for ($i = 0; $i < $fileCount; $i++) {
            #$myFile["name"][$i]
            #$myFile["tmp_name"][$i]
            switch ($myFile['error'][$i]) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }
            if ($myFile['size'][$i] > $maxFileSize ) {
                throw new RuntimeException('Exceeded filesize limit.');
            }
            
            $mime = $finfo->file($myFile['tmp_name'][$i]);
            
            $ext = array_search(
                $mime,
                $possExts,
                true
            );
            if (false === $ext) {
                throw new RuntimeException('Invalid file format.');
            }
            $sha = sha1_file($myFile['tmp_name'][$i]);
            $b64sha = base64_encode ($sha);
            if ($section == 0 && $i == 0){
                $gid = substr(hexdec(substr($sha,0,$SHORT_URL_LENGTH)),0,$SHORT_URL_LENGTH);
            } else {
                if ($section != 0){
                    $gid = $_POST['gid'];
                }
            }
            $uploaded_file = sprintf('%s/%s,%s.%s',
                IMAGE_DIR,
                $gid,
                $b64sha,
                $ext
            );
            
            if (!move_uploaded_file(
                $myFile['tmp_name'][$i],
                $uploaded_file
            )) {
                throw new RuntimeException('Failed to move uploaded file.');
            } else {
                $short_id = substr($b64sha,0,$SHORT_URL_LENGTH);
                
                
                
                $width = 0;
                $height = 0;
                $newWidth = 0;
                $newHeight = 0;
                if ($ext === "jpg"){
                    list($width,$height) = getimagesize($uploaded_file);
                    
                    $thumbnail = sprintf('%s/%s,%s.%s',
                        THUMBNAIL_DIR,
                        $gid,
                        $b64sha,
                        $ext
                    );
                    
                    //generate thumbnails for later
                    $myImage = imagecreatefromjpeg($uploaded_file);
                    $newWidth = 200;
                    $newHeight = round(200*$height/$width);
                    
                    $image_p = imagecreatetruecolor($newWidth, $newHeight);
                    imagecopyresampled($image_p, $myImage, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
                    imagejpeg($image_p, $thumbnail, 100);
                    
                    
                }
                
                $pages[] = array(
                    'shortID'=>$short_id,
                    'page'=>($i+1),
                    'name'=>$myFile['name'][$i],
                    'width'=>$width,
                    'height'=>$height,
                    'thumb_width'=>$newWidth,
                    'thumb_height'=>$newHeight,
                    
                );
                
            }
            
            
        }
        $data = array(
            'gid'=>$gid,
            'pages'=>$pages,
            'uploader'=>$_POST['uploader'],
            'name'=>$_POST['title'],
            'cover_thumb'=>$pages[0]['shortID'],
            
        );
        
        /*MYSQL PART*/
        include 'config.php';
        mysqli_report(MYSQLI_REPORT_STRICT);
        if (!isset($_POST['id'])){
            $stmt=$mysqli->prepare("INSERT INTO ".DB_TABLE_ALBUMS."(name,gid,uploader,cover_thumb) VALUES (?,?,?,?)");
            if(!($stmt->bind_param('siss',$data['name'],$data['gid'],$data['uploader'],$data['cover_thumb']))){
                echo (htmlspecialchars($stmt->error));
            }
            if (!($stmt->execute() )){
                echo (htmlspecialchars($stmt->error));
            }
            $id = $stmt->insert_id;
            $stmt->close();
        } else {
            $id = $_POST['id'];
        }
        
        
        $numPages = count($pages);
        $stmt=$mysqli->prepare("INSERT INTO ".DB_TABLE_PAGES."(gid,shortID,name,page,width,height,thumb_width,thumb_height) VALUES (?,?,?,?,?,?,?,?)");
        $shortID = 'a1';
        $page = 0;
        $name = '';
        $width = 0;
        $height = 0;
        $thumb_width = 0;
        $thumb_height = 0;
        if(!($stmt->bind_param('issiiiii',$data['gid'],$shortID,$name,$page,$width,$height,$thumb_width,$thumb_height))){
            echo (htmlspecialchars($stmt->error));
        }
        for ($i = 0;$i<$numPages;$i++){
            $shortID = $data['pages'][$i]['shortID'];
            $name = $data['pages'][$i]['name'];
            $page = $data['pages'][$i]['page']+$section*10;
            $width = $data['pages'][$i]['width'];
            $height = $data['pages'][$i]['height'];
            $thumb_width = $data['pages'][$i]['thumb_width'];
            $thumb_height = $data['pages'][$i]['thumb_height'];
            
            
                if (!($stmt->execute() )){
                echo (htmlspecialchars($stmt->error));
            }
        }
        
        header('Content-Type: application/json');
        print_r(json_encode($data));
        
    } catch (mysqli_sql_exception $e){
        if ($debug){
            echo $e->errorMessage();
        }
    } catch (RuntimeException $e) {
        echo $e->getMessage();
    }

    
    
}

?>