function api_call(b, a, c) {
    b.open("POST", base_url + "api.php");
    b.setRequestHeader("Content-Type", "application/json");
    b.onreadystatechange = c;
    b.send(JSON.stringify(a))
}

function api_response(b) {
    if (b.readyState == 4) {
        if (b.status == 200) {
            var a = JSON.parse(b.responseText);
            if (a.login != undefined) {
                top.location.href = login_url
            } else {
                return a
            }
        } else {
            alert("Server communication failed: " + b.status + " (" + b.responseText + ")")
        }
    }
    return false
}

function hookEvent(b, a, c) {
    if (typeof(b) == "string") {
        b = document.getElementById(b)
    }
    if (b == null) {
        return
    }
    if (b.addEventListener) {
        if (a == "mousewheel") {
            b.addEventListener("DOMMouseScroll", c, false)
        }
        b.addEventListener(a, c, false)
    } else {
        if (b.attachEvent) {
            b.attachEvent("on" + a, c)
        }
    }
}

function cancelEvent(a) {
    a = a ? a : window.event;
    if (a.stopPropagation) {
        a.stopPropagation()
    }
    if (a.preventDefault) {
        a.preventDefault()
    }
    a.cancelBubble = true;
    a.cancel = true;
    a.returnValue = false;
    return false
}
if (typeof KeyEvent == "undefined") {
    var KeyEvent = {
        DOM_VK_SPACE: 32,
        DOM_VK_PAGE_UP: 33,
        DOM_VK_PAGE_DOWN: 34,
        DOM_VK_LEFT: 37,
        DOM_VK_UP: 38,
        DOM_VK_RIGHT: 39,
        DOM_VK_DOWN: 40,
        DOM_VK_A: 65,
        DOM_VK_D: 68,
        DOM_VK_S: 83,
        DOM_VK_W: 87,
        DOM_VK_NUMPAD2: 98,
        DOM_VK_NUMPAD4: 100,
        DOM_VK_NUMPAD6: 102,
        DOM_VK_NUMPAD8: 104
    }
}
var currentpage = 1;
var currentthumb = 1;
var pane_outer = document.getElementById("pane_outer");
var pane_thumbs = document.getElementById("pane_thumbs");
var pane_images = document.getElementById("pane_images");
var pane_images_inner = document.getElementById("pane_images_inner");

function do_mousescroll(c, a) {
    a = a ? a : window.event;
    var b = (a.detail ? a.detail * -1 : a.wheelDelta / 40) * 20;
    scroll_relative(c, b);
    cancelEvent(a)
}
var touch_start = -1;
var touch_offset = -1;

function do_touchstart(a) {
    if (touch_start == -1) {
        if (a.touches.length > 0) {
            touch_start = a.touches[0].clientY;
            touch_offset = touch_start
        }
    }
    cancelEvent(a)
}

function do_touchmove(a) {
    if (touch_start > -1) {
        if (a.touches.length > 0) {
            scroll_relative("pane_images", (a.touches[0].clientY - touch_offset) * 2);
            touch_offset = a.touches[0].clientY
        }
    }
    cancelEvent(a)
}

function do_touchend(a) {
    touch_start = -1;
    cancelEvent(a)
}

function scroll_relative(b, a) {
    document.getElementById(b).scrollTop -= a
}

function scroll_absolute(b, a) {
    document.getElementById(b).scrollTop = a
}

function sync_thumbpane() {
    if (currentpage != currentthumb) {
        var b = document.getElementById("thumb_" + Math.min(pagecount, currentpage + 1));
        var a = Math.max(0, Math.round(b.offsetTop - (pane_thumbs.offsetHeight - b.offsetHeight) / 2));
        scroll_absolute("pane_thumbs", a);
        currentthumb = currentpage
    }
}

function load_image_dispatch(b) {
    var a = api_response(imagelist[b - 1].xhr);
    if (a != false) {
        if (a.error != undefined) {
            alert("Error loading image: " + a.error)
        } else {
            imagelist[b - 1].i = a.i;
            imagelist[b - 1].s = a.s;
            imagelist[b - 1].d = a.d;
            imagelist[b - 1].o = a.o;
            imagelist[b - 1].ll = a.ll;
            imagelist[b - 1].ls = a.ls;
            imagelist[b - 1].lf = a.lf;
            imagelist[b - 1].lo = a.lo;
            imagelist[b - 1].xres = a.xres;
            imagelist[b - 1].yres = a.yres;
            load_image(b)
        }
        imagelist[b - 1].xhr = undefined
    }
}

function action_gallery() {
    document.location = gallery_url
}

function action_fullimg(a) {
    document.location = base_url + imagelist[a - 1].lf
}

function action_set(a) {
    document.location = mpv_url + "?inline_set=" + a
}

function action_reload(b) {
    if (imagelist[b - 1].i != undefined) {
        imagelist[b - 1].i = undefined;
        imagelist[b - 1].xhr = new XMLHttpRequest();
        var a = {
            method: "imagedispatch",
            gid: gid,
            page: b,
            imgkey: imagelist[b - 1].k,
            mpvkey: mpvkey,
            nl: imagelist[b - 1].s
        };
        api_call(imagelist[b - 1].xhr, a, function() {
            load_image_dispatch(b)
        })
    }
}

function action_search(a) {
    window.open(base_url + imagelist[a - 1].ls, "_ehsearch")
}

function action_link(a) {
    prompt("Copy the URL below.", base_url + "r/" + imagelist[a - 1].ll)
}

function rescale_image(c, b) {
    var i = Math.max(700, window.innerWidth - (hide_thumbbar ? 5 : 230));
    var h = parseInt(imagelist[c - 1].xres);
    var a = parseInt(imagelist[c - 1].yres);
    var d = h;
    var g = a;
    if (always_scale || (d > i)) {
        g = Math.round(g * i / d);
        d = i
    }
    var e = 0;
    if (b.offsetTop + b.offsetHeight < pane_images.scrollTop) {
        var f = parseInt(b.style.height.replace(/px/, ""));
        e += f - g
    }
    b.style.width = d + "px";
    b.style.height = g + "px";
    document.getElementById("image_" + c).style.height = (g + 24) + "px";
    return {
        view_height: g,
        scroll_offset: e
    }
}

function load_image(b) {
    if (imagelist[b - 1].i != undefined) {
        var c = '<a href="#page' + (b + 1) + '"><img id="imgsrc_' + b + '" src="' + imagelist[b - 1].i + '" title="' + imagelist[b - 1].n + '" style="margin:0; width:' + imagelist[b - 1].xres + "px; height:" + imagelist[b - 1].yres + 'px" /></a> <div class="mi1"> 	<div class="mi2"> 		' + (imagelist[b - 1].o == "org" ? '<img style="cursor:default" title="Original Image" src="' + img_url + 'mpvd_d.png" />' : '<img title="' + imagelist[b - 1].o + '" onclick="action_fullimg(' + b + ')" src="' + img_url + 'mpvd.png" />') + ' 		<img title="Reload broken image" onclick="action_reload(' + b + ')" src="' + img_url + 'mpvr.png" /> 	</div> 	<div class="mi3"> 		<a href="' + base_url + imagelist[b - 1].lo + '" target="_ehshow_' + gid + "_" + b + '"><img title="Open image in normal viewer" onclick="action_open(' + b + ')" src="' + img_url + 'mpvn.png" /></a> 		<img title="Show galleries with this image" onclick="action_search(' + b + ')" src="' + img_url + 'mpvs.png" /> 		<img title="Get forum link to image" onclick="action_link(' + b + ')" src="' + img_url + 'mpvl.png" /> 	</div> 	<div class="mi4">' + imagelist[b - 1].d + " :: " + imagelist[b - 1].n + '</div> 	<div style="clear:both"></div> </div>';
        document.getElementById("image_" + b).innerHTML = c;
        rescale_image(b, document.getElementById("imgsrc_" + b))
    } else {
        if (imagelist[b - 1].xhr != undefined) {
            return
        }
        imagelist[b - 1].xhr = new XMLHttpRequest();
        var a = {
            method: "imagedispatch",
            gid: gid,
            page: b,
            imgkey: imagelist[b - 1].k,
            mpvkey: mpvkey
        };
        api_call(imagelist[b - 1].xhr, a, function() {
            load_image_dispatch(b)
        })
    }
}

function load_thumb(a) {
    document.getElementById("thumb_" + a).innerHTML = '<img src="http://' + thumbserver + imagelist[a - 1].t + '" title="Page ' + a + " - " + imagelist[a - 1].n + '" />'
}

function preload_generic(c, j, d) {
    var f = c.scrollTop;
    var b = f + c.offsetHeight;
    var a = j == "image";
    for (var g = 1; g <= pagecount; g++) {
        var e = document.getElementById(j + "_" + g);
        var k = e.offsetTop;
        var h = k + e.offsetHeight;
        if ((e.style.visibility == "hidden") && (h >= f) && (k <= b + d)) {
            if (a) {
                load_image(g)
            } else {
                load_thumb(g)
            }
            e.style.visibility = "visible"
        } else {
            if (e.style.visibility == "visible") {
                if ((h < f - 10000) || (k > b + 10000)) {
                    e.innerHTML = "";
                    e.style.visibility = "hidden"
                }
                if (a && (h >= f + 100) && (k <= f + 100)) {
                    currentpage = g
                }
            }
        }
    }
}

function preload_scroll_thumbs() {
    preload_generic(pane_thumbs, "thumb", 100)
}

function preload_scroll_images() {
    preload_generic(pane_images, "image", 3000);
    sync_thumbpane()
}
var extents_update_timer = undefined;

function update_window_extents_delayed() {
    if (typeof extents_update_timer != "undefined") {
        clearTimeout(extents_update_timer)
    }
    extents_update_timer = setTimeout("update_window_extents()", 20)
}

function update_window_extents() {
    pane_outer.style.height = (window.innerHeight - 4) + "px";
    pane_outer.style.width = (window.innerWidth - 5) + "px";
    pane_images.style.width = (window.innerWidth - (hide_thumbbar ? 5 : 230)) + "px";
    pane_images.style.height = (window.innerHeight - 4) + "px";
    pane_thumbs.style.height = (window.innerHeight - 4) + "px";
    var d = 0;
    var a = 0;
    for (var c = 1; c <= pagecount; c++) {
        var e = document.getElementById("imgsrc_" + c);
        if (e != undefined) {
            var b = rescale_image(c, e);
            d += b.view_height + 24;
            a += b.scroll_offset
        }
    }
    pane_images_inner.style.height = d + "px";
    if (a != 0) {
        scroll_relative("pane_images", a)
    }
    extents_update_timer = undefined
}

function do_fullscreen() {
    var a = document.body.requestFullScreen || document.body.webkitRequestFullScreen || document.body.mozRequestFullScreen || document.body.msRequestFullScreen;
    if (a) {
        a.call(document.body)
    } else {
        alert("You are using an old crappy browser. Hit F11 to enter and exit fullscreen.")
    }
}
document.body.style.overflow = "hidden";
window.onresize = update_window_extents_delayed;
hookEvent("pane_thumbs", "mousewheel", function(a) {
    do_mousescroll("pane_thumbs", a)
});
hookEvent("pane_images", "mousewheel", function(a) {
    do_mousescroll("pane_images", a)
});
hookEvent("pane_images", "touchstart", do_touchstart);
hookEvent("pane_images", "touchend", do_touchend);
hookEvent("pane_images", "touchcancel", do_touchend);
hookEvent("pane_images", "touchleave", do_touchend);
hookEvent("pane_images", "touchmove", do_touchmove);
pane_thumbs.onscroll = preload_scroll_thumbs;
pane_images.onscroll = preload_scroll_images;
pane_thumbs.onclick = function() {
    setTimeout(function() {
        preload_scroll_images()
    }, 50)
};
setTimeout(function() {
    update_window_extents();
    preload_scroll_images();
    preload_scroll_thumbs()
}, 50);
document.onkeydown = function(b) {
    if (b.altKey || b.ctrlKey || b.metaKey) {
        return
    }
    var a = (window.event) ? b.keyCode : b.which;
    if (b.shiftKey) {
        switch (a) {
            case KeyEvent.DOM_VK_SPACE:
                scroll_relative("pane_images", Math.round(window.innerHeight * 0.75));
                cancelEvent(b);
                break
        }
    } else {
        switch (a) {
            case KeyEvent.DOM_VK_PAGE_DOWN:
            case KeyEvent.DOM_VK_RIGHT:
            case KeyEvent.DOM_VK_D:
            case KeyEvent.DOM_VK_NUMPAD6:
                document.location = "#page" + Math.min(pagecount, currentpage + 1);
                cancelEvent(b);
                break;
            case KeyEvent.DOM_VK_PAGE_UP:
            case KeyEvent.DOM_VK_LEFT:
            case KeyEvent.DOM_VK_A:
            case KeyEvent.DOM_VK_NUMPAD4:
                document.location = "#page" + Math.max(1, currentpage - 1);
                cancelEvent(b);
                break;
            case KeyEvent.DOM_VK_UP:
            case KeyEvent.DOM_VK_W:
            case KeyEvent.DOM_VK_NUMPAD8:
                scroll_relative("pane_images", 50);
                cancelEvent(b);
                break;
            case KeyEvent.DOM_VK_DOWN:
            case KeyEvent.DOM_VK_S:
            case KeyEvent.DOM_VK_NUMPAD2:
                scroll_relative("pane_images", -50);
                cancelEvent(b);
                break;
            case KeyEvent.DOM_VK_SPACE:
                scroll_relative("pane_images", -Math.round(window.innerHeight * 0.75));
                cancelEvent(b);
                break
        }
    }
};