<?php


if ( false !== $answer = ifFileExists() ){
    $imgSrc = $answer;
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    $mime = $finfo->file($imgSrc);
    header("Content-Type: ".$mime);
    header('Content-Length: ' . filesize($imgSrc));
    readfile($imgSrc);
}



function ifFileExists(){
    $subf = $_GET['sub'] === "t" ? "thumbnails/" : ($_GET['sub']==="c" ? "cooking/" : "");
    $gid = $_GET['gid'];
    $hash = $_GET['hash'];
    $image = "";

    $scanned_directory = array_diff(scandir($subf), array('..', '.'));
    foreach($scanned_directory as $string){
        #echo "comparing: ".$string." to ".$imageHash."<br>";
        if (strpos($string, $hash) !== false) {
            #echo "image Hash correct<br>";
            if (strpos($string,$gid)!==false ){
                #echo "<br>user match or no user: ".$user."<br><br>";
                if (file_exists($subf.$string)){
                    return $subf.$string;
                }
            }
        }
    }
    return false;
}
