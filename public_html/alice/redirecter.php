<?php
require_once 'core/config.php';

if (ifFileExists()){
  //do nothing cause we already served the file!  
} else {
  $image = get404Image();
  if($_GET['sub']=='t'){$image = "t_".$image;}
  $path = sprintf("%s%s/%s",ROOT_DIR,SMALL_ICONS_DIR,$image);
  dumbReadFile($path,'404');
}

function dumbReadFile($url,$name){
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    $mime = $finfo->file($url);
    header("Content-Type: ".$mime);
    header('Content-Length: ' . filesize($url));
    if(isset($name)&&!empty($name)){
        header("Content-Disposition:inline;filename=$name");
    }
    
    echo file_get_contents($url);
    

}

function ifFileExists(){
  if(!isset($_GET['sub']) || empty($_GET['sub']))return false;
  if(!isset($_GET['gid']) || empty($_GET['gid']))return false;
  if(!isset($_GET['hash']) || empty($_GET['hash']))return false;
  
  $suffix = "";
  
  
  //get appropriate suffix for the actual file
  switch($_GET['sub']){
    case THUMBNAIL_DIR_URL:
      $suffix = THUMBNAIL_SUFFIX;
      break;
    case LARGE_IMAGE_DIR_URL:
      $suffix = RAW_SUFFIX;
      break;
    case IMAGE_DIR_URL:
    default:
      $suffix = BASE_SUFFIX;
  }
  
  $gid = $_GET['gid'];
  $shortID = $_GET['hash'];
  
  //get the connection
  require_once 'core/connect.php';
  
  $query = "SELECT hash,name,hasRaw,actualPageOrder FROM pages WHERE pages.gid=? AND pages.shortID=? AND pages.hidden=0";
  if(!$stmt = $mysqli->prepare($query)){
      throw new RuntimeException('Database error fr1. Inform Admin');
  }
  if(!$stmt->bind_param('is',$gid,$shortID)){
      throw new RuntimeException('Database error fr2. Inform Admin');
  }
  if(!$stmt->bind_result($hash,$name,$hasRaw,$hidden)){
      throw new RuntimeException('Database error fr3. Inform Admin');
  }
  if(!$stmt->execute()){
      throw new RuntimeException('Database error fr4. Inform Admin');
  }
  $stmt->fetch();
  $stmt->close();
  
  //if we found nothing, return false
  if(empty($hash)){
    return false;
  }
  
  //if no raw but raw requested, show base image
  if(!$hasRaw && $_GET['sub']==LARGE_IMAGE_DIR_URL){
    $suffix = BASE_SUFFIX;
  }
  
  
  //otherwise concat location with hash with suffix
  $image_location = sprintf('%s/%s%s',IMAGE_UPLOAD_DIR,$hash,$suffix);
  $mime = getMimeType($image_location);
  smartReadFile($image_location,$name,$mime);
  return true;
  
  
  
  
    $subf = $_GET['sub'] === "t" ? "thumbnails/" : ($_GET['sub']==="c" ? "cooking/" : "");
    $gid = $_GET['gid'];
    $hash = $_GET['hash'];
    $image = "";

    $scanned_directory = array_diff(scandir($subf), array('..', '.'));
   
}

//returns a local url for 404 image, can made dynamic
function get404Image(){
  return '404_yoshino.jpg';
}


//taken from somwhere I don't remember. google it if you want to
function smartReadFile($location, $filename, $mimeType = 'application/octet-stream')
{
	if (!file_exists($location))
	{
		header ("HTTP/1.1 404 Not Found");
		return;
	}
	
	$size	= filesize($location);
	$time	= date('r', filemtime($location));
	
	$fm		= @fopen($location, 'rb');
	if (!$fm)
	{
		header ("HTTP/1.1 505 Internal server error");
		return;
	}
	
	$begin	= 0;
	$end	= $size - 1;
	
	if (isset($_SERVER['HTTP_RANGE']))
	{
		if (preg_match('/bytes=\h*(\d+)-(\d*)[\D.*]?/i', $_SERVER['HTTP_RANGE'], $matches))
		{
			$begin	= intval($matches[1]);
			if (!empty($matches[2]))
			{
				$end	= intval($matches[2]);
			}
		}
	}

	if (isset($_SERVER['HTTP_RANGE']))
	{
		header('HTTP/1.1 206 Partial Content');
	}
	else
	{
		header('HTTP/1.1 200 OK');
	}
	
	header("Content-Type: $mimeType"); 
	//header('Cache-Control: public, must-revalidate, max-age=0');
    header("Cache-Control: max-age=2592000"); //30days (60sec * 60min * 24hours * 30days)
	//header('Pragma: no-cache');  
    header("Pragma:");
	header('Accept-Ranges: bytes');
	header('Content-Length:' . (($end - $begin) + 1));
	if (isset($_SERVER['HTTP_RANGE']))
	{
		header("Content-Range: bytes $begin-$end/$size");
	}
	header("Content-Disposition: inline; filename=\"$filename\"");
	header("Content-Transfer-Encoding: binary");
	header("Last-Modified: $time");
    header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + (60 * 60 * 24 * 7))); // 1 hour

	
	$cur	= $begin;
	fseek($fm, $begin, 0);
	
	while(!feof($fm) && $cur <= $end && (connection_status() == 0))
	{
		print fread($fm, min(1024 * 16, ($end - $cur) + 1));
		$cur += 1024 * 16;
	}
}
function getMimeType($file){
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    $mime = $finfo->file($file);
    return $mime;
}