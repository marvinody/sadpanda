/*
function uploadSelection(b,a,c){
    b.open("POST", "test_post_json.php",true);
    //b.setRequestHeader("Content-Type", "application/json");
    //b.setRequestHeader('Content-Type','multipart/form-data');
    console.log("Sending Data:");
    console.log(JSON.stringify(a));
    b.onreadystatechange = c;
    b.send(a)
}
function api_response(b) {
    if (b.readyState == 4) {
        if (b.status == 200) {
            var a = JSON.parse(b.responseText);
            return a;
        } else {
            console.error("Server communication failed: " + b.status + " (" + b.responseText + ")")
        }
    }
    return false
}*/
"use strict";
function UploadHandler(){
    this.xhr = new XMLHttpRequest();
    this.section = 0;
    this.chunks = [];
    this.FL = document.getElementById("files").files;
}

UploadHandler.prototype.uploadAll = function(){
    console.log("uploadAll called");
    console.log("Section:",this.section);
    console.log("Chunks:",this.chunks.length);
    
    if (this.chunks.length === 0){
        this.chunks = this.chunkFiles(this.FL.length);
        console.dir(this);
    }
    if (this.section === this.chunks.length){
        return;
    }
    
    var fd = new FormData();
    var i;
    for (i=this.chunks[this.section].start; i<=this.chunks[this.section].end; i+=1){
        fd.append("userfile[]",this.FL[i]);
    }
    fd.append("section",this.section);
    this.section+=1;
    this.uploadSelection(fd, this.dealWithResponse);
    
};
UploadHandler.prototype.dealWithResponse = function(){
    var a = this.apiResponse();
    if (a !== false) {
       console.log("received data:");
       console.log(a);
       this.uploadAll();
    }
    
};
UploadHandler.prototype.retFalse = function(){
    return false;
};
UploadHandler.prototype.apiResponse = function(){
    if (this.xhr.readyState == 4) {
        if (this.xhr.status == 200) {
            var a = JSON.parse(this.xhr.responseText);
            return a;
        }
        console.error("Server communication failed: " + this.xhr.status + " (" + this.xhr.responseText + ")");
    }
    return false;
};
UploadHandler.prototype.uploadSelection = function(a,c){
    //a = data
    //c = callback
    this.xhr.open("POST", "test_post_json.php",true);
    console.log("Sending Data:");
    console.log(JSON.stringify(a));
    this.xhr.onreadystatechange = c.bind(this);
    this.xhr.send(a);
};
UploadHandler.prototype.chunkFiles = function(len){
    var retVal = [];
    var perTransfer = 10;
    var i;
    for (i = 0; (i)*perTransfer <= len ; i+=1){
        retVal[i] = {
            start: i*perTransfer,
            end:Math.min((i+1)*perTransfer-1,len-1)
        };
    }
    return retVal;
};
"use strict";
var selDiv = "";
    document.addEventListener("DOMContentLoaded", init, false);
    function init() {
        document.querySelector('#files').addEventListener('change', handleFileSelect, false);
        document.querySelector("form").addEventListener('submit',function(e){
            e.preventDefault();
            console.log("Uploading");
            var uploader = new UploadHandler();
            uploader.uploadAll();
        });
        selDiv = document.querySelector("#selectedFiles");
    }
    function handleFileSelect(e) {
        if(!e.target.files || !window.FileReader) {return;}
        selDiv.innerHTML = "";
        var files = e.target.files;
        var filesArr = Array.prototype.slice.call(files);
        filesArr.forEach(function(f,i) {
            var f = files[i];
            if(!f.type.match("image.*")) {
                return;
            }
            var reader = new FileReader();
            reader.onload = function (e) {
                var html = "<img src=\"" + e.target.result + "\">" + f.name + "<br clear=\"left\"/>";
                selDiv.innerHTML += html;                
            }
            reader.readAsDataURL(f); 
        });
    }