<?php

if(isset($_SESSION['prefsLoaded'])&&!empty($_SESSION['prefsLoaded']&&$_SESSION['prefsLoaded']==true)){
	//do nothing cause everything loaded into session already
} else {
	//load prefs
	if(isset($_SESSION['user'])&&!empty($_SESSION['user']['id'])){
		//assumed logon
		loadPrefs($id);
	} else {
		//loadDefPrefs
		$_SESSION['prefs']['rows'] = 4;
		$_SESSION['prefs']['size'] = 'N';
		$_SESSION['prefs']['viewer'] = 's';
        $_SESSION['prefsLoaded'] = true;
	}
}

function savePageSession(){
    function findOverlap($str1, $str2){
        /*http://stackoverflow.com/questions/2945446/built-in-function-to-combine-overlapping-string-sequences-in-php*/
        $return = array();
        $sl1 = strlen($str1);
        $sl2 = strlen($str2);
        $max = $sl1>$sl2?$sl2:$sl1;
        $i=1;
        while($i<=$max){
            $s1 = substr($str1, -$i);
            $s2 = substr($str2, 0, $i);
            if($s1 == $s2){
                $return[] = $s1;
            }
            $i++;
        }
        if(!empty($return)){
            return $return;
        }
        return false;
    }
    function replaceOverlap($str1, $str2, $length = "long"){
        if($overlap = findOverlap($str1, $str2)){
            switch($length){
                case "short":
                    $overlap = $overlap[0];
                    break;
                case "long":
                    default:
                    $overlap = $overlap[count($overlap)-1];
                break;
            }     
            $str1 = substr($str1, 0, -strlen($overlap));
            $str2 = substr($str2, strlen($overlap));
            return $str1.$overlap.$str2;
        }
        return false;
    }

    $_SESSION['url'] = replaceOverlap(BASE_HREF,$_SERVER['REQUEST_URI']);
    //doesn't feel right to have this here, but this lets me only
    //call it if session is started. which works
}
function loadPageSession(){
    $url = BASE_HREF;
    $url = isset($_SESSION['url']) ? $_SESSION['url'] : BASE_HREF;
    header("Location: $url");
    die();

}

function loadPrefs($id,$mysqli){
	#TODO, FINISH LATER
    $_SESSION['prefs'] = [];
    $_SESSION['prefsLoaded'] = false;
    $query = "SELECT pref,value from userPrefs where userId=?";
    if(!$stmt = $mysqli->prepare($query)){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        die;
    }
    if(!$stmt->bind_param('i',$id)){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        die;
    }
    if(!$stmt->execute()){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        die;
    }
    $stmt->bind_result($pref,$value);
    while($stmt->fetch()){
        $_SESSION['prefs'][$pref] = $value;
    }
    $stmt->close();
    
    $_SESSION['prefsLoaded'] = true;
}


function savePrefs($id,$mysqli){
    if(!($id > 0)){return false;}
    $query = "INSERT INTO userPrefs (userId,pref,value) values (?,?,?) 
            ON DUPLICATE KEY UPDATE
            value=?;";

    if(!$stmt = $mysqli->prepare($query)){
        printf('errno: %d, error: %s', $mysqli->errno, $mysqli->error);
        die;
    }
    $pref = "";
    $value = "";
    if(!$stmt->bind_param("isss",$id,$pref,$value,$value)){
        printf('errno: %d, error: %s', $mysqli->errno, $mysqli->error);
        die; 
    }
    foreach($_SESSION['prefs'] as $pref1 => $value1){
        $pref = $pref1;
        $value = "".$value1;
        if(!$stmt->execute()){
            printf('errno: %d, error: %s', $mysqli->errno, $mysqli->error);
            die;
        }
    }
    
}
function checkForChangedSetting($getParam,$posValues,$link,$mysqli,$prefName = null){
    
    $prefName = isset($prefName) ? $prefName : $getParam;
	if(isset($_GET[$getParam])&&!empty($_GET[$getParam])){
		if(in_array("".($_GET[$getParam]),$posValues,true)){
			$_SESSION['prefs'][$prefName] = ($_GET[$getParam]);
            savePrefs($_SESSION['user']['id'],$mysqli);
            header('Pragma: no-cache'); 
            header("Refresh:0;url=".$link);
            die;
		}
        
	}
}