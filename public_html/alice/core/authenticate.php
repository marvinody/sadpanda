<?php
function authenticate($mysqli,$requireLogOn = false){
    if(isset($_SESSION['user'])&&!empty($_SESSION['user']['id'])){
        $query = "SELECT sessionId FROM `logins` WHERE userId=? ORDER BY id DESC LIMIT 1";
        if(!$stmt = $mysqli->prepare($query)){
            error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
            die;
        }
        if(!$stmt->bind_param('i',$_SESSION['user']['id'])){
            error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
            die;
        }
        
        if(!$stmt->execute()){
            error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
            die;
        }
        if(!$stmt->bind_result($sessId)){
            error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
            die;
        }
        $stmt->fetch();
        $stmt->close();
        #echo "sessID:".$sessId."<br>";
        #echo "session_id:".session_id()."<br>";
        if(session_id()==$sessId){
            //good sess
            return true;
        } else {
            die("Your session has become invalidated. Please relogin");
        }
    } else {
        if ($requireLogOn){
            return false;
        }
    }
    
    #idk what to return. should user be authenticated if no sign in?
    //change name to checkcurrent sessionId
    //make another function to check if id is set and shit like that.
    return true;
    
}
function check_ownership($mysqli,$gid){
  //assume authenticated already because we only call this after...
  $query = "SELECT uploaderId FROM albums WHERE gid=?";
  if(!$stmt = $mysqli->prepare($query)){
    error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
    die;
  }
  if(!$stmt->bind_param('i',$gid)){
    error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
    die;
  }

  if(!$stmt->execute()){
    error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
    die;
  }
  if(!$stmt->bind_result($uploaderId)){
    error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
    die;
  }
  $stmt->fetch();
  $stmt->close();
  
  if($_SESSION['user']['id']==$uploaderId){
    return true;   
  }
  return false;
}
function obliterate_session(){
    // Unset all of the session variables.
$_SESSION = array();

    // If it's desired to kill the session, also delete the session cookie.
    // Note: This will destroy the session, and not just the session data!
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
              $params["path"], $params["domain"],
              $params["secure"], $params["httponly"]
        );
    }

    // Finally, destroy the session.
    session_destroy();
    session_regenerate_id(true);
}
function login($mysqli,$user,$pass){
    $query = "SELECT id,passHash FROM `users` WHERE user=?";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param('s',$user);
    $stmt->bind_result($id,$passHash);
    $stmt->execute();
    $stmt->fetch();
    $stmt->close();
    $retVal = array("error"=>"Could not verify with the supplied username/password");
    if($id>0){
        if(password_verify($pass,$passHash)){
            //success on login
            $_SESSION['user'] = [];
            $_SESSION['user']['id'] = $id;
            $_SESSION['user']['username'] = $user;
            $_SESSION['user']['sessionID'] = session_id();
            
            if(isset($_SESSION['gallery'])){
                $_SESSION['gallery']['reload'] = true;
            }
            
            $retVal = array(
                "username"=>$user,
                "id"=>$id,
                "sessionID"=>session_id(),
            );
        }
        
        $options = array('cost' => 11);
        $query = "INSERT INTO logins (userId,attemptedHash,ip,sessionId) values (?,?,?,?)";
        if(!$stmt = $mysqli->prepare($query)){
            error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
            die;
        }
        if(!$stmt->bind_param(
            'isss',
            $id,
            password_hash($_POST['PassWord'], PASSWORD_BCRYPT, $options),
            $_SERVER['REMOTE_ADDR'],
            session_id()
        )){
            error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
            die;
        }
        if(!$stmt->execute()){
            error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
            die;
        }
        $stmt->close();
    }
    return $retVal;
}
function loadUserVote($mysqli){
    /*TODO make this a database call and pull data needed*/
    $_SESSION['user']['votePower'] = 1;
}
function checkForExistingUserName($user,$mysqli){
    $query = "SELECT id FROM `users` WHERE user=?";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param('s',$user);
    $stmt->bind_result($hasUser);
    $stmt->execute();
    $stmt->fetch();
    $stmt->close();
    return $hasUser;
}