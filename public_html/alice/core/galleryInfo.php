<?php

/*
require_once 'config.php';
require_once 'connect.php';
//can probably do some caching here if required but not gonna worry until it gets slow
echo "<pre>";
print_r(getGallery($_GET['gid'],$mysqli));
echo "</pre>";

*/
if(!defined('SITE_NAME')) {
   die('Direct access not permitted');
}

function getGallery($gid,$mysqli,$reload=false){
	if(isset($_SESSION['gallery']) && $_SESSION['gallery']['gid']==$gid && !isset($_SESSION['gallery']['reload']) && !$reload){
		if(isset($_SESSION['gallery']['timesLoaded'])){$_SESSION['gallery']['timesLoaded']++;}else{$_SESSION['gallery']['timesLoaded']=1;}
		return $_SESSION['gallery'];//small caching, works for 1 album
	}
    if(!is_numeric($gid)){
        die("gid is invalid number");
    }
    $album = getBasicInfo($gid,$mysqli);
    $album['pages'] = getImages($gid,$mysqli);
    $album['tags'] = getTags($gid,$mysqli);
    $album['userTags'] = getUserTags($gid,$mysqli);
    $album['comments'] = getComments($gid,$mysqli);
    list($album['averageVotes'],$album['numVotes']) = getAverageAlbumVote($gid,$mysqli);
    $album['userVote'] = getUserAlbumVote($gid,$mysqli,$album['averageVotes']);

    $album['totalFileSize'] = 0;
    for($i=0;$i<count($album['pages']);$i++ ){
        $album['totalFileSize']+=$album['pages'][$i]['filesize'];
    }
    $album['totalFileSize'] = $album['totalFileSize']===0 ? "N/A" : $album['totalFileSize'];
    $album['singlePageViewer'] = BASE_HREF.'/s/'.$gid.'/';
    $album['multiPageViewer'] = BASE_HREF.'/mpv/'.$gid.'/';
    $album['link']=BASE_HREF.'/g/'.$gid.'/';
    $album['favorites'] = getAllFavorites($gid,$mysqli);
    $album['favorites']['userFav'] = getFavorite($gid,$mysqli);
    getAllFavorites($gid,$mysqli);
    $_SESSION['gallery'] = $album;
    
    return $album;
    
}

function getBasicInfo($gid,$mysqli){
    if(isset($_SESSION['gallery']) && $_SESSION['gallery']['gid']==$gid){
        return $_SESSION['gallery'];   
    }
    $query = "SELECT a.name,a.gid,a.cover_thumb,b.user, a.votes, a.parent, a.isTranslated, a.TIMESTAMP FROM albums a,users b WHERE gid=".$gid." and a.uploaderId=b.id";
    $album = array();
    if ($stmt = $mysqli->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result($name,$gid,$cover_thumb,$uploader,$votes,$parent,$isTranslated,$timestamp);
		$stmt->fetch();
		$album = array(
			'title'=>htmlEntities($name, ENT_QUOTES),
			'gid'=>$gid,
			'cover_thumb'=>$cover_thumb,
			'thumbnail'=>BASE_HREF."/t/".$gid."/".$cover_thumb.".jpg",
			'uploader'=>$uploader,
			'votes'=>$votes,
			'isTranslated'=>$isTranslated === 1 ? true : false,
			'parent'=>$parent,
			'timestamp'=>substr($timestamp,0,strlen($timestamp)-3),//cut down the seconds part on timestamp
		);
        $stmt->close();
    }
    return $album;
}

function getFavorite($gid,$mysqli){
    require_once 'authenticate.php';
    $retVal = array();
    if(authenticate($mysqli,true)){
        $query = "SELECT  `favoriteList` ,  `note` 
            FROM  `favorites` WHERE gid = ? AND  `userId` =?";
        if ($stmt = $mysqli->prepare($query)) {
            $stmt->bind_param('ii',$gid,$_SESSION['user']['id']);
            $stmt->execute();
            $stmt->bind_result($favCat,$favNote);
            $stmt->fetch();
            $retVal['favNote'] = $favNote;
            $retVal['favCat'] = $favCat;
        }
    } else {
        //not logged in, no favorites
    }
    return $retVal;
}
function getAllFavorites($gid,$mysqli){
    $query = "SELECT COUNT( * ) FROM  `favorites` where gid=$gid";
    $res = $mysqli->query($query);
    $row = $res->fetch_row();
    $string = "0 times";
    if($row[0]>0){
        if($row[0]>1){$string = $row[0]." times";}
        else{$string = $row[0]." time";}
    }
    return array(
        'numFavs'=>$row[0],
        'strFav'=>$string,
    );
}
function getUserAlbumVote($gid,$mysqli,$avg){
    if(isset($_SESSION['user'])&&!empty($_SESSION['user']['id'])){
        $query = "SELECT rating FROM albumVotes WHERE gid=? and userId=?";
        $stmt=$mysqli->prepare($query);
        $stmt->bind_param("ii", $gid,$_SESSION['user']['id']);
        $stmt->execute();
        $stmt->bind_result($rating);

        $stmt->fetch();
        if(!empty($rating)){return array("vote"=>$rating/2,"voted"=>1);}
        else{return array("vote"=>$avg,"voted"=>0);}
    } else {
        return array("vote"=>$avg,"voted"=>0);
    }
}

function getAverageAlbumVote($gid,$mysqli){
	//$query = "SELECT rating FROM albumVotes WHERE gid=".$gid;
	$query = "SELECT AVG(rating) as avgRating,COUNT(id) as totalVotes FROM `albumVotes` where gid=$gid;";
    $averageVote = 0;$i=0;
	if ($stmt = $mysqli->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($averageVote,$i);
        $stmt->fetch();
	
	}
	return array(round($averageVote/2,1),$i);
}
function getUserTags($gid,$mysqli){
    $query = "SELECT a.`tag`, b.`namespace`,c.`votes`
        FROM tags a, namespace b, albumTags c
        WHERE gid=? and a.namespaceId = b.id and c.tagId=a.id and c.userId=? ORDER by b.priority,b.id ASC";
    if(!$stmt = $mysqli->prepare($query)){
        error_log('Database error getUserTags1. Inform Admin');
        die('Database error getUserTags1. Inform Admin');
    }
    if(!$stmt->bind_param('ii',$gid,$_SESSION['user']['id'])){
        error_log('Database error getUserTags2. Inform Admin');
        die('Database error getUserTags2. Inform Admin');
    }
    if(!$stmt->bind_result($tag,$namespace,$votes)){
        error_log('Database error getUserTags3. Inform Admin');
        die('Database error getUserTags3. Inform Admin');
    }
    if(!$stmt->execute()){
        error_log('Database error getUserTags4. Inform Admin');
        die('Database error getUserTags4. Inform Admin');
    }
    $tagList = [];
    $i=0;
    while ($stmt->fetch()) {
        if($i===0){
            $tagList[] = array(
                'namespace'=>$namespace,
                'tags'=>[],
            );
            $tagList[$i]['tags'][] = array(
                'tag'=>$tag,
                'votes'=>$votes,
            );

            $i++;
        } else {
            if($namespace===$tagList[$i-1]['namespace']){//namespace will always be ordered by priority
                //append tag to current namespace
                $tagList[$i-1]['tags'][] = array(
                    'tag'=>$tag,
                    'votes'=>$votes,
                );
            } else {
                //new namespace and add it

                $tagList[] = array(
                    'namespace'=>$namespace,
                    'tags'=>[],
                );
                $tagList[$i]['tags'][] = array(
                    'tag'=>$tag,
                    'votes'=>$votes,
                );
                $i++;
            }
        }
    }
    $stmt->close();
    return $tagList;
    
}
/*
SELECT a.`tag`, b.`namespace`,sum( distinct temp.votes) AS votes,b.priority
        FROM tags a, namespace b, albumTags c,(SELECT ( aT.votes * uPT.userPowerTotal) AS votes,aT.tagId FROM albumTags aT, userPowerTotals uPT WHERE aT.userId=uPT.userId) AS temp
        WHERE gid=86532864 and a.namespaceId = b.id and temp.tagId=a.id and c.tagId=temp.tagId and c.`votes`>0  GROUP BY a.id ORDER by b.priority,b.id ASC

SELECT a.`tag`, b.`namespace`,SUM(temp.votes) AS votes,b.priority
        FROM tags a, namespace b, albumTags c,(SELECT ( aT.votes * uPT.userPowerTotal) AS votes,aT.tagId FROM albumTags aT, userPowerTotals uPT WHERE aT.userId=uPT.userId AND gid=86532864) AS temp
        WHERE gid=86532864 and a.namespaceId = b.id and temp.tagId=a.id and c.`votes`>0 GROUP BY a.id ORDER by b.priority,b.id ASC


SELECT SUM(temp.votes),temp.tagId FROM (SELECT ( aT.votes * uPT.userPowerTotal) AS votes,aT.tagId FROM albumTags aT, userPowerTotals uPT WHERE aT.userId=uPT.userId) as temp GROUP BY temp.tagId
SELECT ( aT.votes * uPT.userPowerTotal) FROM albumTags aT, userPowerTotals uPT WHERE aT.userId=uPT.userId*/
function getTags($gid,$mysqli){
	
    $query = "SELECT a.`tag`, b.`namespace`,sum( distinct temp.votes) AS votes,b.priority
        FROM tags a, namespace b, albumTags c,(SELECT ( aT.votes * uPT.userPowerTotal) AS votes,aT.tagId FROM albumTags aT, userPowerTotals uPT WHERE aT.userId=uPT.userId) AS temp
        WHERE gid=$gid and a.namespaceId = b.id and temp.tagId=a.id and c.tagId=temp.tagId and c.`votes`>0  GROUP BY a.id ORDER by b.priority,b.id,a.id ASC";
    
    /*$query = "SELECT a.`tag`, b.`namespace`,SUM(c.`votes`) AS votes,b.priority
        FROM tags a, namespace b, albumTags c
        WHERE gid=$gid and a.namespaceId = b.id and c.tagId=a.id and c.`votes`>0 GROUP BY a.id ORDER by b.priority,b.id ASC";*/
	$tagList = [];
	if ($stmt = $mysqli->prepare($query)) {
		$stmt->execute();
		$stmt->bind_result($tag, $namespace,$votes,$priority);
		$i=0;
		while ($stmt->fetch()) {
			if($i===0){
				$tagList[] = array(
					'namespace'=>$namespace,
					'tags'=>[],
				);
				$tagList[$i]['tags'][] = array(
					'tag'=>$tag,
					'votes'=>$votes,
				);
				
				$i++;
			} else {
				if($namespace===$tagList[$i-1]['namespace']){//namespace will always be ordered by priority
					//append tag to current namespace
					$tagList[$i-1]['tags'][] = array(
						'tag'=>$tag,
						'votes'=>$votes,
					);
				} else {
					//new namespace and add it
					
					$tagList[] = array(
						'namespace'=>$namespace,
						'tags'=>[],
					);
					$tagList[$i]['tags'][] = array(
						'tag'=>$tag,
						'votes'=>$votes,
					);
					$i++;
				}
			}
		}
		/*$tags[] = array (
				'tag'=>$tag,
				'namespace'=>$namespace,
				'votes'=>$votes,
				'priority'=>$priority,
			);*/
		$stmt->close();
	}
	
	return $tagList;
}

function getComments($gid,$mysqli){
	$query = "SELECT a.`comment`, b.`user`,UNIX_TIMESTAMP(a.`updatedTimestamp`) as timestampInt,a.opVote,a.isOp,a.id,b.id
        FROM comments a, users b
        WHERE gid=".$gid." and a.userId = b.id ORDER by a.id ASC";
	$comments = [];
    if ($stmt = $mysqli->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result($comment, $user,$timestamp,$opVote,$isOp,$id,$userId);
        while ($stmt->fetch()) {
            $comments[] = array (
				'cid'=>$id,
                'comment'=>nl2br(htmlEntities($comment, ENT_QUOTES)),
                'user'=>$user,
                'timestamp'=>$timestamp,
				'baseVote'=>$opVote,
                'totalVote'=>$opVote,
                'isOp'=>$isOp,
                'userId'=>$userId,
				
            );
        }
        
    }
	$stmt->close();
	for($i=0;$i<count($comments);$i++){
		if($comments[$i]['isOp']===0){
			$comments[$i]['votes'] = getVotesForComment($comments[$i]['cid'],$mysqli);
			for($k=0;$k<count($comments[$i]['votes']);$k++){
				$comments[$i]['totalVote']+=$comments[$i]['votes'][$k]['vote'];
			}
			
		}
		
		
	}
    return $comments;
}

function getVotesForComment($id,$mysqli){
	$query = "SELECT b.user, a.points
        FROM commentVotes a,users b
        WHERE ".$id."=a.commentId AND a.userId=b.id ORDER by a.id ASC";
	$votes = [];
	if ($stmt = $mysqli->prepare($query)) {
        $stmt->execute();
		$stmt->bind_result($user,$vote);
		while($stmt->fetch()){
			$votes[] = array(
				'user'=>$user,
				'vote'=>$vote,
			);
		}
		$stmt->close();
	} else {
		$votes = $mysqli->error;
	}
	
	return $votes;
}

function getImages($gid,$mysqli){
    $query = "SELECT shortID,name,actualPageOrder,width,height,thumb_width,thumb_height,filesize,hasRaw,id FROM ".DB_TABLE_PAGES." WHERE gid=".$gid." and hidden=0 ORDER by actualPageOrder ASC";
    $images = [];
    if ($stmt = $mysqli->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result($shortID,$name,$page,$width,$height,$thumb_width,$thumb_height,$filesize,$hasRaw,$id);
        while ($stmt->fetch()) {
            $images[] = array (
                'shortID'=>$shortID,
                'name'=>htmlEntities($name, ENT_QUOTES),
                'page'=>$page,
                'width'=>$width,
                'height'=>$height,
                'thumb_width'=>$thumb_width,
                'thumb_height'=>$thumb_height,
                'filesize'=>$filesize,
				't_link'=>BASE_HREF."/t/".$gid."/".$shortID.".jpg",
				'f_link'=>BASE_HREF."/c/".$gid."/".$shortID.".jpg",
                'hasRaw'=>$hasRaw,
                'id'=>$id
            );
        }
        $stmt->close();
    } else {
		//ideally should never happen
		$images[0]="ERROR:".$mysqli->error;
	}

    return $images;
}
