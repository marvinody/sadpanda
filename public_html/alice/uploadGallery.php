<!doctype html>
<html>
    <head>
        <title>Upload Gallery</title>
        <script>
            
        </script>
        <style>
            #selectedFiles img {
                max-width: 125px;
                max-height: 125px;
                float: left;
                margin-bottom:10px;
            }
        </style>
    </head>
    <body>
        Only taking .jpg or .png <br>
		.png will be saved but converted to jpg and served that way to reduce bandwidth.<br>
		.png will still be available...somehow...eventually
        <form method="post" enctype="multipart/form-data">
            <input name='title' type='text' placeholder='Title'/>
            <input name='uploader' type='text' placeholder='Uploader'/>
            <input name='key' type='password' placeholder='Key' />
            Send these files:<br />
            <input name='userfile[]' id='files' type='file' multiple='multiple' accept='image/*'/>
            <input type="submit" value="Send files" />
            <div id='selectedFiles'></div>
          
        </form>
        <script>
        
    function UploadHandler(){
        this.xhr = new XMLHttpRequest();
        this.section = 0;
        this.chunks = [];
        this.FL = document.getElementById("files").files;
        this.id = 0;
    }

    UploadHandler.prototype.uploadAll = function(){
        console.log("uploadAll called");
        console.log("Section:",this.section);
        console.log("Chunks:",this.chunks.length);
        
        if (this.chunks.length === 0){
            this.chunks = this.chunkFiles(this.FL.length);
            console.dir(this);
        }
        if (this.section === this.chunks.length){
            return;
        }
        
        var fd = new FormData();
        var i;
        for (i=this.chunks[this.section].start; i<=this.chunks[this.section].end; i+=1){
            fd.append("userfile[]",this.FL[i]);
        }
        fd.append("section",this.section);
        fd.append("title",document.querySelector("input[name='title']").value);
        fd.append("uploader",document.querySelector("input[name='uploader']").value);
        fd.append("key",document.querySelector("input[name='key']").value);
        this.section+=1;
        if (this.id !== 0){
            fd.append("id",this.id);
            fd.append("gid",this.gid);
        }
        this.uploadSelection(fd, this.dealWithResponse);
        
    };
    UploadHandler.prototype.dealWithResponse = function(){
        var a = this.apiResponse();
        if (a !== false && a.syntaxError != true) {
           console.log("received data:");
           console.log(a);
           this.id = a.id;
           this.gid = a.gid;
           this.uploadAll();
        } else {
            console.error("ERROR:")
            console.error(a);
        }
        
    };
    UploadHandler.prototype.retFalse = function(){
        return false;
    };
    UploadHandler.prototype.apiResponse = function(){
        if (this.xhr.readyState == 4) {
            if (this.xhr.status == 200) {
                var a={};
                try{
                    a = JSON.parse(this.xhr.responseText);
                } catch(e){
                    a.syntaxError = true;
                    a.data = this.xhr.responseText;
                }
                
                return a;
            }
            console.error("Server communication failed: " + this.xhr.status + " (" + this.xhr.responseText + ")");
        }
        return false;
    };
    UploadHandler.prototype.uploadSelection = function(a,c){
        //a = data
        //c = callback
        this.xhr.open("POST", "uploadSection.php",true);
        console.log("Sending Data:");
        console.log(JSON.stringify(a));
        this.xhr.onreadystatechange = c.bind(this);
        this.xhr.send(a);
    };
    UploadHandler.prototype.chunkFiles = function(len){
        var retVal = [];
        var perTransfer = 10;
        var i;
        for (i = 0; (i)*perTransfer <= len ; i+=1){
            retVal[i] = {
                start: i*perTransfer,
                end:Math.min((i+1)*perTransfer-1,len-1)
            };
        }
        return retVal;
    };
	var selDiv = "";
		
	document.addEventListener("DOMContentLoaded", init, false);
	
	function init() {
		document.querySelector('#files').addEventListener('change', handleFileSelect, false);
		selDiv = document.querySelector("#selectedFiles");
        
        document.querySelector("form").addEventListener('submit',function(e){
            e.preventDefault();
            console.log("Uploading");
            var uploader = new UploadHandler();
            uploader.uploadAll();
        });
	}
		
	function handleFileSelect(e) {
		
		if(!e.target.files || !window.FileReader) return;

		selDiv.innerHTML = "";
		
		var files = e.target.files;
		var filesArr = Array.prototype.slice.call(files);
		filesArr.forEach(function(f,i) {
			var f = files[i];
			if(!f.type.match("image.*")) {
				return;
			}

			var reader = new FileReader();
			reader.onload = function (e) {
				var html = "<img src=\"" + e.target.result + "\">" + f.name + "<br clear=\"left\"/>";
				selDiv.innerHTML += html;				
			}
			reader.readAsDataURL(f); 
		});
		
	}
	</script>

</body>
</html>