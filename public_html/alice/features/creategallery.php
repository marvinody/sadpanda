<?php


/*
Assumes user is logged in already and authenticated
*/
function creategallery($mysqli, $categories, $gname, $gname_jpn, $public, $publiccat, $comment, $tos){
    
    $errors = [];
    $errors[] = check_name($gname, $gname_jpn);
    $errors[] = check_comment($comment);
    $errors[] = check_publiccat($categories, $public, $publiccat);
    $errors[] = check_tos($tos);
    //returning "null" in the functions adds a bunch of blank stuff in arr
    //so let's get rid of it
    $errors = array_values(array_filter($errors));
    $gid = 0;
    if(count($errors) == 0){
        //no errors, so create it?
        $gid = generate_gid($mysqli);
        create_album($mysqli,$gid,$gname,$gname_jpn,$public,$publiccat,$comment);
        
    }

    return array($errors,$gid);
}


function create_album($mysqli,$gid,$gname,$gname_jpn,$public,$publiccat,$comment){
    $query = "INSERT INTO albums (name,gid,jap_name,published,category,uploaderId) VALUES (?,?,?,?,?,?)";
    
    $gname = $gname;
    $jpn_name = $gname_jpn;
    $is_public = isset($public);
    $cat = $is_public ? $publiccat : 1;
    
    if(!$stmt = $mysqli->prepare($query)){
        throw new RuntimeException('Database error mnca1. Inform Admin');
    }
    if(!$stmt->bind_param('sisiii',$gname,$gid,$jpn_name,$is_public,$cat,$_SESSION['user']['id'])){
        throw new RuntimeException('Database error mnca2. Inform Admin');
    }
    if(!$stmt->execute()){
        throw new RuntimeException('Database error mnca3. Inform Admin');
    }
    $stmt->close();
    
    $query = "INSERT INTO comments (gid,comment,userId,isOp) VALUES (?,?,?,?)";
    if(!$stmt = $mysqli->prepare($query)){
        throw new RuntimeException('Database error mnca4. Inform Admin');
    }
    if(!$stmt->bind_param('isii',$gid,$comment,$_SESSION['user']['id'],$a = 1)){
        throw new RuntimeException('Database error mnca5. Inform Admin');
    }
    if(!$stmt->execute()){
        echo $mysqli->error;
        throw new RuntimeException('Database error mnca6. Inform Admin');
    }
    $stmt->close();
    
}
function _generate_gid(){
    return mt_rand(10000000,99999999);
}
function generate_gid($mysqli){
    $isOk = false;
    $query = "SELECT id FROM `albums` where gid=?";
    $gid = 0;
    $retVal = $gid;
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param('i',$gid);
    $stmt->bind_result($res);
    while(!$isOk){//while not ok
        $gid = _generate_gid();
        $retVal = $gid;
        $stmt->execute();
        if($stmt->fetch()){
            //found matching set,
            $isOk = false;
        } else {//no match
            $isOk = true;
        }
        
    }
    return $retVal;
}
function check_name($gname,&$gname_jpn){
    if(!isset($gname) || empty($gname))
        return "Gallery title cannot be empty!";
    if(strlen($gname)>512)
        return "Gallery title (eng/jap) must be less than 500 characters!";
    if(isset($gname_jpn) && !empty($gname_jpn)){
        if(strlen($gname_jpn)>512)
            return "Gallery title (eng/jap) must be less than 500 characters!";
    } else {
        $gname_jpn = NULL;//to insert null into 
    }
        
        
            
    return null;
}
function check_comment($comment){
    
    if(!isset($comment) || empty($comment))
        return "You must have a description (just make up something short)";
        
    if(strlen($comment)>2000)
        return "Description must be less than 2000 characters";
        
    return null;
}
function check_publiccat($categories, $public, $publiccat){
    if(!isset($public))return null;
    if($publiccat < 1)return "Invalid category";//should never be <=0
    for($i=0;$i<count($categories);$i++){
        if($categories[$i]['id'] == $publiccat)
            return null;//id matches one, no error
    }
    return "Invalid category";//no match
    
}
function check_tos($tos){
    if(!isset($tos) || empty($tos) || $tos==false)
        return "Please make sure you click the ToS because I don't know why it's required";
    return null;
}
