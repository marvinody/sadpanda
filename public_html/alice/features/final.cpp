/* Copyright 2008, 2010, Oracle and/or its affiliates. All rights reserved.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

There are special exceptions to the terms and conditions of the GPL
as it is applied to this software. View the full text of the
exception in file EXCEPTIONS-CONNECTOR-C++ in the directory of this
software distribution.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/* Standard C++ includes */
#include <stdlib.h>
#include <iostream>
#include <sstream>

/*
  Include directly the different
  headers from cppconn/ and mysql_driver.h + mysql_util.h
  (and mysql_connection.h). This will reduce your build time!
*/
#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include <string>
#include <string.h>
#include <vector>
using namespace std;

void tokenize(char *str,int length, vector<string> *ns, vector<string> *other){
  int current = 0;
  bool hasSeenColon = false;
  ostringstream tagStr;
  while(current != length){
    switch(str[current]){
      case ':':
        hasSeenColon = true;
        tagStr << ':';
        current++;
        break;
        
      case '$'://if we see $, just end it already
        if(hasSeenColon){
          ns->push_back(tagStr.str());
          tagStr.clear();
          tagStr.str("");
          ++current;
          hasSeenColon = false;
        } else {
          tagStr << str[current++];
        }
        
        break;
        
      case ' ':
        if(hasSeenColon){
          char *c = find(str + current, str+length, ':' );
          char *d = find(str + current, str+length, '$' );
          int c_ind = c - str,
              d_ind = d - str;
          /*so we know we've seen a colon in the past. if we see another colon,
          we want to stop the search here right now.
          if we see a $ BEFORE that colon, we want to end it at the $ because the colon is osmething else
          if we see neither or anything else, we want to end it right now
          */
          if(d_ind < c_ind){//end now
            tagStr << " ";//add this to string
            current++;
          } else {
            ns->push_back(tagStr.str());
            tagStr.clear();
            tagStr.str("");
            ++current;
            hasSeenColon = false;
          }
          
          
        } else {
          other->push_back(tagStr.str());
          tagStr.clear();
          tagStr.str("");
          ++current;
        }
        break;
        
      default:
        tagStr << str[current++];
        break;
        
        
    }
    
    
  }
  other->push_back(tagStr.str());
}

void printVector(vector<string> vec){
  int len=vec.size();
  for(int i=0;i<len;i++){
    cout << vec[i] << endl;
  }
  
}

void debug(vector<string> ns, vector<string> other){
  int len = ns.size();
  cout << "ns:" << endl;
  for(int i=0;i<len;i++)
    cout << ns[i] << endl;
  
  len = other.size();
  cout << "other:" << endl;
  for(int i=0;i<len;i++)
    cout << other[i] << endl;
  
}
void split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
}
int main(int argc, char *argv[]){
  if(argc < 2){
    std::cerr << "Must be called with at least one tag, space separated" << endl;
    return 1;
  }
  if(argc != 2 ){
    std::cerr << "Must be called with all tags enclosed in 1 quote" << endl;
    return 1;
  }
  size_t length = strlen(argv[1]);
  if(length<3){
    std::cerr << "Must have more characters to search" << endl;
    return 2;
  }
  vector<string> namespaceStrings;
  vector<string> otherStrings;
  tokenize(argv[1],length,&namespaceStrings,&otherStrings);
  //debug(namespaceStrings,otherStrings);
  //return 3;
  
  vector<string> queries;
  int numNeeded = 0;
  vector<string> params;
  /*Do namespace query first*/
  /*
  SELECT A.* 
  FROM albumTags at, albums A, tags t,namespace ns
  WHERE 
    at.tagId = t.id AND 
    (t.tag IN ("fumo")) AND 
    (ns.namespace IN ("location")) AND
    A.gid = at.gid AND
    A.hidden = 0 AND
    A.published = 1 AND
    A.expunged = 0

  GROUP BY A.id 
  HAVING COUNT(DISTINCT t.tag) = 1
  */
  
  if(namespaceStrings.size() > 0){
    ostringstream innerInnerQueryTag;
    ostringstream innerInnerQueryNS;
    string sep = "";
    int len = namespaceStrings.size();
    vector<string> namespaceNS;
    for(int i=0;i<len;i++){
      if(namespaceStrings[i].empty())continue;
      if(namespaceStrings[i].size() < 7)continue;//3 for ns, :, 3 for tag
      string ns,tag;
      vector<string> delimmed;
      split(namespaceStrings[i],':',delimmed);
      ns = delimmed[0];
      tag= delimmed[1];
      innerInnerQueryTag << sep << "" << "?" << "";
      innerInnerQueryNS << sep << "" << "?" << "";
      namespaceNS.push_back(ns);
      params.push_back(tag);
      sep = ",";
    }
    len = namespaceNS.size();
    for(int i=0;i<len;i++){
      params.push_back(namespaceNS[i]);
    }
    if(sep!=""){//do we have some entries?
      ostringstream innerInnerQuery;
      innerInnerQuery << 
      "SELECT A.* " 
      "FROM albumTags at, albums A, tags t,namespace ns "
      "WHERE "
        "at.tagId = t.id AND "
        "(t.tag IN (" << innerInnerQueryTag.str() << ")) AND "
        "(ns.namespace IN (" << innerInnerQueryNS.str() << ")) AND "
        "A.gid = at.gid AND "
        "A.hidden = 0 AND "
        "A.published = 1 AND "
        "A.expunged = 0 "
      "GROUP BY A.id "
      "HAVING COUNT(DISTINCT t.tag) = 1";
      queries.push_back(innerInnerQuery.str());
      numNeeded++;
      
    }  
  
  }
  /*TITLE QUERY*/
  /*
  SELECT A.* 
  FROM albums A
  WHERE 
    A.name LIKE '%sanae%'
  */
  int otherSize = otherStrings.size();
  if(otherSize > 0){
    ostringstream innerInnerQueryLike;
    string sep = "";
    for(int i=0;i<otherSize;i++){
      if(otherStrings[i].empty())continue;
      if(otherStrings[i].size()<3)continue;
      innerInnerQueryLike << sep << "A.name LIKE " << "" << "?" << "";
      params.push_back("%"+otherStrings[i]+"%");
      sep = " AND ";
    }
    if(sep!=""){
      ostringstream innerInnerQuery;
      innerInnerQuery << 
        "SELECT A.* " 
        "FROM albums A "
        "WHERE A.hidden = 0 AND A.published = 1 AND A.expunged = 0 AND " << innerInnerQueryLike.str();
      queries.push_back(innerInnerQuery.str());
    }
    
  }
  /*TAG QUERY*/
  /*
  SELECT A.* 
  FROM albumTags at, albums A, tags t 
  WHERE 
    at.tagId = t.id AND 
    (t.tag IN ("fumo")) AND 
    A.gid = at.gid AND
    A.hidden = 0 AND
    A.published = 1 AND
    A.expunged = 0

  GROUP BY A.id 
  HAVING COUNT(DISTINCT t.tag) = 1
  */
  if(otherSize > 0){
    ostringstream innerInnerQueryTags;
    string sep = "";
    for(int i=0;i<otherSize;i++){
      if(otherStrings[i].empty())continue;
      if(otherStrings[i].size()<3)continue;
      innerInnerQueryTags << sep << "" << "?" << "";
      params.push_back(otherStrings[i]);
      sep = ",";
    }
    if(sep!=""){
      ostringstream innerInnerQuery;
      innerInnerQuery << 
        "SELECT A.* "
        "FROM albumTags at, albums A, tags t "
        "WHERE "
          "at.tagId = t.id AND "
          "(t.tag IN (" << innerInnerQueryTags.str() << ")) AND "
          "A.gid = at.gid AND "
          "A.hidden = 0 AND "
          "A.published = 1 AND "
          "A.expunged = 0 "
        "GROUP BY A.id "
        "HAVING COUNT(DISTINCT t.tag) = 1 ";
      queries.push_back(innerInnerQuery.str());
      numNeeded++;
    }
    
  }
  
  //debug(namespaceStrings,otherStrings);
  int qlen = queries.size();
  ostringstream innerInnerQuery;
  string sep = "";
  for(int i=0;i<qlen;i++){
    innerInnerQuery << sep << "(" << queries[i] << ")";
    sep = "UNION ALL";
  }
  
  ostringstream innerQuery;
  innerQuery <<
  "SELECT * "
  "FROM "
  "( "
    << innerInnerQuery.str() <<
  ") AS temp "
    "GROUP BY temp.gid "
    "HAVING COUNT(*) >= " << numNeeded;
  
 
  
  ostringstream queryStream;
  queryStream << 
  "SELECT foundAlbums.gid,foundAlbums.name,foundAlbums.cover_thumb,foundAlbums.category,u.user as uploader,COUNT(p.id) as pages "
  "FROM ( "
    << innerQuery.str() <<
  ") "
    "foundAlbums, " 
    "users u, "
    "pages p "
  "WHERE "
      "foundAlbums.uploaderId = u.id "
    "AND p.gid=foundAlbums.gid "
    "AND p.hidden=0 "
    "AND p.actualPageOrder!=0 "
  "GROUP BY foundAlbums.gid";
  
  
  
  /*
SELECT foundAlbums.gid,foundAlbums.name,foundAlbums.cover_thumb,foundAlbums.category,u.user,COUNT(p.id) as pages 
FROM 
  (
  SELECT *
  FROM
  (
      (
        SELECT A.* 
        FROM albumTags at, albums A, tags t 
        WHERE 
          at.tagId = t.id AND 
          (t.tag IN ("fumo")) AND 
          A.gid = at.gid AND
          A.hidden = 0 AND
          A.published = 1 AND
          A.expunged = 0
          
        GROUP BY A.id 
        HAVING COUNT(DISTINCT t.tag) = 1
      )
      UNION ALL
      (
        SELECT A.* 
        FROM albums A
        WHERE 
          A.name LIKE '%sanae%'
      )
      UNION ALL
      (
        SELECT A.* 
        FROM albumTags at, albums A, tags t,namespace ns
        WHERE 
          at.tagId = t.id AND 
          (t.tag IN ("fumo")) AND 
          (ns.namespace IN ("location")) AND
          A.gid = at.gid AND
          A.hidden = 0 AND
          A.published = 1 AND
          A.expunged = 0
          
        GROUP BY A.id 
        HAVING COUNT(DISTINCT t.tag) = 1
      )
    ) AS temp
    GROUP BY temp.gid
    HAVING COUNT(*) >= 3
  )
  foundAlbums, 
  users u,
	pages p
WHERE 
	foundAlbums.uploaderId = u.id 
  AND p.gid=foundAlbums.gid 
  AND p.hidden=0 
  AND p.actualPageOrder!=0
GROUP BY foundAlbums.gid

  */
  
  

  string query = queryStream.str();
try {
  sql::Driver *driver;
  sql::Connection *con;
  sql::ResultSet *res;
  sql::PreparedStatement *pstmt;
  sql::ResultSetMetaData *resMeta;

  /* Create a connection */
  driver = get_driver_instance(); 
  con = driver->connect("tcp://127.0.0.1:3306", "root", "albino");
  /* Connect to the MySQL test database */
  con->setSchema("sadpanda_albums");

  //cout << query << endl;
  /* '?' is the supported placeholder syntax */
  pstmt = con->prepareStatement(query);
  //here's where it gets confusing, again.
  //we need to fill in the array with namespaceTags, then namespaceNS, then titleTags, then regularTags
  /*for (int i = 1; i < argc; i++) {
    //cout << i << ": " << argv[i] << endl;
    pstmt->setString(i, argv[i]);
  }*/
  int plen = params.size();
  for(int i=0;i<plen;i++){
    pstmt->setString(i+1, params[i]);
  }
  
  
  
  
  res = pstmt->executeQuery();
  cout << res->rowsCount() << endl;
  //http://stackoverflow.com/questions/13700751/get-type-of-the-returned-data-in-mysql-query
  resMeta = res->getMetaData();
  int colCount = resMeta->getColumnCount();
  vector<string> cols;
  for(int i=0;i<colCount;i++){
    cols.push_back(resMeta->getColumnLabel(i+1));
  }
  cout << colCount << endl;
  while(res->next()){
    for(int i=0;i<colCount;i++){
      cout << cols[i] << endl;
      cout << res->getString(i+1) << endl;
      
    }
  }
  
  
  delete pstmt;

  delete con;

} catch (sql::SQLException &e) {
  cout << "# ERR: SQLException in " << __FILE__;
  cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
  cout << "# ERR: " << e.what();
  cout << " (MySQL error code: " << e.getErrorCode();
  cout << ", SQLState: " << e.getSQLState() << " )" << endl;
}

cout << endl;

return EXIT_SUCCESS;
}
