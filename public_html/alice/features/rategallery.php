<?php

function rategallery($gid,$mysqli,$rating){
     /*
    received data
    apikey:"341ba9d027ae772a1d4c"
    apiuid:-1
    gid:40732230
    method:"rategallery"
    rating:10
    token:"294e5a30f5"
    
    1<=r<=4
    5<=g<=8
    9<=b<=10
    
    return 
    {"rating_avg":4.67,
    "rating_usr":5,
    "rating_cnt":334,
    "rating_cls":"ir irb"}
    */
    if(!isset($gid)||empty($gid)){return array('error'=>'empty gallery id');}
    if(!isset($_SESSION['gallery']['gid'])||empty($_SESSION['gallery']['gid'])){
        return array('error'=>'empty gallery id');
    }
    if(!isset($_SESSION['user']['id'])||empty($_SESSION['user']['id'])){
        return array('error'=>'You must be logged in');
    }
    if($_SESSION['gallery']['gid']!=$gid){return array("error"=>"gid seems wrong");}
    if(!isset($rating)||empty($rating)||$rating<1||$rating>10){
        return array("error"=>"Incorrect or missing rating");
    }
    $query = "INSERT INTO albumVotes (gid,userId,rating) values (?,?,?) 
            ON DUPLICATE KEY UPDATE
            rating=?;";

    if(!$stmt = $mysqli->prepare($query)){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        return array('error'=>'Database error, try again later or yell at admin');
    }
    if(!$stmt->bind_param("iiii",$_SESSION['gallery']['gid'],$_SESSION['user']['id'],$rating,$rating)){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        return array('error'=>'Database error, try again later or yell at admin');
    }

    if(!$stmt->execute()){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        return array('error'=>'Database error, try again later or yell at admin');
    }
    $stmt->close();
    $query = "SELECT COUNT( * ) FROM  `albumVotes` WHERE gid =?";
	$stmt=$mysqli->prepare($query);
	$stmt->bind_param("i", $_SESSION['gallery']['gid']);
    $stmt->execute();
    $stmt->bind_result($totalVotes);
    $stmt->fetch();
	$stmt->close();
    $scaledRating = $rating/2;
    $newAverage = 0;
    if(empty($_SESSION['gallery']['userVote']['voted'])){
        $newAverage = getNewAverageIfNotRatedBefore($_SESSION['gallery']['averageVotes'],$scaledRating,$totalVotes);
    } else {
        $newAverage = getNewAverageIfRatedBefore($_SESSION['gallery']['averageVotes'],$_SESSION['gallery']['userVote']['vote'],$scaledRating,$totalVotes);
    }
    
    $_SESSION['gallery']['userVote'] = array(
        'vote'=>$scaledRating,
        'voted'=>1,
    );
    $_SESSION['gallery']['averageVotes'] = $newAverage;
    $_SESSION['gallery']['numVotes'] = $totalVotes;
    return array(
        'rating_avg'=>$newAverage,
        'rating_usr'=>$scaledRating,
        'rating_cnt'=>$totalVotes,
        'rating_cls'=>getRatingClass($rating),
    );
}
function getNewAverageIfRatedBefore($oldAverage, $oldRating, $newRating, $totalVotes){
    //calculate total points, sub old points, add new points, divide again. math should work
    return ($oldAverage*$totalVotes - $oldRating + $newRating)/$totalVotes;
}
function getNewAverageIfNotRatedBefore($oldAverage,$newRating,$totalVotes){
    //redo the weights, pretty much
    return $oldAverage*($totalVotes-1)/$totalVotes + $newRating/$totalVotes;
}
function getRatingClass($rating){ 
    $class = "ir ";
    if($rating >= 1 && $rating <= 4){ $class.="irr";}
    if($rating >= 5 && $rating <= 8){ $class.="irg";}
    if($rating >= 9 && $rating <= 10){$class.="irb";}
    return $class;
}