<?php
//adds comment to a particular GID. assumes logon already. check before you call me
function addcomment($comment,$mysqli){
    $comments = $_SESSION['gallery']['comments'];
    if(!isset($_SESSION['gallery']['gid'])||empty($_SESSION['gallery']['gid'])){
        error_log("Trying to add a comment with blank session gid");
        return $_SESSION['gallery']['comments'];
    }
    if(!isset($comment)||empty($comment)){
        //empty comment
        return $comments;
    }
    if(!isset($_SESSION['user']['id'])||empty($_SESSION['user']['id'])){
        error_log("Trying to comment while not logged in. Bypassed webapge, care");
        return $comments;
    }
    $fixedComment = (trim($comment) );
    $query = "SELECT userId,id,comment FROM comments WHERE gid=".$_SESSION['gallery']['gid']." AND isOp=0 ORDER BY id DESC LIMIT 1";
    if ($stmt = $mysqli->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result($userId,$id,$oldComment);
		$stmt->fetch();
        $stmt->close();
        if($userId == $_SESSION['user']['id']){//if the most recent comment user == logged in user
            //update
            $newCom = $oldComment."\n".$fixedComment;
            $query = "UPDATE comments SET comment=? WHERE id=?";
            if(!$stmt = $mysqli->prepare($query)){
                error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
                return array('error'=>'Database error, try again later or yell at admin');
            }
            if(!$stmt->bind_param("si",$newCom,$id)){
                error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
                return array('error'=>'Database error, try again later or yell at admin');
            }

            if(!$stmt->execute()){
                error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
                return array('error'=>'Database error, try again later or yell at admin');
            }
            $stmt->close();
            $totalComments = count($comments);
            $_SESSION['gallery']['comments'][$totalComments-1]['comment'] = nl2br(htmlEntities($newCom, ENT_QUOTES));
            $_SESSION['gallery']['comments'][$totalComments-1]['timestamp'] = time();
            $comments[$totalComments-1]['comment'] = nl2br(htmlEntities($newCom, ENT_QUOTES));
            $comments[$totalComments-1]['timestamp'] = time();
            
            
        } else {
            //make new comment
            $query = "INSERT INTO comments (gid,userId,comment) values (?,?,?)";

            if(!$stmt = $mysqli->prepare($query)){
                error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
                return array('error'=>'Database error, try again later or yell at admin');
            }
            if(!$stmt->bind_param("iis",$_SESSION['gallery']['gid'],$_SESSION['user']['id'],$fixedComment)){
                error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
                return array('error'=>'Database error, try again later or yell at admin');
            }

            if(!$stmt->execute()){
                error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
                return array('error'=>'Database error, try again later or yell at admin');
            }
            $stmt->close();
            $insertId = $mysqli->insert_id;
            $result = $mysqli->query("SELECT UNIX_TIMESTAMP(`timestamp`) as timestampInt FROM comments where id=$insertId");
            $row = $result->fetch_assoc();
            $comments[] = addSessionComment($insertId,$fixedComment,$row['timestampInt'],$_SESSION['user']['votePower']);
            
        }
        return $comments;
    }
    
}
function addSessionComment($id,$comment,$timestamp,$vote){
    $cArr =  array(
        'cid'=>$id,
        'comment'=>$comment,
        'user'=>$_SESSION['user']['username'],
        'userId'=>$_SESSION['user']['id'],
        'timestamp'=>$timestamp,
        'baseVote'=>$vote,
        'totalVote'=>$vote,
        'isOp'=>0,
    );
    $_SESSION['gallery']['comments'][] = $cArr;
    return $cArr;
}
