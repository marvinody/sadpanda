<?php

if(!defined('SITE_NAME')) {
   die('Direct access not permitted');
}

function addfavorite($mysqli){
    //assume that gid is ok since gallery info was collected before this
    //return errors if not ok and then display in fav HTML
    if(!isset($_SESSION['gallery']['gid'])){return array('error'=>"Invalid gallery to favorite. Make sure you're on the album page!");}
    $gid = $_SESSION['gallery']['gid'];
    $favCat = $_POST['favcat'];
    $favNote = $_POST['favnote'];
    if(!is_numeric($favCat)){
        return array('error'=>'Invalid option for category');
    }//know it's numeric
    if($favCat < 0 || $favCat > 9 ){
        return array('error'=>'Invalid category for favorite');
    }//know it's 0<=x<=9
        
    
    if(isset($favNote) && !empty($favNote) && (strlen($favNote)>200)){
        return array('error'=>'Note is invalid');
    }
        
    $query = "INSERT INTO favorites (gid,userId,favoriteList,note) values (?,?,?,?) 
            ON DUPLICATE KEY UPDATE
            favoriteList=?, note=?;";

    if(!$stmt = $mysqli->prepare($query)){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        return array('error'=>'Database error, try again later or yell at admin');
    }
    if(!$stmt->bind_param("iiisis",$gid,$_SESSION['user']['id'],$favCat,$favNote,$favCat,$favNote)){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        return array('error'=>'Database error, try again later or yell at admin');
    }

    if(!$stmt->execute()){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        return array('error'=>'Database error, try again later or yell at admin');
    }
    $res = $mysqli->affected_rows;
    $stmt->close();
    $_SESSION['gallery']['favorites']['userFav'] = array(
        'favNote'=>$favNote,
        'favCat'=>$favCat,
    );
    $totalFavs = $_SESSION['gallery']['favorites']['numFavs'];
    if($res===1){//was inserted
      $totalFavs++;
    } else if($res === 2 || $res === 0){//updated || set to current values
      //do nothing
    }
    $_SESSION['gallery']['favorites']['numFavs'] = $totalFavs;
    
    $favLinkString = "Favorites $favCat";
    $html = '<div class=\"i\" style=\"background-image:url(\''.BASE_HREF.'/images/fav.png\'); background-position:0px '
            .(-2-(19*$favCat)).'px; margin-left:10px\" title=\"Favorites '.$favCat.'\"></div>';
    $favCountString = $totalFavs.($totalFavs == 1 ? "time" : "times");
    $_SESSION['gallery']['favorites']['strFav'] = $favCountString;
    loadFavoriteSuccessHtml($_SESSION['gallery']['url'],$_SESSION['gallery']['title'],$html,$favLinkString,$favCountString);
  
    return 0;//need to return empty value for gallerypopup handler
}
function deleteFavorite($gid,$mysqli){
    $query = "DELETE FROM favorites WHERE gid=? AND userId=?";
    if(!$stmt = $mysqli->prepare($query)){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        return array('error'=>'Database error, try again later or yell at admin');
    }
    if(!$stmt->bind_param("ii",$gid,$_SESSION['user']['id'])){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        return array('error'=>'Database error, try again later or yell at admin');
    }

    if(!$stmt->execute()){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        return array('error'=>'Database error, try again later or yell at admin');
    }
    $stmt->close();
    unset($_SESSION['gallery']['favorites']['userFav']);
    $totalFavs = $_SESSION['gallery']['favorites']['numFavs'];
    $totalFavs--;
    $_SESSION['gallery']['favorites']['numFavs'] = $totalFavs;
    $favLinkString = '<img src=\"'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif\" /> Add to Favorites';
    $html = '';
    $favCountString = $totalFavs.($totalFavs == 1 ? "time" : "times");
    $_SESSION['gallery']['favorites']['strFav'] = $favCountString;
    loadFavoriteSuccessHtml($_SESSION['gallery']['url'],$_SESSION['gallery']['title'],$html,$favLinkString,$favCountString);
}



function getFavorite($gid,$mysqli){
  
}


function loadFavoriteSuccessHtml($url, $title, $html, $favLinkString, $favCountString){
    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
    echo '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">';
    echo '<head>';
    echo '<title>'.SITE_NAME.'</title>';
    echo '<link rel="stylesheet" type="text/css" href="'.BASE_HREF.'/css/g.css" />';
    echo '';
    echo '</head>';
    echo '<body style="text-align:center">';
    echo '<script type="text/javascript">';
    echo 'function popUp(URL,w,h) {';
    echo '	window.open(URL,"_pu"+(Math.random()+"").replace(/0\./,""),"toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width="+w+",height="+h+",left="+((screen.width-w)/2)+",top="+((screen.height-h)/2));';
    echo '	return false;';
    echo '}';
    echo '</script>';
    echo '';
    echo '<div class="stuffbox" style="width:584px; height:350px; margin:2px auto; text-align:left; padding:8px; font-size:9pt">';
    echo '<div style="width:100%; height:40px; font-size:11pt; font-weight:bold; margin:3px; text-align:center; overflow:hidden">'.$title.'</div>';
    echo '<form id="galpop" action="" method="post">';
    echo '';
    echo '<div style="text-align:center; margin-top:50px">';
    echo '<p>[<a href="'.$url.'" onclick="window.close(); return false">Close Window</a>]</p>';
    echo '';
    echo '<noscript>';
    echo '<p>The requested action has been performed.</p>';
    echo '<p>If you enable JavaScript, this won&#039;t be this awkward next time.</p>';
    echo '</noscript>';
    echo '<script type="text/javascript">';
    echo 'if(window.opener.document.getElementById("favoritelink") != undefined) ';
    echo '{';
    echo '    window.opener.document.getElementById("fav").innerHTML="'.$html.'";';
    echo '    window.opener.document.getElementById("favoritelink").innerHTML="'.$favLinkString.'";';
    echo '    window.opener.document.getElementById("favcount").innerHTML="'.$favCountString.'";';
    echo '}';
    echo 'window.close();';
    echo '';
    echo '';
    echo '';
    echo '';
    echo '</script>';
    echo '';
    echo '</div>';
    echo '';
    echo '</body>';
    echo '</html>';
    
}
