<?php

function taggallery($mysqli,$gid,$tagsString,$vote){
    //assume logged in already cause outer call
    //general logic is to try to update tags, create if needed. then just reload all tags.
    //use same code in gallery.php to recreate html for tags
    $userVote = 5;//dynamically find this TODO
    $totalVote = $userVote*$vote;//sign change or 0;
    
    $tags = explode(",",$tagsString);
    $tagsUpdated = 0;
    for($i=0;$i<count($tags);$i++){
        $combinedTag = $tags[$i];
        $test = explode(":",$combinedTag);
        if(count($test)==1){continue;}
        list($namespace,$tag) = explode(":",$combinedTag);
        $namespace = trim($namespace);
        $tag = trim($tag);
        if(strlen($tag) <= 3 || strlen($namespace) <= 3){continue;}
        $nsId = _getNameSpaceId($mysqli,$namespace);
        $tagId = _getTagId($mysqli,$nsId,$tag);
        
        updateTag($mysqli,$tagId,$gid,$_SESSION['user']['id'],$vote);
        $tagsUpdated++;
    }
    if($tagsUpdated == 0){
        return array("error"=>"Could not find any valid tags (NS & Tag at least 3 letters long)");
    }
    require_once 'core/galleryInfo.php';
    $gallery = getGallery($gid,$mysqli,true);
    $tagHTML = makeTagList($gallery);
    
    
    return array("tagpane"=>$tagHTML);
    
}

function _checkIfPossibleForTag($gallery,$namespace,$j){
    //could there be a tag in this namespace (given X voted ups we know of) that we liked?
    if($j >= count($gallery['userTags'])){return false;}
    return $namespace==$gallery['userTags'][$j]['namespace'];
}
function _getClassListForTagOnUserVote($mightHaveTag,$tag,$userTags,$j){
    if(!$mightHaveTag){return "";}
    $tags = $userTags[$j]['tags'];
    for($i=0;$i<count($tags);$i++){
        if($tag == $tags[$i]['tag']){
            if($tags[$i]['votes']==0){return "";}
            return $tags[$i]['votes'] > 0 ? "tup" : "tdn";
        }
    }
    return "";
    
}
function makeTagList($gallery){
	$tagVoteForBordered = 5;
	$retVal = '';
    $retVal.='<table>';
    $len = count($gallery['tags']);
    $j = 0;//this is cnt for userTags so we know where we at
    $tags = $gallery['tags'];
    for($i=0;$i<$len;$i++){
        $retVal.='<tr>';
            $retVal.='<td class="tc">'.$tags[$i]['namespace'].':</td>';
            $retVal.='<td>';
            $namespace = $tags[$i]['namespace'];
            $possibleTag = _checkIfPossibleForTag($gallery,$namespace,$j);
            for($k=0;$k<count($tags[$i]['tags']);$k++){
                $tagArr=$tags[$i]['tags'][$k];
                $tagVoteClass = _getClassListForTagOnUserVote($possibleTag,$tagArr['tag'],$gallery['userTags'],$j);
                $class = $tagArr['votes'] > $tagVoteForBordered ? "gt" : "gtl";
                $nameColonTag = $namespace.':'.$tagArr['tag'];
                $retVal.='<div id="td_'.$nameColonTag.'" class="'.$class.'" style="opacity:1.0"><a id="ta_'.$nameColonTag.'" href="'.BASE_HREF.'/tag/'.$nameColonTag.'" class="'.$tagVoteClass.'" onclick="return toggle_tagmenu(\''.$nameColonTag.'\', this)">'.$tagArr['tag'].'</a>';
                $retVal.='</div>';
            }
            if($possibleTag){
                $j++;//cause we know we need to match next namespace
            }
            $retVal.='</td>';


        $retVal.='</tr>';

    }

    $retVal.='</table>';
	return $retVal;
}



/*
INSERT INTO userPowerTotals(userId, userPowerTotal) VALUES (new.userId,2) 
ON DUPLICATE KEY UPDATE votes = (SELECT SUM(*) FROM userPower WHERE userId=new.userId)

CREATE TRIGGER NewTrigger 
AFTER INSERT ON Transactions
FOR EACH ROW
UPDATE Accounts a
   SET a.AccountBalance = 
    (SELECT SUM(TransactionAmount) 
       FROM Transactions
      WHERE AccountID = a.AccountID)
 WHERE a.AccountID = NEW.AccountID;
 */

function updateTag($mysqli,$tagId,$gid,$userId,$vote){
    $query = "INSERT INTO albumTags (tagId, gid, userId, votes) VALUES (?,?,?,?) 
ON DUPLICATE KEY UPDATE votes = votes + ?";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param('iiiii',$tagId,$gid,$userId,$vote,$vote);
    $stmt->execute();
    $stmt->close();
    
}

function _getTagId($mysqli,$nsId,$tag){
    $query = "SELECT id,masterTagId FROM tags WHERE namespaceId=? AND tag=?";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param('is',$nsId,$tag);
    $stmt->bind_result($id,$mtId);
    $stmt->execute();
    $stmt->fetch();
    $stmt->close();
 
    if(empty($mtId) && !($mtId>0) && $id > 0){
        //mtid useless, parent is already id
        return $id;
    } else if($mtId>0){
        return $mtId;
    }
    //else insert
    $query = "INSERT INTO tags (tag,namespaceId) VALUES (?,?)";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param('si',$tag,$nsId);
    $stmt->execute();
    $stmt->close();
    return $mysqli->insert_id;
}

function _getNameSpaceId($mysqli,$namespace){
    $query = "SELECT id FROM `namespace` WHERE namespace=?";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param('s',$namespace);
    $stmt->bind_result($id);
    $stmt->execute();
    $stmt->fetch();
    $stmt->close();
    if($id>0){return $id;}
    
    $query = "INSERT INTO namespace (namespace) VALUES (?)";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param('s',$namespace);
    $stmt->execute();
    $stmt->close();
    return $mysqli->insert_id;
    

}
/*tagpane:"
<table>
    <tr>
        <td class="tc">language:</td>
        <td>
            <div id="td_language:chinese" class="gt" style="opacity:1.0"><a id="ta_language:chinese" href="http://exhentai.org/tag/language%3Achinese" class="" onclick="return toggle_tagmenu('language:chinese', this)">chinese</a></div>
            <div id="td_language:translated" class="gtl" style="opacity:1.0"><a id="ta_language:translated" href="http://exhentai.org/tag/language%3Atranslated" class="" onclick="return toggle_tagmenu('language:translated', this)">translated</a></div>
        </td>
    </tr>
    <tr>
        <td class="tc">artist:</td>
        <td>
            <div id="td_artist:momofuki_rio" class="gtl" style="opacity:1.0"><a id="ta_artist:momofuki_rio" href="http://exhentai.org/tag/artist%3Amomofuki+rio" class="" onclick="return toggle_tagmenu('artist:momofuki rio', this)">momofuki rio</a></div>
        </td>
    </tr>
    <tr>
        <td class="tc">male:</td>
        <td>
            <div id="td_male:bbm" class="gtl" style="opacity:1.0"><a id="ta_male:bbm" href="http://exhentai.org/tag/male%3Abbm" class="" onclick="return toggle_tagmenu('male:bbm', this)">bbm</a></div>
            <div id="td_male:dilf" class="gtl" style="opacity:1.0"><a id="ta_male:dilf" href="http://exhentai.org/tag/male%3Adilf" class="" onclick="return toggle_tagmenu('male:dilf', this)">dilf</a></div>
            <div id="td_male:glasses" class="gtl" style="opacity:1.0"><a id="ta_male:glasses" href="http://exhentai.org/tag/male%3Aglasses" class="" onclick="return toggle_tagmenu('male:glasses', this)">glasses</a></div>
            <div id="td_male:shotacon" class="gtl" style="opacity:1.0"><a id="ta_male:shotacon" href="http://exhentai.org/tag/male%3Ashotacon" class="" onclick="return toggle_tagmenu('male:shotacon', this)">shotacon</a></div>
        </td>
    </tr>
    <tr>
        <td class="tc">female:</td>
        <td>
            <div id="td_female:anal" class="gtl" style="opacity:1.0"><a id="ta_female:anal" href="http://exhentai.org/tag/female%3Aanal" class="" onclick="return toggle_tagmenu('female:anal', this)">anal</a></div>
            <div id="td_female:big_breasts" class="gtl" style="opacity:1.0"><a id="ta_female:big_breasts" href="http://exhentai.org/tag/female%3Abig+breasts" class="" onclick="return toggle_tagmenu('female:big breasts', this)">big breasts</a></div>
            <div id="td_female:blowjob" class="gtl" style="opacity:1.0"><a id="ta_female:blowjob" href="http://exhentai.org/tag/female%3Ablowjob" class="" onclick="return toggle_tagmenu('female:blowjob', this)">blowjob</a></div>
            <div id="td_female:dark_skin" class="gtl" style="opacity:1.0"><a id="ta_female:dark_skin" href="http://exhentai.org/tag/female%3Adark+skin" class="" onclick="return toggle_tagmenu('female:dark skin', this)">dark skin</a></div>
            <div id="td_female:double_penetration" class="gtl" style="opacity:1.0"><a id="ta_female:double_penetration" href="http://exhentai.org/tag/female%3Adouble+penetration" class="" onclick="return toggle_tagmenu('female:double penetration', this)">double penetration</a></div>
            <div id="td_female:drugs" class="gtl" style="opacity:1.0"><a id="ta_female:drugs" href="http://exhentai.org/tag/female%3Adrugs" class="" onclick="return toggle_tagmenu('female:drugs', this)">drugs</a></div>
            <div id="td_female:glasses" class="gtl" style="opacity:1.0"><a id="ta_female:glasses" href="http://exhentai.org/tag/female%3Aglasses" class="" onclick="return toggle_tagmenu('female:glasses', this)">glasses</a></div>
            <div id="td_female:harem" class="gtl" style="opacity:1.0"><a id="ta_female:harem" href="http://exhentai.org/tag/female%3Aharem" class="" onclick="return toggle_tagmenu('female:harem', this)">harem</a></div>
            <div id="td_female:impregnation" class="gtl" style="opacity:1.0"><a id="ta_female:impregnation" href="http://exhentai.org/tag/female%3Aimpregnation" class="" onclick="return toggle_tagmenu('female:impregnation', this)">impregnation</a></div>
            <div id="td_female:maid" class="gtl" style="opacity:1.0"><a id="ta_female:maid" href="http://exhentai.org/tag/female%3Amaid" class="" onclick="return toggle_tagmenu('female:maid', this)">maid</a></div>
            <div id="td_female:nun" class="gtl" style="opacity:1.0"><a id="ta_female:nun" href="http://exhentai.org/tag/female%3Anun" class="tup" onclick="return toggle_tagmenu('female:nun', this)">nun</a></div>
            <div id="td_female:stockings" class="gtl" style="opacity:1.0"><a id="ta_female:stockings" href="http://exhentai.org/tag/female%3Astockings" class="" onclick="return toggle_tagmenu('female:stockings', this)">stockings</a></div>
        </td>
    </tr>
    <tr>
        <td class="tc">misc:</td>
        <td>
            <div id="td_group" class="gtl" style="opacity:1.0"><a id="ta_group" href="http://exhentai.org/tag/group" class="" onclick="return toggle_tagmenu('group', this)">group</a></div>
            <div id="td_mmf_threesome" class="gtl" style="opacity:1.0"><a id="ta_mmf_threesome" href="http://exhentai.org/tag/mmf+threesome" class="" onclick="return toggle_tagmenu('mmf threesome', this)">mmf threesome</a></div>
            <div id="td_tankoubon" class="gtl" style="opacity:1.0"><a id="ta_tankoubon" href="http://exhentai.org/tag/tankoubon" class="" onclick="return toggle_tagmenu('tankoubon', this)">tankoubon</a></div>
        </td>
    </tr>
</table>"*/