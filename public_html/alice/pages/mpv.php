<?php
if(!defined('SITE_NAME')) {
   die('Direct access not permitted');
}


require_once 'core/galleryInfo.php';
$gallery = getGallery($gid,$mysqli);

checkForChangedSetting('inline_set',["ms_n","ms_c","ms_y"],$gallery['multiPageViewer'],$mysqli,"mpv_justify");
checkForChangedSetting('inline_set',["mt_n","mt_y"],$gallery['multiPageViewer'],$mysqli,"mpv_thumbpane");
//$images = getImages($gid,$mysqli);
$images = $gallery['pages'];
echoHeader($gallery['title']);
echo makePanes("thumbs",$thumb_dir,$images);
echo makePanes("images",$picture_dir,$images);
echo "</div>";
echoFooter($thumb_dir,$base_url_no_http,$images,$gid,$gallery['link'],$gallery['multiPageViewer']);

function makePanes($paneName,$dir,$images){
    
    $height = 0;
    $i = 1;
    $return = '<div id="pane_'.$paneName.'">'."\n";
    $smallDivs = '';
    foreach($images as $image){
        $path = $dir.$image['name'];
        
        
        switch($paneName){
            case 'images':
                $smallDivs.=makeImageDiv($i,$image['height'],$image['width']);
                if ($image['height'] > 0){
                    $height+=$image['height'];
                    #echo "<script>console.log('iter:'+".$i."+' with height:'+".$image['height'].");console.log('height:'+".$height.")</script>"."\n";
                } else {
                    die("no height for:".$i);
                }
                
                break;
            case 'thumbs':
                $smallDivs.=makeThumbDiv($i,$image['thumb_height'],$image['thumb_width']);
                $height+=$image['thumb_height'];
                break;
        }
        
        
        $i++;
    }
    #echo "<script>console.log('end of script, height: '+".$height.")</script>"."\n";
    $return .= '<div id="pane_'.$paneName.'_inner" style="height:'.$height.'px">'."\n";
    $return .= $smallDivs;
    $return .= "</div>\n</div>";
    return $return;
}

    

function makeThumbDiv($number,$height,$width){
    return '<div id="thumb_'.$number.'" style="height: '.$height.'px; visibility:hidden;" onclick="document.location=\'#page'.$number.'\'"></div>'."\n";
}    
    
function makeImageDiv($number,$height,$width){
    $margin_style = $_SESSION['prefs']['mpv_justify'] == 'ms_c' ? 'margin:auto;' : '';
    return '<a name="page'.$number.'"></a><div id="image_'.$number.'" class="mi0" style="height: '.$height.'px; visibility:hidden; max-width:'.$width.'px;'.$margin_style.'"></div>'."\n";
}

    /*
function getImages($gid,$mysqli){
    $query = "SELECT shortID,name,page,width,height,thumb_width,thumb_height FROM ".DB_TABLE_PAGES." WHERE gid=".$gid." ORDER by id ASC";
    $images = [];
    if ($stmt = $mysqli->prepare($query)) {
        $stmt->execute();
        $stmt->bind_result($shortID, $name,$page,$width,$height,$thumb_width,$thumb_height);
        while ($stmt->fetch()) {
            $images[] = array (
                'shortID'=>$shortID,
                'name'=>$name,
                'page'=>$page,
                'width'=>$width,
                'height'=>$height,
                'thumb_width'=>$thumb_width,
                'thumb_height'=>$thumb_height,
            );
        }
        $stmt->close();
    }

    return $images;
}*/

function getImgSize($path){
    list($width, $height) = getimagesize($path); 
    return array(
        'width'=>$width,
        'height'=>$height,
    );
}

function generateImageList($dir,$base_url,$gid,$images){
    $i = 1;
    $IL = [];
    $d = $dir==="cooking/" ? "c/" : ( $dir==="thumbnails/" ? "t/" : "t/");
    foreach($images as $image){
        $IL[] = array (
            'n'=>$image['name'],
            't'=>$base_url.$d.$gid."/".$image['shortID'].".jpg",
            'k'=>$image['shortID'],
            'd'=>$image['height']." x ".$image['width'],
            'xres'=>$image['width'],
            'yres'=>$image['height'],
            'dir'=>$dir,
            'z'=>THUMBNAIL_DIR,
            
        );
        $i++;
    }
    $return  = "var imagelist = ".json_encode($IL);
    $return .= "
    var pagecount = ".($i-1)."
    ";
    return $return;
}   

function getKeyForImage($imgPath){
    return substr( sha1_file($imgPath),0,  8 );
}


function echoFooter($thumb_path,$base_url,$images,$gid,$link,$mpvUrl){
    $IL = generateImageList($thumb_path,$base_url,$gid,$images);
    $scale_var =  (isset($_SESSION['prefs']['mpv_justify']) && $_SESSION['prefs']['mpv_justify']=="ms_y") ? "true" : "false";
    $thumbbar_var = (isset($_SESSION['prefs']['mpv_thumbpane']) && $_SESSION['prefs']['mpv_thumbpane']=="mt_y") ? "true" : "false";
    echo '<script type="text/javascript">
    
    var gid='.$gid.';
    var mpvkey = "402934-ff5178b9a0";'.
    $IL.
    'var thumbserver = "https://";
    var base_url = "'.BASE_HREF.'/";
    var img_url = "'.BASE_HREF.'/images/";
    var gallery_url = "'.$link.'/";
    var mpv_url = "'.$mpvUrl.'";
    var always_scale = '.$scale_var.';
    var hide_thumbbar = '.$thumbbar_var.';
    </script>

    <script type="text/javascript" src="/alice/js/mpv.js?v=0.03"></script>

    </body>
    </html>';
    
}
















function echoHeader($title){
    $justify = $_SESSION['prefs']['mpv_justify'];
    $thumbpane = $_SESSION['prefs']['mpv_thumbpane'];
    
    $ms_n_text = $justify == 'ms_n' ? '' : '; opacity:0.5';
    $ms_c_text = $justify == 'ms_c' ? '' : '; opacity:0.5';
    $ms_y_text = $justify == 'ms_y' ? '' : '; opacity:0.5';
    $mt_n_text = $thumbpane == 'mt_n' ? '' : '; opacity:0.5';
    $mt_y_text = $thumbpane == 'mt_y' ? '' : '; opacity:0.5';
    
    $pane_thumb_display = $thumbpane == 'mt_y' ? ';display:none' : '';
    $pane_images_left = $thumbpane == 'mt_y' ? '0' : '225';
    
    
        
    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>'.$title.'</title>
<link rel="stylesheet" type="text/css" href="'.BASE_HREF.'/css/g.css" />
<style type="text/css">

div.mi0{width:100%;min-width:700px;padding:0;margin:0 0 4px;background:#F2EFDF;border:1px solid #E3E0D1}
div.mi1{position:relative;margin:0 auto;padding:0}
div.mi1 img{margin:0 2px;cursor:pointer}
div.mi2{position:relative;float:left;z-index:2;margin-top:-2px}
div.mi3{position:relative;float:right;z-index:2;margin-top:-2px}
div.mi4{position:absolute;font-size:10pt;width:100%;top:0px;left:0px;text-align:center;z-index:1}
div#bar1{height:1px;width:100%}
div#bar2{position:relative;float:right;width:35px;height:1px}
div#bar3{position:absolute;top:0px;left:0px;width:35px;height:100px;z-index:3;text-align:right}
div#bar3 img{cursor:pointer;margin:3px}
div#pane_outer{width:99%;height:99%;margin:0;padding:0;position:relative}
div#pane_thumbs{position:absolute;top:0px;left:0px;width:220px;height:500px;overflow-x:hidden;overflow-y:scroll'.$pane_thumb_display.'}
div#pane_thumbs_inner{width:100%}
div#pane_thumbs_inner div{cursor:pointer;padding:0;margin:0}
div#pane_images{position:absolute;top:0px;left:'.$pane_images_left.'px;width:700px;height:500px;overflow:hidden}
div#pane_images_inner{width:100%}

</style>

</head>
<body style="text-align:center">
<script type="text/javascript">
function popUp(URL,w,h) { 
	window.open(URL,"_pu"+(Math.random()+"").replace(/0\./,""),"toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width="+w+",height="+h+",left="+((screen.width-w)/2)+",top="+((screen.height-h)/2));
	return false;
}
</script>

<div id="pane_outer">
	<div id="bar1">
		<div id="bar2">
			<div id="bar3">
				<img src="'.BASE_HREF.'/images/mpvc.png" onclick="action_gallery()" title="Close Image Viewer" />
				<img src="'.BASE_HREF.'/images/mpvf.png" onclick="do_fullscreen()" style="margin-top:20px" title="Go Fullscreen - F11 or ESC to cancel" />
				<img src="'.BASE_HREF.'/images/mpvsn.png" onclick="action_set(\'ms_n\')" style="margin-top:20px'.$ms_n_text.'" title="Align Left, Scale Down Only" />
				<img src="'.BASE_HREF.'/images/mpvsc.png" onclick="action_set(\'ms_c\')" style="margin-top: 5px'.$ms_c_text.'" title="Align Center, Scale Down Only" />
				<img src="'.BASE_HREF.'/images/mpvsy.png" onclick="action_set(\'ms_y\')" style="margin-top: 5px'.$ms_y_text.'" title="Align Center, Scale To Fit" />
				<img src="'.BASE_HREF.'/images/mpvtn.png" onclick="action_set(\'mt_n\')" style="margin-top:20px'.$mt_n_text.'" title="Show Thumbnail Pane" />
				<img src="'.BASE_HREF.'/images/mpvty.png" onclick="action_set(\'mt_y\')" style="margin-top: 5px'.$mt_y_text.'" title="Hide Thumbnail Pane" />
			</div>
		</div>
	</div>';
}

