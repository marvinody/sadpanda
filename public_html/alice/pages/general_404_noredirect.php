<?php
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/core/config.php'); 
echo "
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">
<head>
<title>404: Page not found - ".SITE_NAME."</title>
<link rel=\"stylesheet\" type=\"text/css\" href=\"".BASE_HREF."/css/g.css\" />

</head>
<body style=\"text-align:center\">
<script type=\"text/javascript\">
function popUp(URL,w,h) { 
	window.open(URL,\"_pu\"+(Math.random()+\"\").replace(/0\./,\"\"),\"toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=\"+w+\",height=\"+h+\",left=\"+((screen.width-w)/2)+\",top=\"+((screen.height-h)/2));
	return false;
}
</script>

<div class=\"d\">
<p>The requested page has been removed, or is unavailable.</p>
<p>Close this at your leisure</p>
</div>



</body>
</html>
";
?>
