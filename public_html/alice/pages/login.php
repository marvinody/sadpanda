<?php
if(!defined('SITE_NAME')) {
   die('Direct access not permitted');
}
session_start();
if(isset($_SESSION['user']) && isset($_SESSION['user']['id']) && $_SESSION['user']['id']>0){
    die("dude, you're already logged in");
}

?>
<!DOCTYPE html
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title><?php echo BASE_HREF; ?> Login</title>
	<link rel="stylesheet" type="text/css" href="<?php echo BASE_HREF; ?>/css/g.css" />
	<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex,nofollow" />
</head>

<body style="text-align:center">
	<div class="d">
		<p>This page requires you to log on.</p>

		<form action="<?php echo BASE_HREF; ?>/api.php" method="post" name="ipb_login_form">
			<!--<input type="hidden" name="CookieDate" value="1" />-->
			<!--<input type="hidden" name="b" value="d" />-->
            <input type="hidden" name="method" value="login" />
			<table style="margin:auto; text-align:left">
				<tr><td>User:</td><td><input class="stdinput" type="text" name="UserName" size="12" maxlength="32" value="" style="width:80px" /></td></tr>
				<tr><td>Pass:</td><td><input class="stdinput" type="password" name="PassWord" size="12" maxlength="32" value="" style="width:80px" /></td></tr>
				<tr><td colspan="2" style="padding-top:5px; text-align:center; vertical-align:middle"><input class="stdbtn" style="width:60px" type="submit" name="ipb_login_submit" value="Login!" />&nbsp;or&nbsp;<a href="<?php echo BASE_HREF ?>/register.php" target="_n">Register</a></td></tr>
			</table>
		</form>
	</div>
</body>
</html>
