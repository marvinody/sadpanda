<?php
if(!defined('SITE_NAME')) {
   die('Direct access not permitted');
}
require_once 'core/galleryInfo.php';
date_default_timezone_set('UTC');


$gallery = getGallery($gid,$mysqli);

savePageSession();//from loadUserPref

checkForChangedSetting('rows',["4","10","20","40"],$gallery['link'],$mysqli);
checkForChangedSetting('size',["L","N"],$gallery['link'],$mysqli);
checkForChangedSetting('viewer',["mpv","s"],$gallery['link'],$mysqli);

if(isset($_POST) && isset($_POST['commenttext'])){
    require_once 'features/addcomment.php';
    $gallery['comments'] = addcomment($_POST['commenttext'],$mysqli);
    header('Location: '.$_SERVER['REQUEST_URI']);
    exit;
}


$totalImagesDisplayed = $_SESSION['prefs']['rows']*5;//base value [if size = N, *2, otherwise leave alone]
$totalImagesDisplayed *= ($_SESSION['prefs']['size'] === 'N' ? 2 : 1);//if N, *2, otherwise *1

$page = (isset($_GET['p'])&&!empty($_GET['p']) )? $_GET['p'] : 1;
$maxPages = ceil(count($gallery['pages'])/$totalImagesDisplayed);
$page = max(min($page,$maxPages), 1);


echoHeader(SITE_NAME." - ".$gallery['title']);
echo getBody($gallery,$totalImagesDisplayed,$page);


function getBody($gallery,$totalImagesDisplayed,$page){
	echo bodyHeader();
	echo navHeader();
	echo '<script type="text/javascript">'.getJSVars($gallery).getJSFuncs().'</script>';
	echo bodyMidClass($gallery);
	echo bodyImages($gallery,$totalImagesDisplayed,$page);
	echo bodyComments($gallery);
	echo bodyFooter();
	
}

function bodyFooter(){
	$retVal = '<p id="frontpage">[<a href="'.BASE_HREF.'">Front Page</a>]</p>';
	$retVal.='<script type="text/javascript" src="'.BASE_HREF.'/js/gallery.js"></script>';
	$retVal.='<script type="text/javascript">update_rating_image(display_rating*2)</script>';
    $retVal.="<script>function show_comments(){var dom = document.querySelectorAll('.negative-comment');for (var i=0;i<dom.length;i++){dom[i].classList.remove('negative-comment');}}</script>";
    $retVal.='<div class="dp">Please don\'t do stupid things on this site or I\'ll have to take it down</div>';
	$retVal.='</body>';
   
	$retVal.='</html>';
	return $retVal;
	
	
}
function bodyComments($gallery){
	$time = date("j F Y, H:i e",1459053689 );
	$retVal = '<a name="comments"></a>';
	$retVal.= '<div id="cdiv" class="gm">';
	$len = count($gallery['comments']);
	$allVisible = true;
	for ($i=0;$i<$len;$i++){
		$com = $gallery['comments'][$i];
		$vote = formatVote($com['totalVote']);
		$cid = $com['isOp']===1 ? 0 : $com['cid'];
        $c1Class = $com['totalVote'] > 0 ? "c1" : "c1 negative-comment";
        $allVisible = $allVisible && $com['totalVote'] > 0;
		$retVal.='<a name="c'.$cid.'"></a>';
		$retVal.='<div class="'.$c1Class.'">';
			$retVal.='<div class="c2">';
				$retVal.= '<div class="c3">Posted on '.date("j F Y, H:i e",$com['timestamp']).' by: &nbsp; <a href="redirectToUploaderHere">'.$com['user'].'</a>&nbsp; &nbsp;';
					$retVal.='<a href="#pmUserHere"><img class="ygm" src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/ygm.png" alt="PM" title="Contact Poster" /></a>';
				$retVal.='</div>';
				if($com['isOp']===1){
				$retVal.= '<div class="c4 nosel"><a name="ulcomment"></a>Uploader Comment</div>';
				} else {
                    if(isset($_SESSION['user']) && $_SESSION['user']['id']!=$com['userId']){
                        $retVal.='<div class="c4 nosel">[<a id="comment_vote_up_'.$cid.'" style="" href="#" onclick="vote_comment_up('.$cid.'); this.blur(); return false">Vote+</a>] &nbsp; [<a id="comment_vote_down_'.$cid.'" style="" href="#" onclick="vote_comment_down('.$cid.'); this.blur(); return false">Vote-</a>]</div>';
                    }
                    
                    $retVal.='<div class="c5 nosel" onmouseover="document.getElementById(\'cvotes_'.$cid.'\').style.display=\'\'" onclick="this.onmouseover(); this.onmouseout=undefined" onmouseout="document.getElementById(\'cvotes_'.$cid.'\').style.display=\'none\'">Score <span id="comment_score_'.$cid.'" style="opacity:1.0">'.$vote.'</span> </div>';
				}
				$retVal.= '<div class="c"></div>';
			$retVal.='</div>';
			
			$retVal.='<div class="c6" id="comment_'.$cid.'">'.formatComment($com['comment']).'</div>';
			//gonna need to expand this once I fix the voting call in galleryINFO TODO
			$retVal.='<div class="c7" id="cvotes_'.$cid.'" style="display:none">'.($com['isOp']===1?"":formatVoters($com)).'</div>';
		$retVal.='</div>';
	}
	$retVal.='<div id="chd">';
    if (!$allVisible){
        $retVal .= "<p>There are some comments below the viewing threshold - <a href='#comments' onclick='show_comments()'>click to show all</a></p>";
    }
    $retVal.='<p id="postnewcomment">[<a href="#" onclick="display_comment_field(); document.getElementById(\'postnewcomment\').style.display=\'none\'; return false">Post New Comment</a>]</p>
			</div>';
	$retVal.='<a name="cnew"></a>';
	$retVal.='<div id="formdiv" style="display:none">';
		$retVal.='<form method="post" action="#cnew">';
	if (isset($_SESSION['user']) && $_SESSION['user']['id'] > 0 ){
		$retVal.='<textarea name="commenttext" class="stdinput" placeholder="Enter your comments here, then hit Post Comment. If the last comment posted is yours, this will be appended to that post."></textarea>
<input type="submit" name="postcomment" value="Post Comment" class="stdbtn">';
	} else {
		$retVal.=' <div>You have to <a href="#linkToRegisterAndRedirect?" rel="nofollow">register</a> before you can add comments.</div>';
	}
		$retVal.='</form>';
	$retVal.='</div>';
		
        
	$retVal.= '</div>';
	return $retVal;
	
}
function formatVoters($com){
	$retVal = "Base ".formatVote($com['baseVote']);
	$maxVotesShown = 12;
    if(isset($com['votes'])){
        $totalVotes = count($com['votes']);
        $maxVotesShown = min($maxVotesShown,$totalVotes);
        for($i=0;$i<$maxVotesShown;$i++){
            $retVal.= ", <span>".$com['votes'][$i]['user']." ".formatVote($com['votes'][$i]['vote'])."</span>";
        }
        if($totalVotes > $maxVotesShown){
            $retVal.=", and ".$totalVotes-$maxVotesShown." more...";
	   }
    }
	
	return $retVal;
}
function formatVote($num){
	if($num>=0){
		return sprintf("%+d",$num);
	} else {
		return sprintf("%d",$num);
	}
}
function formatComment($comment){
	return $comment;
}
function getPageHTML($totalPages,$cPage,$num2Display,$link){
    $start = max(1,$cPage-$num2Display/2);
    $end = min($totalPages,$cPage+$num2Display/2);
    $retVal = "";
    for($i = $start; $i <= $end;$i++){
        if($i == $cPage){
            $retVal .= '<td class="ptds"><a href="'.$link.'?p='.$i.'" onclick="return false">'.$i.'</a>
                </td>';
        } else {
            $retVal .= '<td onclick="document.location=this.firstChild.href"><a href="'.$link.'?p='.$i.'" onclick="return false">
                '.$i.'</a></td>';
        }
        
    }
    return $retVal;
}

function bodyImages($gallery,$totalImagesDisplayed,$page){
	//todo fix page number thing
	$retVal = '<div class="gtb">
        <p class="gpc">Showing '.(($page-1)*$totalImagesDisplayed+1).' - '.min(count($gallery['pages']),$totalImagesDisplayed*$page).' of '.count($gallery['pages']).' images</p>
        <table class="ptt" style="margin:2px auto 0px">
            <tr>
                <td class="ptdd">&lt;</td>
                '.getPageHTML(ceil(count($gallery['pages'])/$totalImagesDisplayed),$page,12,$gallery['link']).'
                <td class="ptdd">&gt;</td>
            </tr>
        </table>
    </div>
	<div id="gdo">
        <div id="gdo1">
            <div id="gdo2"><!--TODO FIX THIS WITH A CALL TO ITSELF TO CHANGE # OF DISPLAY OBJECTS -->';
			$rows = ["4","10","20","40"];
				for($i=0;$i<count($rows);$i++){
					if($_SESSION['prefs']['rows']==$rows[$i]){
						$retVal.='<div class="ths nosel">'.$rows[$i].' rows</div>';
					} else {
						$retVal.='<div class="tha nosel" onclick="document.location=\''.$gallery['link'].'?rows='.$rows[$i].'\'">'.$rows[$i].' rows</div>';
					}
					
				}
				
            
			$retVal.='</div>
        </div>
        <div id="gdo3">
            <div id="gdo4">';
				$sizes = ["N","L"];function getFullSizeNames($c){return $c=="L"?"Large":"Normal";}
				for($i=0;$i<count($sizes);$i++){
					if($_SESSION['prefs']['size']==$sizes[$i]){
						$retVal.='<div class="ths nosel">'.getFullSizeNames($sizes[$i]).'</div>';
					} else {
						$retVal.='<div class="tha nosel" onclick="document.location=\''.$gallery['link'].'?size='.$sizes[$i].'\'">'.getFullSizeNames($sizes[$i]).'</div>';
					}
				}
			$retVal.='</div>
        </div>
    </div>'.
	'<div id="gdt">'.
		getImageList($gallery,$totalImagesDisplayed,$page)
	.'<div class="c"></div>
    </div>
	<div class="gtb">
        <table class="ptb" style="margin:0px auto 10px">
            <tr>
                <td class="ptdd">&lt;</td>
                '.getPageHTML(ceil(count($gallery['pages'])/$totalImagesDisplayed),$page,12,$gallery['link']).'
                <td class="ptdd">&gt;</td>
            </tr>
        </table>
    </div>';
	return $retVal;
	
}

function get_gdt_height($pages){
  $max = 0;
  for($i = 0;$i<count($pages);$i++){
      if($pages[$i]['thumb_height'] > $max){
        $max = $pages[$i]['thumb_height'];
      }
  }
  return $max;
}

function getImageList($gallery,$totalImagesDisplayed,$page){
	$class = getImageSubClass();
	$end = min(count($gallery['pages']),$totalImagesDisplayed*$page);
	$max_height = get_gdt_height($gallery['pages']);
  $height = $class==="gdtl" ? $max_height : $max_height/2;//TODO change this generation to be more dynamic?
  
	//$i will start on (0-ind) index of first thumb on page
	$retVal = "";
	$link = getImageHref($gallery);
	for($i=($page-1)*$totalImagesDisplayed;$i<$end;$i++){
		$retVal.='<div class="'.$class.'" style="height:'.$height.'px">';
			$retVal.=getPageThumbDiv($gallery['pages'][$i],$class,$link);
		$retVal.='</div>';
	
	}
	return $retVal;
}
function getPageThumbDiv($page,$class,$link){
	if($class==='gdtl'){
		return '<a href="'.sprintf($link,$page['page']).'"><img alt="'.$page['page'].'" title="Page '.$page['page'].': '.$page['name'].'" src="'.$page['t_link'].'" /></a>';
	}elseif($class==='gdtm'){
		return '<div style="margin:1px auto 0; width:100px; height:'.($page['thumb_height']/2).'px; background:transparent url('.$page['t_link'].')no-repeat;background-size:100px '.($page['thumb_height']/2).'px;">
                <a href="'.sprintf($link,$page['page']).'"><img alt="'.$page['page'].'" title="Page '.$page['page'].': '.$page['name'].'" src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/blank.gif" style="width:100px; height:'.($page['thumb_height']/2).'px; margin:-1px 0 0 -1px" />
                </a>
            </div>';
	}
}
function getImageHref($gallery){
	if(isset($_SESSION['prefs']['viewer'])&&!empty($_SESSION['prefs']['viewer']) ){
		if($_SESSION['prefs']['viewer']=== 'mpv'){
			return $gallery['multiPageViewer']."#page%d";
		}
	}
	return $gallery['singlePageViewer'].'%d';
}
function getImageSubClass(){
	if($_SESSION['prefs']['size']==='L'){
		return "gdtl";
	} elseif($_SESSION['prefs']['size']==='N') {
		return "gdtm";
	}
	return "gdtm";//default
}
function getHTMLforViewerSwitch($link){
    if($_SESSION['prefs']['viewer']=='mpv'){
        $opp_full_name = "Single page";
        $change_link = $link."?viewer=s";
    } else {
        $opp_full_name = "Multi page";
        $change_link = $link."?viewer=mpv";
    }
    
    return '<p class="g2 gsp"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" /> <a id="switchview" href="'.$change_link.'">'.$opp_full_name.' viewer</a>
				</p>';
}
function bodyMidClass($gallery){
	return '<div class="gm">
		<div id="gleft">
            <div id="gd1"><img style="width:250px;" src="'.$gallery['thumbnail'].'" alt="'.$gallery['title'].'" /></div>
		</div>
		<div id="gd2">
			<h1 id="gn" style="border-bottom: 1px solid #5C0D12;">'.$gallery['title'].'</h1>
			<!--<h1 id="gj">'.$gallery['title'].'</h1>-->
		</div>
		<div id="gright">
			<div id="gd5">
				<p class="g3 gsp"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" /> <a href="#" onclick="return popUp(\''.BASE_HREF.'/report.php?gid=917148\',750,350)">Report Gallery</a>
				</p>
				<!--<p class="g2 gsp"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" /> <a href="#" onclick="return popUp(\'http://g.e-hentai.org/archiver.php?gid=917148&amp;token=294e5a30f5&amp;or=405284--7edca5864a5d83a74fd2f10da55b2668dc26fd16\',350,320)">Archive Download</a>
				</p>-->
				<!--<p class="g2"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" /> <a href="#" onclick="return popUp(\'http://g.e-hentai.org/gallerytorrents.php?gid=917148&amp;t=294e5a30f5\',610,590)">Torrent Download ( 1 )</a>
				</p>-->
				<!--<p class="g2"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" /> <a href="http://g.e-hentai.org/hathdler.php?gid=917148&amp;t=294e5a30f5">H@H Downloader</a>
				</p>-->
				<!--<p class="g2 gsp"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" /> <a href="http://g.e-hentai.org/stats.php?gid=917148&amp;t=294e5a30f5">Show Gallery Stats</a>
				</p>-->
				<!--<p class="g2"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" /> <a href="#" onclick="return popUp(\'http://g.e-hentai.org/codegen.php?gid=917148&amp;t=294e5a30f5\', 1024, 700)">Get External Gallery</a>
				</p>-->
				<p class="g2 gsp"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" /> <a id="expungelink" href="#" onclick="return pop_exp()">Petition to Expunge</a>
				</p>
				<p class="g2"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" /> <a id="renamelink" href="#" onclick="return pop_ren()">Petition to Rename</a>
				</p>
                '.getHTMLforViewerSwitch($gallery['link']).'
			</div>
		</div>
		<div id="gmid">
            <div id="gd3">
                <!--<div id="gdc">
                    <a href="http://g.e-hentai.org/non-h"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/c/non-h.png" alt="non-h" class="ic" />
                    </a>
                </div>-->
                <div id="gdn"><a href="#redirectToUploaderHere">'.$gallery['uploader'].'</a>&nbsp;
                    <a href="#ImplementDirectContactMaybe?"><img class="ygm" src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/ygm.png" alt="PM" title="Contact Uploader" />
                    </a>
                </div>
				
				<div id="gdd">
                    <table>
                        <tr>
                            <td class="gdt1">Posted:</td>
                            <td class="gdt2">'.$gallery['timestamp'].'</td>
                        </tr>
                        <tr>
                            <td class="gdt1">Parent:</td>
                            <td class="gdt2">'.getParentInfo($gallery['parent']).'</td>
                        </tr>
                        <tr>
                            <td class="gdt1">Visible:</td>
                            <td class="gdt2">Yes</td>
                        </tr>
                        <tr>
                            <td class="gdt1">Language:</td>
                            <td class="gdt2">'.($gallery['isTranslated'] ? "FIX THIS" : "N/A").'
                            </td>
                        </tr>
                        <tr>
                            <td class="gdt1">File Size:</td>
                            <td class="gdt2">'.$gallery['totalFileSize'].' KB</td>
                        </tr>
                        <tr>
                            <td class="gdt1">Length:</td>
                            <td class="gdt2">'.count($gallery['pages']).'</td>
                        </tr>
                        <tr>
                            <td class="gdt1">Favorited:</td>
                            <td class="gdt2" id="favcount">'.$_SESSION['gallery']['favorites']['strFav'].'</td>
                        </tr>
                    </table>
                </div>
				
				<div id="gdr" onmouseout="rating_reset()">
                    <table>
                        <tr>
                            <td id="grt1">Rating:</td>
                            <td id="grt2">
                                <div id="rating_image" class="ir '.getVoteClass($gallery['userVote']).'" style="background-position:0px -21px"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/blank.gif" usemap="#rating" alt="" />
                                </div>
                            </td>
                            <td id="grt3"><span id="rating_count">'.$gallery['numVotes'].'</span>
                            </td>
                        </tr>
                        <tr>
                            <td id="rating_label" colspan="3">Average: '.$gallery['averageVotes'].'</td>
                        </tr>
                    </table>
                    <map name="rating">
                        <area shape="rect" coords="0,0,7,16" onclick="rating_set(1)" onmouseover="rating_show(1)" />
                        <area shape="rect" coords="8,0,15,16" onclick="rating_set(2)" onmouseover="rating_show(2)" />
                        <area shape="rect" coords="16,0,23,16" onclick="rating_set(3)" onmouseover="rating_show(3)" />
                        <area shape="rect" coords="24,0,31,16" onclick="rating_set(4)" onmouseover="rating_show(4)" />
                        <area shape="rect" coords="32,0,39,16" onclick="rating_set(5)" onmouseover="rating_show(5)" />
                        <area shape="rect" coords="40,0,47,16" onclick="rating_set(6)" onmouseover="rating_show(6)" />
                        <area shape="rect" coords="48,0,55,16" onclick="rating_set(7)" onmouseover="rating_show(7)" />
                        <area shape="rect" coords="56,0,63,16" onclick="rating_set(8)" onmouseover="rating_show(8)" />
                        <area shape="rect" coords="64,0,71,16" onclick="rating_set(9)" onmouseover="rating_show(9)" />
                        <area shape="rect" coords="72,0,79,16" onclick="rating_set(10)" onmouseover="rating_show(10)" />
                    </map>
                </div>
                '.makeFavoriteDiv($gallery).'
				
			</div>
			<div id="gd4">
                '.makeTagList($gallery).'
                <div id="tagmenu_act" style="display:none"></div>
                <div id="tagmenu_new">
                    <form action="" method="post" class="nopm">
                        <input id="newtagfield" type="text" name="tags" placeholder="Enter new tags, separated with comma. Must be in style of namespace:tag" class="stdinput" onfocus="pop_tagaccept()" size="60" maxlength="200" />
                        <input id="newtagbutton" type="submit" name="submit" value="Tag!" class="stdbtn" onclick="tag_from_field(); return false" />
                    </form>
                </div>
                <div id="gwrd"><img id="waitroller" src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/roller.gif" alt="" style="visibility:hidden" />
                </div>
            </div>
			<div class="c"></div>
			
			
		</div>
		
		<div class="c"></div>
		
	
	</div>';
}
function makeFavoriteDiv($gallery){
    if(isset($gallery['favorites']['userFav']) && isset($gallery['favorites']['userFav']['favCat']) ){
        return '<div id="gdf" onclick="return pop_fav()">
                    <div style="float:left; cursor:pointer" id="fav">
                        <div class="i" style="background-image:url('.BASE_HREF.'/images/fav.png); background-position:0px '
            .(-2-(19*$gallery['favorites']['userFav']['favCat'])).'px; margin-left:10px" title="Favorites '.$gallery['favorites']['userFav']['favCat'].'"></div>
                    </div>
                    <div style="float:left">
                        &nbsp; <a id="favoritelink" href="#" onclick="return false">Favorites '.$gallery['favorites']['userFav']['favCat'].'</a>
                    </div>
                    <div class="c"></div>
                </div>';
    } else {
        return '<div id="gdf" onclick="return pop_fav()">
                    <div style="float:left; cursor:pointer" id="fav"></div>
                    <div style="float:left">&nbsp;
                        <a id="favoritelink" href="#" onclick="return false"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" /> Add to Favorites</a>
                    </div>
                    <div class="c"></div>
                </div>';
    }
    
}
function getVoteClass($userVote){ 
    $class = "";
    if(!$userVote['voted']){return "";}
    $rating = $userVote['vote']*2;
    if($rating >= 1 && $rating <= 4){ $class.="irr";}
    if($rating >= 5 && $rating <= 8){ $class.="irg";}
    if($rating >= 9 && $rating <= 10){$class.="irb";}
    return $class;
}
function getParentInfo($parent){
	/*TODO change the parent thing maybe?*/
	if(empty($parent))
		return "None";
	return "<a href='".BASE_HREF."/g/".$parent."'>".$parent."</a>";
}
function _checkIfPossibleForTag($gallery,$namespace,$j){
    if($j >= count($gallery['userTags'])){return false;}
    return $namespace==$gallery['userTags'][$j]['namespace'];
}
function _getClassListForTagOnUserVote($mightHaveTag,$tag,$userTags,$j){
    if(!$mightHaveTag){return "";}
    $tags = $userTags[$j]['tags'];
    for($i=0;$i<count($tags);$i++){
        if($tag == $tags[$i]['tag']){
            if($tags[$i]['votes']==0){return "";}
            return $tags[$i]['votes'] > 0 ? "tup" : "tdn";
        }
    }
    return "";
    
}
function makeTagList($gallery){
	$tagVoteForBordered = 5;
	$retVal = '';
	$retVal.='<div id="taglist">';
		$retVal.='<table>';
		$len = count($gallery['tags']);
        $j = 0;//this is cnt for userTags so we know where we at
		$tags = $gallery['tags'];
		for($i=0;$i<$len;$i++){
			$retVal.='<tr>';
				$retVal.='<td class="tc">'.$tags[$i]['namespace'].':</td>';
				$retVal.='<td>';
				$namespace = $tags[$i]['namespace'];
                $possibleTag = _checkIfPossibleForTag($gallery,$namespace,$j);
				for($k=0;$k<count($tags[$i]['tags']);$k++){
					$tagArr=$tags[$i]['tags'][$k];
                    $tagVoteClass = _getClassListForTagOnUserVote($possibleTag,$tagArr['tag'],$gallery['userTags'],$j);
					$class = $tagArr['votes'] > $tagVoteForBordered ? "gt" : "gtl";
					$nameColonTag = $namespace.':'.$tagArr['tag'];
					$retVal.='<div id="td_'.$nameColonTag.'" class="'.$class.'" style="opacity:1.0"><a id="ta_'.$nameColonTag.'" href="'.BASE_HREF.'/tag/'.$nameColonTag.'" class="'.$tagVoteClass.'" onclick="return toggle_tagmenu(\''.$nameColonTag.'\', this)">'.$tagArr['tag'].'</a>';
					$retVal.='</div>';
				}
                if($possibleTag){
                    $j++;//cause we know we need to match next namespace
                }
				$retVal.='</td>';
				
				
			$retVal.='</tr>';
			
		}
		
		$retVal.='</table>';
	$retVal.='</div>';
	return $retVal;
}
function getJSFuncs(){
	return 'function pop_fav() {
            return popUp(popbase + "addfav", 675, 415);
        }

        function pop_exp() {
            return popUp(popbase + "expunge", 675, 415);
        }

        function pop_ren() {
            return popUp(popbase + "rename", 675, 415);
        }';
}
function getJSVars($gallery){
	return '
	var login_url = "http://e-hentai.org/bounce_login.php?b=d&bt=3-917148-294e5a30f5";
        var base_url = "'.BASE_HREF.'/";
        var gid = '.$gallery['gid'].';
        var token = "294e5a30f5";
        var apiuid = -1;
        var apikey = "341ba9d027ae772a1d4c";
        var average_rating = '.$gallery['averageVotes'].';
        var display_rating = '.$gallery['userVote']['vote'].';
        var popbase = base_url + "gallerypopups.php?gid='.$gallery['gid'].'&t=294e5a30f5&act=";
	';
}
function navHeader(){
	include ROOT_DIR.'pages/periph/navHeader.php';
    return getHeader();

}

function bodyHeader(){
	return '<body style="text-align:center">
	<script type="text/javascript">
        function popUp(URL, w, h) {
            window.open(URL, "_pu" + (Math.random() + "").replace(/0\./, ""), "toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=" + w + ",height=" + h + ",left=" + ((screen.width - w) / 2) + ",top=" + ((screen.height - h) / 2));
            return false;
        }
    </script>
	';
}


function echoHeader($title){
	echo '
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>'.$title.'</title>
		<link rel="stylesheet" type="text/css" href="'.BASE_HREF.'/css/g.css" />
        <style>.negative-comment{display:none;}</style>
	</head>
	';
	
}