<?php
if(!defined('SITE_NAME')) {
   die('Direct access not permitted');
}
session_start();
$_SESSION = array();
session_destroy();
echo "
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">
<head>
<title>Gallery Not Available -".SITE_NAME."</title>
<link rel=\"stylesheet\" type=\"text/css\" href=\"".BASE_HREF."/css/g.css\" />

</head>
<body style=\"text-align:center\">
<script type=\"text/javascript\">
function popUp(URL,w,h) { 
	window.open(URL,\"_pu\"+(Math.random()+\"\").replace(/0\./,\"\"),\"toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=\"+w+\",height=\"+h+\",left=\"+((screen.width-w)/2)+\",top=\"+((screen.height-h)/2));
	return false;
}
</script>

<div class=\"d\">
<p>Logout succesful.</p>
<p>You will be redirected to the front page momentarily.</p>
<p id=\"continue\"><a href=\"".BASE_HREF."\">(Click here to continue)</a></p>
</div>

<script type=\"text/javascript\">
function gotonext() {
	document.getElementById(\"continue\").innerHTML = \"Please wait...\";
	document.location = \"".BASE_HREF."\";
}
setTimeout(\"gotonext()\", 3000);
</script>

</body>
</html>
";
