<?php
if(!defined('SITE_NAME')) {
   die('Direct access not permitted');
}
session_start();




function getHTMLHeader($pageTitle){
    require_once 'pages/periph/navHeader.php';
    return '<!doctype html>
<html lang="en"><head>
<meta charset="utf-8">
<title>'.SITE_NAME.' - '.$pageTitle.'</title>
<link rel="stylesheet" type="text/css" href="'.BASE_HREF.'/css/g.css">
<style type="text/css">

td.gtc1{height:22px; padding-left:10px;text-align:left}
td.gtc2{}
td.gtc3{padding-right:10px; text-align:right}
td.gtc4{text-align:center}
td.gtc5{}
td.gtc6{text-align:center}

</style>
<meta name="robots" content="noarchive">
<style id="__web-inspector-hide-shortcut-style__" type="text/css">
.__web-inspector-hide-shortcut__, .__web-inspector-hide-shortcut__ *, .__web-inspector-hidebefore-shortcut__::before, .__web-inspector-hideafter-shortcut__::after
{
    visibility: hidden !important;
}
</style></head>
<body style="text-align:center">
<script type="text/javascript">
function popUp(URL,w,h) { 
	window.open(URL,"_pu"+(Math.random()+"").replace(/0\./,""),"toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width="+w+",height="+h+",left="+((screen.width-w)/2)+",top="+((screen.height-h)/2));
	return false;
}
</script>'.getHeader();
}

function getHTMLFooter(){
    return '


<p class="ip">[<a href="'.BASE_HREF.'">Front Page</a>]</p>


</body></html>';
}


echo getHTMLHeader("Terms of Service");
?>
<div class="stuffbox" style="text-align:left; width:980px; margin:10px auto 10px auto; padding:5px">
    <h1 style="font-size:10pt; font-weight:bold; margin:3px; text-align:center">Help</h1>
    <div style="font-size:10pt; font-weight:bold; text-align:justify; margin:10px auto">
        <a id="roe" name="roe">Terms of Service</a>
    </div>
    <div>
        <p style="text-align:justify">I copied pretty much everything else from EH but I will eventually write this part up myself.</p>
        <p style="text-align:justify">General rule for now, if you don't want your coworker/family/friends knowing that you're uploading it, please don't upload it and get me in trouble cause then I'll have to get you in trouble later. Thanks</p>
        <!--
        <p style="text-align:justify">We strive to uphold the ideals of freedom of speech on this website. However, to stay within the confines of applicable law and generally accepted moral limits, there are certain rules you must obey with content you upload to this site. What you can and cannot post is listed below. If what you wish to post is not listed, you can assume that this isn't the place to post it.</p>
        <p style="text-align:justify">You may:<br>
        <br>
        - Post galleries featuring legally drawn characters of any kind, including anime-style drawings and western-style cartoons, both pornographic and non-pornographic. Alternative stuff is allowed, but please mark the gallery appropriately in the title.<br>
        <br>
        - Post real high-quality cosplay that complies with US and international law.<br></p><br>
        <p style="text-align:justify">You may <span style="font-weight:bold">NOT</span>:<br>
        <br>
        - Post any kind of material that contains or depicts people who are or appear to be underage (even if they are borderline), nor galleries that claim to contain this kind of content (using keywords like "lolita", "preteen", "incest", "lolicon" or derivatives, even if they do not contain this). This includes drawings and underage "models" who do not engage in actual sexual activity, as well as normal photos involving children. <strong>ANY KIND OF ILLICIT ACTIVITY INVOLVING CHILDREN WILL BE REPORTED ALONG WITH A FULL TRACE OF ALL YOUR ACTIVITY ON THIS SITE.</strong><br>
        <br>
        - Post photograpic material of real people that can be considered "obscene". This includes extreme BDSM, bestiality, scat, and other stuff that is way "out there". This isn't where to come for that stuff, please go away.<br>
        <br>
        - Post any kind of drawn or virtual material that violates the principles above. This explicitly includes, but is not limited to, computer-generated images ("rendered") and images created based on real people or pictures ("rotoscoped"). If it resembles or was obviously manufactured to resemble the real thing, this rule particularly applies.<br>
        <br>
        - Post material listed in our <a href="https://forums.e-hentai.org/index.php?showtopic=19797">E-Hentai Prohibited Content and Artists</a> thread. We know that you did not obtain permission for these, so don't even try.<br>
        <br>
        - Post photographic material of real people that does not fit under the Cosplay category.<br>
        <br>
        - <span style="font-weight:bold">ANY KIND OF MATERIAL INVOLVING CHILDREN IS ABSOLUTELY PROHIBITED, EVEN IF THEY ARE NOT PORNOGRAPHIC OR RISQUE IN NATURE, AND EVEN IF THEY CONTAIN NO NUDITY NOR POSES WITH SEXUAL IMPLICATIONS. JUST A SINGLE VIOLATION OF THIS RULE WILL CAUSE YOUR ACCOUNT TO BE PERMANENTLY BANNED, AND IF THE MATERIAL IS ILLICIT, YOUR DETAILS TO BE HANDED OVER TO THE AUTHORITIES IN YOUR JURISDICTION.</span><br>
        <br>
        - <span style="font-weight:bold">Computer-generated or "virtual" child pornography is just as prohibited as the real stuff. Uploading this type of images is a one-strike-and-you're-out infraction.</span></p>
        <p style="text-align:justify">If you violate the rules, you agree that we can submit your browsing history, uploading history, posting history, medical history, any and all IP addresses you have ever browsed or posted with, proxy traces, e-mail addresses, referring web sites, browser type, blood type, favorite stuffed animals, and any and all other personal information gathered in any way on any of the E-Hentai services, including but not limited to E-Hentai Classic, E-Hentai Galleries and the E-Hentai Forums, to anyone we damn well please. So be warned.</p>
    </div>
    <div style="font-size:10pt; font-weight:bold; text-align:justify; margin:10px auto">
        <a id="pgu" name="pgu">Posting Guidelines</a>
    </div>
    <div>
        <p style="text-align:justify">Activity that violates one or more of the following guidelines may cause your galleries to be expunged, and your account to be temporarily or permanently blocked from posting galleries.<br>
        <br>
        - Compilations of more than one "work" is not allowed. This does not include combining selections of images into one Image Set gallery.<br>
        <br>
        - Excessive updates to or "bumping" of galleries must be avoided. "Excessive" should generally be interpreted as more than twice per month.<br>
        <br>
        - Avoid posting galleries with just a few images unless it is naturally released as such. "A few" should generally be interpreted as less than eight.<br></p>
    </div>
    <div style="font-size:10pt; font-weight:bold; text-align:justify; margin:10px auto">
        <a id="cl" name="cl">Content Licensing</a>
    </div>
    <div>
        <p style="text-align:justify">You agree that, upon uploading content to this site, the content you upload is either Public Domain, released under a Creative Commons-type license, or not copyrighted (i.e. it is within your rights to publish it here); or you own the copyright to the content and consent to it being fully published here; or you have a separate license from the copyright holder to make their content publicly available.</p>
        <p style="text-align:justify">You retain all of your ownership rights for your content. However, by submitting content to E-Hentai, you hereby grant E-Hentai a worldwide, non-exclusive, royalty-free, perpetual and irrevocable license to use, reproduce, distribute, prepare derivative works of, display, and perform the content in connection with the service and E-Hentai's (and its successors' and affiliates') business, including without limitation for promoting and redistributing part or all of the service (and derivative works thereof) in any media formats and through any media channels. You also hereby grant each user of the service a non-exclusive license to access your content through the service, and to use, reproduce, distribute, display and perform such content as permitted through the functionality of the service and under these terms. <strong>The above licenses granted by you are perpetual and irrevocable.</strong></p>
        <p style="text-align:justify"><strong>IF YOU ARE NOT LEGALLY ENTITLED TO GRANT THESE LICENSES FOR THE UPLOADED CONTENT, THEN YOU ARE NOT PERMITTED TO UPLOAD THE CONTENT TO E-HENTAI GALLERIES OR OTHER SERVICES PROVIDED BY E-HENTAI. YOU RETAIN FULL LIABILITY FOR ANY MISAPPROPRIATED CONTENT UPLOADED TO THIS SERVICE.</strong></p>
    </div>
    <div style="font-size:10pt; font-weight:bold; text-align:justify; margin:10px auto">
        <a id="rcc" name="rcc">Rules of Content Classification</a>
    </div>
    <div>
        <p style="text-align:justify">E-Hentai Galleries has a publicly viewable section of the galleries that are available for access for everyone. In order to improve the experience for out visitors, there are certain restrictions in place on what types of content can be classified as which category.</p>
        <p style="text-align:justify; font-weight:bold">The Doujinshi, Manga, Artist CG, Game CG and Image Set sections should only be used for corresponding content that contain "hentai" or "ecchi" material. All non-H material should be posted as "Non-H".</p>
        <p style="text-align:justify">The Cosplay section should only be used for real people involved in "costume play". This includes but is not limited to characters from anime, manga or games.</p>
        <p style="text-align:justify">The Western section should only be used for western-style drawn art of a sexual nature ("cartoon porn").</p>
        <p style="text-align:justify">The Misc section can be used as a generic category for stuff that doesn't fall under the other ones, but that still would hold some interest for the average E-Hentai visitor. Do not post low quality material and fetish porn. Excessive amounts of crap can get you banned.</p>
        <p style="text-align:justify">Note that the galleries do not earn any GP unless they are published.</p>
    </div>
    <div style="font-size:10pt; font-weight:bold; text-align:justify; margin:10px auto">
        Copyright and Intellectual Property
    </div>
    <div>
        E-Hentai Galleries complies with applicable copyright law. Unfortunately, due to sheer volume it is impossible for us to verify the copyright status of every single gallery uploaded to our site, nor are we legally complied to do so. If you find that your copyright has been infringed by one of our members, please report this using the <span style="font-weight:bold; color:red">Report Gallery</span> button on the gallery in question, or by emailing a list of links to the galleries in question to <strong>reports <span style="font-style:italic" title="All Spammers Must Die">(a)</span> e-hentai period org</strong>. Please make sure to include all the information below.<br>
        <br>
        <strong>A properly filed takedown notice needs the following information to be valid. You are hereby made aware that if any of the legally required information is missing or invalid, we cannot be legally considered to have actual knowledge of copyright infringement.</strong><br>
        <br>
        - A physical or electronic signature of a person (full legal name) authorized to act on behalf of the owner of an exclusive right that is allegedly infringed;<br>
        <br>
        - Identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works at a single online site are covered by a single notification, a representative list of such works at that site;<br>
        <br>
        - Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled and information reasonably sufficient to permit the service provider to locate the material;<br>
        <br>
        - Information reasonably sufficient to permit the service provider to contact you, such as an address, telephone number, and, if available, an electronic mail;<br>
        <br>
        - A statement that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and<br>
        <br>
        - A statement that the information in the notification is accurate, and under penalty of perjury, that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.<br>
        <br>
        You may send it to the specified email address in the form of a DMCA Takedown Notice if you wish, as it contains sufficient provisions. Normally we respond within the hour, but please allow six to twelve hours in case there is no qualified staff available to process your claim.<br>
        <br>
        Please note that using the report button or sending an email to us directly is much faster than whining to our hosts. They are well informed of our takedown procedures, and they will simply forward the complaint to us with a day or two delay for their own processing.<br>
        <br>
        Furthermore, please be aware that all copyright claims are made under perjury of law, and false claims may be prosecuted.
    </div>-->
</div>

<?php
getHTMLFooter();
?>

