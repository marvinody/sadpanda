<?php
if(!defined('SITE_NAME')) {
   die('Direct access not permitted');
}
require_once 'core/galleryInfo.php';
$gallery = getGallery($gid,$mysqli);
$page = (isset($_GET['page'])&&!empty($_GET['page']) )? $_GET['page'] : 1;
$page = max(min($page,count($gallery['pages'])), 1);

echoHTML($gallery,$page);

function echoHTML($gallery,$page){
    echo getHeader(SITE_NAME." - ".$gallery['title']);
    echo getBody($gallery,$page);
    //echo getFooter();
    echo getCloseHtml();
   /* 
    echo "<pre>";
    echo "page:".$page;
    print_r($_GET);
    print_r($_SESSION);
    echo "</pre>";*/
}



function getBody($gallery,$page){
    $total = count($gallery['pages']);
    $retVal = "";
    $retVal .= getJavascriptStuff($gallery,$page);
    $retVal .= "<div id='i1' class='sni' style='width:".($gallery['pages'][$page-1]['width']+20)."px'>";
        $retVal .= "<h1>".$gallery['title']."</h1>";
        $retVal .= getBodyi2($gallery,$page);
        $retVal .= getBodyi3($gallery,$page);
        $retVal .= getBodyi4($gallery,$page);
        $retVal .= getBodyi5($gallery,$page);
        $retVal .= getBodyi6($gallery,$page);
        $retVal .= getBodyi7($gallery,$page);
    $retVal.= "</div>";
    $retVal.='<p class="ip">[<a href="'.BASE_HREF.'">Front Page</a>]</p>';
    return $retVal;
}
function getBodyi7($gallery,$page){
    
    $retVal  ='<div id="i7" class="if">';
    if($gallery['pages'][$page]['hasRaw']){
        $retVal .='&nbsp; <img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" class="mr" /> <a href="'.BASE_HREF.'/'.LARGE_IMAGE_DIR_URL.'/'.$gallery['gid'].'/'.$page.'">Download original source image</a>';
    }
        
    $retVal .='</div>';
    return $retVal;
}
function getBodyi6($gallery,$page){
    $retVal  ='<div id="i6" class="if">';
        $retVal .='&nbsp; <img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" class="mr" /> <a href="#implementFileSearchHere">Show all galleries with this file</a>';
        $retVal .='&nbsp; <img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" class="mr" /> <a href="#" id="loadfail" onclick="return nl(\''.$gallery['gid'].','.$page.'\')">Click here if the image fails loading</a>';
    $retVal .='</div>';
    return $retVal;
}
function getBodyi5($gallery,$page){
    $retVal = '<div id="i5">';
        $retVal .='<div class="sb">';
            $retVal .='<a href="'.$gallery['link'].'"><img src=
        "'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/b.png" referrerpolicy="no-referrer" /></a>';
        $retVal .='</div>';
    $retVal .='</div>';
    return $retVal;
}
function getBodyi4($gallery,$page){
    $total = count($gallery['pages']);
    $retVal  ='<div id="i4">';
        $retVal .='<div>'.$gallery['pages'][$page-1]['name'].' :: '.$gallery['pages'][$page-1]['width'].' x '.$gallery['pages'][$page-1]['height'].' :: '.$gallery['pages'][$page-1]['filesize'].' KB</div>';
        $retVal .='<div class="sn">';
            $retVal .='<a onclick="return load_image(1, \''.$gallery['gid'].'\')" href="'.$gallery['singlePageViewer'].'1"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/f.png" /></a>';
            $retVal .='<a id="prev" onclick="return load_image('.max($page-1,1).', \''.$gallery['gid'].'\')" href="'.$gallery['singlePageViewer'].max($page-1,1).'"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/p.png" /></a>';
            $retVal .='<div><span>'.$page.'</span> / <span>'.$total.'</span></div>';
            $retVal .='<a id="next" onclick="return load_image('.min($page+1,$total).', \''.$gallery['gid'].'\')" href="'.$gallery['singlePageViewer'].min($page+1,$total).'"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/n.png" /></a>';
            $retVal .='<a onclick="return load_image('.$total.', \''.$gallery['gid'].'\')" href="'.$gallery['singlePageViewer'].$total.'"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/l.png" /></a>';

        $retVal .='</div>';
    $retVal .='</div>';
    return $retVal;
}
function getBodyi3($gallery,$page){
    $retVal =' <div id="i3">
      <a onclick="return load_image('.($page+1).', '.$gallery['gid'].')" href=
      "'.$gallery['pages'][$page]['f_link'].'"><img id="img" src=
      "'.$gallery['pages'][$page-1]['f_link'].'"
      style="height:'.$gallery['pages'][$page-1]['height'].'px;width:'.$gallery['pages'][$page-1]['width'].'px" /></a>
    </div>';
    return $retVal;
}
function getBodyi2($gallery,$page){
    $total = count($gallery['pages']);
    $retVal = "";
    $retVal .='<div id="i2">';
        $retVal .='<div class="sn">';
            //first pic
            $retVal .='<a onclick="return load_image(1, \''.$gallery['gid'].'\')" href="'.$gallery['singlePageViewer'].'1"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/f.png" /></a>';
            $retVal .='<a id="prev" onclick="return load_image('.max($page-1,1).', \''.$gallery['gid'].'\')" href="'.$gallery['singlePageViewer'].max($page-1,1).'"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/p.png" /></a>';
            $retVal .='<div><span>'.$page.'</span> / <span>'.$total.'</span></div>';
            $retVal .='<a id="next" onclick="return load_image('.min($page+1,$total).', \''.$gallery['gid'].'\')" href="'.$gallery['singlePageViewer'].min($page+1,$total).'"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/n.png" /></a>';
            $retVal .='<a onclick="return load_image('.$total.', \''.$gallery['gid'].'\')" href="'.$gallery['singlePageViewer'].$total.'"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/l.png" /></a>';

        $retVal .='</div>';
        $retVal .='<div>'.$gallery['pages'][$page-1]['name'].' :: '.$gallery['pages'][$page-1]['width'].' x '.$gallery['pages'][$page-1]['height'].' :: '.$gallery['pages'][$page-1]['filesize'].' KB</div>';
    $retVal .='</div>';
    return $retVal;
}

function getJavascriptStuff($gallery,$page){
    return '<script type="text/javascript">
//<![CDATA[
  function popUp(URL,w,h) { 
        window.open(URL,"_pu"+(Math.random()+"").replace(/0\./,""),"toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width="+w+",height="+h+",left="+((screen.width-w)/2)+",top="+((screen.height-h)/2));
        return false;
  }
  //]]>
  </script>'.'<script type=
  "text/javascript">
//<![CDATA[
  var gid='.$gallery['gid'].';var startpage='.$page.';var startkey="a2bdbf646d";var showkey="o2xjz818ovz";var base_url="'.BASE_HREF.'/";var prl=9999;var si=17465;var x = '.$gallery['pages'][$page-1]['width'].';var y = '.$gallery['pages'][$page-1]['height'].';
  //]]>
  </script>';
}
function getCloseHtml(){
    return '<script type="text/javascript" src=
  "'.BASE_HREF.'/js/spv.js">
</script></body></html>';
}
function getHeader($title){
	return '
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>'.$title.'</title>
		<link rel="stylesheet" type="text/css" href="'.BASE_HREF.'/css/g.css" />
	</head>
	';
	
}