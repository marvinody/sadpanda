<?php
if(!defined('SITE_NAME')) {
   die('Direct access not permitted');
}
function manageNewPage($mysqli){
    //$albums = getGalleriesForUser($_SESSION['user']['id'],$mysqli);

    $errors = [];
    $gid = 0;
    try{
        if(
            isset($_POST['gname'])
            || isset($_POST['comment'])
            || isset($_POST['gname_jpn'])
            || isset($_POST['name'])
            || isset($_POST['public'])
            || isset($_POST['publiccat'])
            || isset($_POST['tos'])
        ){
            //post set
            //verify
            list($errors,$gid) = getPOSTResponse($mysqli);
            #echo "tos:".$_POST['tos']."<br>";
            if(count($errors)==0){
                header('Location: '.BASE_HREF.'/manage?act=add&gid='.$gid);
                return;
            }
        }
        getHTMLResponse($mysqli,$errors);
    } catch (mysqli_sql_exception $e){
        error_log("manage-new MYSQL ERROR");
        error_log($e->getMessage());
        echo "Some database error happend, please try again later";
      
    } catch (RuntimeException $e) {
        error_log($e->getMessage());
        error_log("MYSQL_ERROR:".$mysqli->error);
        echo "Some error happened, please inform admin or try later";
    }
    
}
/*POST RESPONSE BELOW*/
function getPOSTResponse($mysqli){
    require ROOT_DIR.'features/creategallery.php';
    return creategallery(
        $mysqli,
        getCategories($mysqli),
        $_POST['gname'],
        $_POST['gname_jpn'],
        $_POST['public'],
        $_POST['publiccat'],
        $_POST['comment'],
        $_POST['tos']
    );
}

/*
POST response above
Base html response down here
*/
function getHTMLResponse($mysqli,$errors){
    echo getHTMLHeader("Create New Gallery");
    echo getHTMLBody($errors,getCategories($mysqli));
    echo getHTMLFooter();
}

function getCategories($mysqli){
    #TODO move this caching somewhere else?
    if(isset($_SESSION['g_categories']))
        return $_SESSION['g_categories'];
    $query = "SELECT id,category FROM `categories` WHERE id<>1 ORDER BY priority DESC";

    if(!$stmt = $mysqli->prepare($query)){
        throw new RuntimeException('Database error mngc1. Inform Admin');
    }
    if(!$stmt->bind_result($id,$category)){
        throw new RuntimeException('Database error mngc3. Inform Admin');
    }
    if(!$stmt->execute()){
        throw new RuntimeException('Database error mngc4. Inform Admin');
    }

    $categories = [];
    while($stmt->fetch()){
        $categories[] = array(
            'id'=>$id,
            'category'=>$category
        );
    }
    $_SESSION['g_categories'] = $categories;
    return $categories;
}

function getCategoryHTML($cats){
    $retVal = "";
    $retVal.= '<select id="publiccat" name="publiccat" class="stdinput">';
    for($i=0;$i<count($cats);$i++){
        $retVal.='<option value="'.$cats[$i]['id'].'">'.$cats[$i]['category'].'</option>';
    }
        
    $retVal.= "</select>";
    return $retVal;
}

function getErrorHTML($errors){
    $retVal = "";
    
    $retVal.="<p>Some errors were found in your form</p>";
    for($i=0;$i<count($errors);$i++){
        $retVal.="<p>".$errors[$i]."</p>";
    }
    
    
    return $retVal;
}

function getHTMLBody($errors,$categories){
    /*
    div.ui and everything inside
    */
    //add link in
    $link = BASE_HREF."/manage?act=new";
    
    $retVal = "";
    $retVal.= '<div class="ui">
      <div style="text-align:left">
      <p style="font-weight:bold; font-size:10pt">Create a New Gallery</p>';
    
    if(count($errors)>0)
        $retVal.=getErrorHTML($errors);
    $retVal.= '<form action="'.$link.'" method="post">
      <table style="margin:auto">
          <tr>
              <td style="vertical-align:middle; width:100px">Gallery Title</td>
              <td><input type="text" name="gname" value="" size="109" maxlength="255" class="stdinput" style="width:820px" /></td>
          </tr>
          <tr>
              <td style="vertical-align:middle; padding-left:10px">Japanese Script (optional)</td>
              <td><input type="text" name="gname_jpn" value="" size="109" maxlength="255" class="stdinput" style="width:820px" /></td>
          </tr>
          <!--
          <tr>
              <td style="vertical-align:middle">Gallery Folder</td>
              <td><select id="gfldr" name="gfldr" class="stdinput" style="width:410px"><option value="0" selected="selected">Unsorted</option><option value="1">testing</option></select> or new folder: <input type="text" name="gfldrnew" value="" maxlength="60" class="stdinput" style="width:333px" /></td>
          </tr>
          -->
          <tr>
              <td style="vertical-align:top">Gallery Description</td>
              <td><textarea name="comment" cols="109" rows="18" class="stdinput" style="width:820px; height:300px"></textarea></td>
          </tr>';
    
    
    
    $retVal.='<tr>
              <td></td>
              <td><input type="checkbox" id="public" name="public" onclick="update_public()" /> <label for="public">Make this gallery publicly available for viewing under the category:</label>'.getCategoryHTML($categories).'</td>
          </tr>
          <tr>
              <td></td>
              <td><input type="checkbox" id="tos" name="tos" /> <label for="tos">I have read and agree with the terms of the</label> <a href="http://g.e-hentai.org/tos.php">Terms of Service</a></td>
          </tr>
          <tr>
              <td></td>
              <td><input type="submit" name="creategallery" value="Create and Continue" class="stdbtn" /><br /><br /><img src="http://ehgt.org/g/mr.gif" class="mr" alt="" /> <a href="'.BASE_HREF.'/manage">Back to Manager</a></td>
          </tr>
      </table>
      </form>
      </div>
      </div>';    
  
           
    return $retVal;
    
}



function getHTMLHeader($pageTitle){
    require_once 'pages/periph/navHeader.php';
    return '<!doctype html>
<html lang="en"><head>
<meta charset="utf-8">
<title>'.SITE_NAME.' - '.$pageTitle.'</title>
<link rel="stylesheet" type="text/css" href="'.BASE_HREF.'/css/g.css">
<meta name="robots" content="noarchive">
</head>
<body style="text-align:center">
<script type="text/javascript">
function popUp(URL,w,h) { 
	window.open(URL,"_pu"+(Math.random()+"").replace(/0\./,""),"toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width="+w+",height="+h+",left="+((screen.width-w)/2)+",top="+((screen.height-h)/2));
	return false;
}
</script>'.getHeader();
}
function getHTMLFooter(){
  $div = "";
  $div .=
    '<script type="text/javascript">
      <!--
        function update_public() {
            document.getElementById("publiccat").disabled=document.getElementById("public").checked?"":"disabled";
        }
        update_public();
      --><
      /script>';
  $div .= '<p class="ip">[<a href="'.BASE_HREF.'">Front Page</a>]</p>';
  $div .= '</body></html>';
  
  return $div;
    
}
/*

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>E-Hentai Galleries - The Free Hentai Doujinshi, Manga and Image Gallery System</title>
<link rel="stylesheet" type="text/css" href="https://upload.e-hentai.org/z/0320/g.css" />
<meta name="description" content="With more than 200,000 absolutely free hentai doujinshi, manga, cosplay and CG galleries, E-Hentai Galleries is the world's largest free Hentai archive." />
<meta name="RATING" content="RTA-5042-1996-1400-1577-RTA" />
<meta name="robots" content="noarchive" />
<meta name="juicyads-site-verification" content="0f3e47704e352bf534e98d4d45411fda" />
<link rel="alternate" type="application/atom+xml" title="E-Hentai Galleries Content Feed" href="http://g.e-hentai.org/rss/ehg.xml" />
<link rel="alternate" type="application/atom+xml" title="EHTracker Torrent Feed" href="http://g.e-hentai.org/rss/ehtracker.xml" />
<link rel="search" href="http://ehgt.org/g/opensearchdescription.xml" type="application/opensearchdescription+xml" title="E-Hentai Galleries Search" />

</head>
<body style="text-align:center">
<script type="text/javascript">
function popUp(URL,w,h) {
	window.open(URL,"_pu"+(Math.random()+"").replace(/0\./,""),"toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width="+w+",height="+h+",left="+((screen.width-w)/2)+",top="+((screen.height-h)/2));
	return false;
}
</script>
<p id="nb"><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://g.e-hentai.org/">Front Page</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://g.e-hentai.org/torrents.php">Torrents</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://g.e-hentai.org/favorites.php">Favorites</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://g.e-hentai.org/home.php">My Home</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="https://upload.e-hentai.org/manage.php">My Galleries</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://g.e-hentai.org/toplist.php">Toplists</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://g.e-hentai.org/bounty.php">Bounties</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://e-hentai.org/">News</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="https://forums.e-hentai.org/">Forums</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="https://ehwiki.org/wiki/Category:E-Hentai_Galleries">Wiki</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://hentaiverse.org/" onclick="popUp('http://hentaiverse.org/',1250,720); return false">HentaiVerse</a></p>
<div class="ui">
<div style="text-align:left">
<p style="font-weight:bold; font-size:10pt">Create a New Gallery</p>
<form action="https://upload.e-hentai.org/manage.php?act=new" method="post">
<table style="margin:auto">
	<tr>
		<td style="vertical-align:middle; width:100px">Gallery Title</td>
		<td><input type="text" name="gname" value="" size="109" maxlength="255" class="stdinput" style="width:820px" /></td>
	</tr>
	<tr>
		<td style="vertical-align:middle; padding-left:10px">Japanese Script (optional)</td>
		<td><input type="text" name="gname_jpn" value="" size="109" maxlength="255" class="stdinput" style="width:820px" /></td>
	</tr>
	<tr>
		<td style="vertical-align:middle">Gallery Folder</td>
		<td><select id="gfldr" name="gfldr" class="stdinput" style="width:410px"><option value="0" selected="selected">Unsorted</option><option value="1">testing</option></select> or new folder: <input type="text" name="gfldrnew" value="" maxlength="60" class="stdinput" style="width:333px" /></td>
	</tr>
	<tr>
		<td style="vertical-align:top">Gallery Description</td>
		<td><textarea name="comment" cols="109" rows="18" class="stdinput" style="width:820px; height:300px"></textarea></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="checkbox" id="public" name="public" onclick="update_public()" /> <label for="public">Make this gallery publicly available for viewing under the category:</label> <select id="publiccat" name="publiccat" class="stdinput"><option value="2">Doujinshi</option><option value="3">Manga</option><option value="4">Artist CG Sets</option><option value="5">Game CG Sets</option><option value="10">Western</option><option value="9">Non-H</option><option value="6">Image Sets</option><option value="7">Cosplay</option><option value="1" selected="selected">Misc</option></select></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="checkbox" id="tos" name="tos" /> <label for="tos">I have read and agree with the terms of the</label> <a href="http://g.e-hentai.org/tos.php">Terms of Service</a></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" name="creategallery" value="Create and Continue" class="stdbtn" /><br /><br /><img src="http://ehgt.org/g/mr.gif" class="mr" alt="" /> <a href="https://upload.e-hentai.org/manage.php">Back to Manager</a></td>
	</tr>
</table>
</form>
<script type="text/javascript"><!--
function update_public() {
	document.getElementById("publiccat").disabled=document.getElementById("public").checked?"":"disabled";
}
update_public();
--></script>
</div>
</div>
<p class="ip">[<a href="http://g.e-hentai.org/">Front Page</a>]</p>
</body>
</html>
*/