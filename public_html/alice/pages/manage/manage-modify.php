<?php
if(!defined('SITE_NAME')) {
   die('Direct access not permitted');
}

function manageModifyPage($mysqli){
    $gid = !empty($_GET['gid']) ? $_GET['gid'] : 0;
    $album = getAlbumInfo($mysqli,$gid);
    $errors = [];
    $gid = 0;
    try{
        if(
            isset($_POST['gname'])
            || isset($_POST['comment'])
            || isset($_POST['gname_jpn'])
            || isset($_POST['publiccat'])
        ){
            //post set
            //verify
            getCategories($mysqli);
            list($errors,$gid) = getPOSTResponse($mysqli);
            if(count($errors)==0){
                require 'manage-redirect.php';
                echoRedirectPage(SITE_NAME,
                      "The gallery has been successfully updated",
                       "You will be returned to the Management Interface momentarily",
                       "manage"
                      );
                die;
            }
        }
        getHTMLResponse($mysqli,$album,$errors);
    } catch (mysqli_sql_exception $e){
        error_log("manage-new MYSQL ERROR");
        error_log($e->getMessage());
        echo "Some database error happend, please try again later";
      
    } catch (RuntimeException $e) {
        error_log($e->getMessage());
        error_log("MYSQL_ERROR:".$mysqli->error);
        echo "Some error happened, please inform admin or try later";
    }
    
}

/*
function manageModifyPage($mysqli){
    $gid = !empty($_GET['gid']) ? $_GET['gid'] : 0;
    $album = getAlbumInfo($mysqli,$gid);
    getHTMLResponse($mysqli, $album);
}
*/
function getAlbumInfo($mysqli,$gid){
    
    $query = 'SELECT name, jap_name, category, c.comment as description  FROM albums a LEFT JOIN comments c ON a.gid=c.gid AND c.isOp=1 WHERE a.gid=?';
    
    if(!$stmt = $mysqli->prepare($query)){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        die;
    }
    if(!$stmt->bind_param('i',$gid)){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        die;
    }
    if(!$stmt->bind_result($name,$jap_name, $category, $description)){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        die;
    }
    if(!$stmt->execute()){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        die;
    }
    if(!$stmt->fetch()){
        error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        die;
    }
    return array(
        'name'=>$name,
        'jap_name'=>$jap_name,
        'category'=>$category,
        'descriptipn'=>$description,
        'gid'=>$gid
    );
}


//post response below, followed by base html
/*POST RESPONSE BELOW*/
function getPOSTResponse($mysqli){
    
    $errors = [];
    $errors[] = check_name();
    $errors[] = check_comment();
    $errors[] = check_publiccat();
    //returning null in the functions adds a bunch of blank stuff in arr
    //so let's get rid of it
    $errors = array_values(array_filter($errors));
    $gid = $_GET['gid'];
    if(count($errors) == 0){
        //no errors, so modify it?
        modify_album($mysqli,$gid);
        
    }

    return array($errors,$gid);
}

function modify_album($mysqli,$gid){
    $query = "UPDATE albums SET name=?, jap_name=?,category=? WHERE gid=?";

    if(!$stmt = $mysqli->prepare($query)){
        throw new RuntimeException('Database error ma1. Inform Admin');
    }
    if(!$stmt->bind_param('ssii',$_POST['gname'],$_POST['gname_jpn'],$_POST['publiccat'],$gid)){
        throw new RuntimeException('Database error ma3. Inform Admin');
    }
    if(!$stmt->execute()){
        throw new RuntimeException('Database error ma4. Inform Admin');
    }
    $stmt->close();
    
    $query = "UPDATE comments SET comment=? WHERE gid=? AND isOp=1";
    if(!$stmt = $mysqli->prepare($query)){
        throw new RuntimeException('Database error ma5. Inform Admin');
    }
    if(!$stmt->bind_param('si',$_POST['comment'],$gid)){
        throw new RuntimeException('Database error ma6. Inform Admin');
    }
    if(!$stmt->execute()){
        throw new RuntimeException('Database error ma7. Inform Admin');
    }
    $stmt->close();
}

function check_name(){
    if(!isset($_POST['gname']) || empty($_POST['gname']))
        return "Gallery title cannot be empty!";
    if(strlen($_POST['gname'])>512)
        return "Gallery title (eng/jap) must be less than 500 characters!";
    if(isset($_POST['gname_jpn']) && !empty($_POST['gname_jpn'])){
        if(strlen($_POST['gname_jpn'])>512)
            return "Gallery title (eng/jap) must be less than 500 characters!";
    } else {
        $_POST['gname_jpn'] = NULL;//to insert null into 
    }
        
        
            
    return null;
}
function check_comment(){
    
    if(!isset($_POST['comment']) || empty($_POST['comment']))
        return "You must have a description (just make up something short)";
        
    if(strlen($_POST['comment'])>512)
        return "Description must be less than 2000 characters";
        
    return null;
}
function check_publiccat(){
    $publiccat = $_POST['publiccat'];
    if($publiccat < 1)return "Invalid category";//should never be <0
    $cat = getCategories(null);//should work regardless
    for($i=0;$i<count($cat);$i++){
        if($cat[$i]['id'] == $publiccat)
            return null;//id matches one, no error
    }
    return "Invalid category";//no match
    
}
function getCategories($mysqli){
    #TODO move this caching somewhere else?
    if(isset($_SESSION['g_categories']))
        return $_SESSION['g_categories'];
    $query = "SELECT id,category FROM `categories` WHERE id<>1 ORDER BY priority DESC";

    if(!$stmt = $mysqli->prepare($query)){
        throw new RuntimeException('Database error mngc1. Inform Admin');
    }
    if(!$stmt->bind_result($id,$category)){
        throw new RuntimeException('Database error mngc3. Inform Admin');
    }
    if(!$stmt->execute()){
        throw new RuntimeException('Database error mngc4. Inform Admin');
    }

    $categories = [];
    while($stmt->fetch()){
        $categories[] = array(
            'id'=>$id,
            'category'=>$category
        );
    }
    $_SESSION['g_categories'] = $categories;
    return $categories;
}


/*
POST response above
Base html response down here
*/
function getHTMLResponse($mysqli,$album,$errors){
    echo getHTMLHeader("Modify Gallery (WIP)");
    echo getHTMLBody($album,getCategories($mysqli),$errors);
    echo getHTMLFooter();
}




function getCategoryHTML($cats,$selected){
    $retVal = "";
    $retVal.= '<select id="publiccat" name="publiccat" class="stdinput">';
    for($i=0;$i<count($cats);$i++){
        $selected_str = $cats[$i]['id'] == $selected ? "selected=selected" : '';
        $retVal.='<option '.$selected_str.' value="'.$cats[$i]['id'].'">'.$cats[$i]['category'].'</option>';
    }
        
    $retVal.= "</select>";
    return $retVal;
}

function getErrorHTML($errors){
    $retVal = "";
    
    $retVal.="<p>Some errors were found in your form</p>";
    for($i=0;$i<count($errors);$i++){
        $retVal.="<p>".$errors[$i]."</p>";
    }
    
    
    return $retVal;
}

function getHTMLBody($album,$categories,$errors){
    /*
    div.ui and everything inside
    */
    //add link in
    $link = BASE_HREF."/manage?act=modify&gid=".$album['gid'];
    
    $retVal = "";
    $retVal.= '<div class="ui">
      <div style="text-align:left">
      <p style="font-weight:bold; font-size:10pt">Modify Gallery (WIP)</p>';
    if(count($errors)>0){
        $retVal.=getErrorHTML($errors);
    }
        
    $retVal.= '<form action="'.$link.'" method="post">
      <table style="margin:auto">
          <tr>
              <td style="vertical-align:middle; width:100px">Gallery Title</td>
              <td><input type="text" name="gname" value="'.$album['name'].'" size="109" maxlength="255" class="stdinput" style="width:820px" /></td>
          </tr>
          <tr>
              <td style="vertical-align:middle; padding-left:10px">Japanese Script (optional)</td>
              <td><input type="text" name="gname_jpn" value="'.$album['jap_name'].'" size="109" maxlength="255" class="stdinput" style="width:820px" /></td>
          </tr>
          <!--
          <tr>
              <td style="vertical-align:middle">Gallery Folder</td>
              <td><select id="gfldr" name="gfldr" class="stdinput" style="width:410px"><option value="0" selected="selected">Unsorted</option><option value="1">testing</option></select> or new folder: <input type="text" name="gfldrnew" value="" maxlength="60" class="stdinput" style="width:333px" /></td>
          </tr>
          -->
          <tr>
              <td style="vertical-align:top">Gallery Description</td>
              <td><textarea name="comment" cols="109" rows="18" class="stdinput" style="width:820px; height:300px">'.$album['descriptipn'].'</textarea></td>
          </tr>';
    
    
    
    $retVal.='<tr>
              <td></td>
              <td><span>Category:</span>'.getCategoryHTML($categories,$album['category']).'</td>
          </tr>
          
          <tr>
              <td></td>
              <td><input type="submit" name="modifygallery" value="Modify Gallery" class="stdbtn" /><br /><br /><img src="http://ehgt.org/g/mr.gif" class="mr" alt="" /> <a href="'.BASE_HREF.'/manage">Back to Manager</a></td>
          </tr>
      </table>
      </form>
      </div>
      </div>';    
  
           
    return $retVal;
    
}



function getHTMLHeader($pageTitle){
    require_once 'pages/periph/navHeader.php';
    return '<!doctype html>
<html lang="en"><head>
<meta charset="utf-8">
<title>'.SITE_NAME.' - '.$pageTitle.'</title>
<link rel="stylesheet" type="text/css" href="'.BASE_HREF.'/css/g.css">
<meta name="robots" content="noarchive">
</head>
<body style="text-align:center">
<script type="text/javascript">
function popUp(URL,w,h) { 
	window.open(URL,"_pu"+(Math.random()+"").replace(/0\./,""),"toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width="+w+",height="+h+",left="+((screen.width-w)/2)+",top="+((screen.height-h)/2));
	return false;
}
</script>'.getHeader();
}
function getHTMLFooter(){
  $div = "";
  $div .=
    '<script type="text/javascript">
      <!--
        function update_public() {
            document.getElementById("publiccat").disabled=document.getElementById("public").checked?"":"disabled";
        }
        update_public();
      --><
      /script>';
  $div .= '<p class="ip">[<a href="'.BASE_HREF.'">Front Page</a>]</p>';
  $div .= '</body></html>';
  
  return $div;
    
}
