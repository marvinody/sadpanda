<?php
if(!defined('SITE_NAME')) {
   die('Direct access not permitted');
}

require_once 'core/connect.php';
require_once 'core/authenticate.php';
date_default_timezone_set('UTC');
savePageSession();//from loadUserPref

if(authenticate($mysqli,true)){
    //do nothing cause good.
    $gid = !empty($_GET['gid']) && is_numeric($_GET['gid']) ? $_GET['gid'] : 0;
    if($gid>0){
        //check if the user has the correct album and owns ti 
      if(check_ownership($mysqli,$gid)){
        //you're good, don't need to do anything here
      } else {
        //display page that gid is wrong
        die("You don't seem to own that gid, bud");
      }
    } else {
        /*
        if(isset($_GET['act'])){
            header("Location: ".BASE_HREF."/manage");
            die;
        }
        */
    }
    
} else {
    //send to relogin
    header("Location: ".BASE_HREF."/login");
    die;
}
$act = isset($_GET['act']) ? $_GET['act'] : "";
/*
so, to prevent caching issues with no change being detected, 
going to delete cached gallery when user goes to the index
*/
switch($act){
    case "eh":
        require 'manage-cut.html';
        break;
    case "reorder":
    require 'manage-reorder.php';
        manageReorderPage($mysqli);
        break;
    case "add":
        require 'manage-add.php';
        manageAddPage($mysqli);
        break;
    case "new":
        require 'manage-new.php';
        manageNewPage($mysqli);
        break;

    case "preview"://user should never get here cause changed url to not point to this anymore
        header("Location: ".BASE_HREF."/g/".$_GET['gid']);
        die;
        
    
    case "modify":
        require 'manage-modify.php';
        manageModifyPage($mysqli);
        break;

    case "publish":
    case "unpublish":
        require 'manage-publish.php';
        managePublishPage($mysqli);
        break;
    case "delete":
    case "multiedit":
        require 'manage-redirect.php';
        echoRedirectPage(SITE_NAME,
              "This feature has not been implemented",
               "You will be returned to the Management Interface momentarily",
               "manage"
              );
        break;
    default:
        if(isset($_SESSION['gallery'])){
            unset($_SESSION['gallery']);   
        }
        require 'manage-index.php';
        manageMainPage($mysqli);
        
}



/*





*/