<?php
function manageMainPage($mysqli){
    $albums = getGalleriesForUser($_SESSION['user']['id'],$mysqli);
    echo getHTMLHeader("Manage - Main Page");
    echo getHTMLBody($albums);
    echo getHTMLFooter();
}

function getGalleriesForUser($userId,$mysqli){
    $query = 
        "SELECT a.name,a.gid,c.category, DATE_FORMAT(a.timestamp, '%Y-%m-%d %H:%i') AS timestamp,a.hidden,a.published, COUNT(p.id) FROM `albums` a
            LEFT JOIN pages p on a.gid=p.gid AND p.hidden =0
            LEFT JOIN categories c on  a.category=c.id
            WHERE a.uploaderId=? AND a.hidden=0 GROUP BY a.gid";
    $albums = [];
    $albums = array(
        'unpublished'=>[],
        'published'=>[],
        'unlisted'=>[],
    );
    if ($stmt = $mysqli->prepare($query)) {
        $stmt->bind_param("i",$userId);
        $stmt->execute();
        $stmt->bind_result($name,$gid,$category,$timestamp,$hidden,$published,$pageCount);
        while ($stmt->fetch()) {
            #if it's the dummy cat (user hasn't chosen yet, then don't display it)
            $category = $category=="dummy" ? "-" : $category;
            if($published){
                if($hidden){
                    $albums['unlisted'][] = array(
                        'name'=>htmlEntities($name, ENT_QUOTES),
                        'gid'=>$gid,
                        'category'=>$category,
                        'timestamp'=>$timestamp,
                        'pageCount'=>$pageCount,
                    );
                } else {
                    $albums['published'][] = array(
                        'name'=>htmlEntities($name, ENT_QUOTES),
                        'gid'=>$gid,
                        'category'=>$category,
                        'timestamp'=>$timestamp,
                        'hidden'=>$hidden,
                        'published'=>$published,
                        'pageCount'=>$pageCount,
                    );
                }
            } else {
                $albums['unpublished'][] = array(
                        'name'=>htmlEntities($name, ENT_QUOTES),
                        'gid'=>$gid,
                        'category'=>$category,
                        'timestamp'=>$timestamp,
                        'hidden'=>$hidden,
                        'published'=>$published,
                        'pageCount'=>$pageCount,
                    );
                
            } 
        }
        $stmt->close();
    } else {
         error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        die('Database error, try again later or yell at admin');
    }
    return $albums;
    
}

function getHTMLBody($albums){
    $retVal = ' <div class="ui" style="text-align:left"><p style="font-weight:bold; font-size:10pt">Your Galleries</p>
    <p><img src="'.BASE_HREF.'/images/mr.gif" class="mr" alt=">"> <a href="'.BASE_HREF.'/manage?act=new">Create a New Gallery</a></p>
<form action="'.BASE_HREF.'/manage?act=multiedit" method="post" id="mgform">';
    $retVal .= getUnpublishedAlbumsHTML($albums);
    $retVal .= getPublishedAlbumsHTML($albums);
    $retVal .= "</div>";
    return $retVal;
}

function getPublishedAlbumsHTML($albums){
    $retVal = '
    <div>
        <div style="padding:5px 2px; font-weight:bold">
            <div style="float:left">Galleries published, available publicly</div>
            <div class="c"></div>
            </div>
        <table id="gtable3" class="mt">
        <tbody>
            <tr>
                <th style="text-align:left; width:390px; padding-left:2px">Gallery Name  </th>
                <th style="text-align:left; width:100px">Date Added  </th>
                <th style="text-align:right; width:40px; padding-right:10px">Files</th>
                <th style="text-align:center; width:110px">Category</th>
                <th style="text-align:left; width:290px">Available Actions</th>
                <th style="text-align:left; width:20px"></th>
            </tr>';
            for($i=0;$i<count($albums['published']);$i++){
                $retVal .= generatePublishedAlbumRow($albums['published'][$i]);
            }
    $retVal .='</tbody></table>
    </div>';
    return $retVal; 
    
}

function getUnpublishedAlbumsHTML($albums){
    $retVal = '
    <div>
        <div style="padding:5px 2px; font-weight:bold">
            <div style="float:left">Galleries under construction, or being modified</div>
            <div class="c"></div>
            </div>
        <table id="gtable2" class="mt">
        <tbody>
            <tr>
                <th style="text-align:left; width:390px; padding-left:2px">Gallery Name  </th>
                <th style="text-align:left; width:100px">Date Added  </th>
                <th style="text-align:right; width:40px; padding-right:10px">Files</th>
                <th style="text-align:center; width:110px">Category</th>
                <th style="text-align:left; width:290px">Available Actions</th>
                <th style="text-align:left; width:20px"></th>
            </tr>';
            for($i=0;$i<count($albums['unpublished']);$i++){
                $retVal .= generateUnpublishedAlbumRow($albums['unpublished'][$i]);
            }
    $retVal .='</tbody></table>
    </div>';
    return $retVal; 
    
}

function generatePublishedAlbumRow($album){
    //changed this because it means I don't have to make dedicated preview page
    //$preview_url = BASE_HREF.'/manage?act=preview&amp;gid='.$album['gid'];
    $preview_url = BASE_HREF.'/g/'.$album['gid'];
    return '<tr id="gr'.$album['gid'].'" class="gtr1">
	<td class="gtc1"><a href="'.$preview_url.'">'.$album['name'].'</a></td>
	<td class="gtc2">'.$album['timestamp'].'</td>
	<td class="gtc3">'.$album['pageCount'].'</td>
	<td class="gtc4">'.$album['category'].'</td>
	<td class="gtc5"> [<a href="'.BASE_HREF.'/manage?act=add&amp;gid='.$album['gid'].'">Add Files</a>] [<a href="'.BASE_HREF.'/manage?act=reorder&amp;gid='.$album['gid'].'">Reorder</a>] [<a href="'.BASE_HREF.'/manage?act=modify&amp;gid='.$album['gid'].'">Modify</a>] [<a href="'.BASE_HREF.'/manage?act=unpublish&amp;gid='.$album['gid'].'">Unpublish</a>] [<a href="'.BASE_HREF.'/manage?act=delete&amp;gid='.$album['gid'].'">Delete</a>]</td>
	<td class="gtc6"><input type="checkbox" id="f'.$album['gid'].'" name="f'.$album['gid'].'"></td>
</tr>';
}



function generateUnpublishedAlbumRow($album){
    //$preview_url = BASE_HREF.'/manage?act=preview&amp;gid='.$album['gid'];
    $preview_url = BASE_HREF.'/g/'.$album['gid'];
    return '<tr id="gr'.$album['gid'].'" class="gtr1">
	<td class="gtc1"><a href="'.$preview_url.'">'.$album['name'].'</a></td>
	<td class="gtc2">'.$album['timestamp'].'</td>
	<td class="gtc3">'.$album['pageCount'].'</td>
	<td class="gtc4">'.$album['category'].'</td>
	<td class="gtc5"> [<a href="'.BASE_HREF.'/manage?act=add&amp;gid='.$album['gid'].'">Add Files</a>] [<a href="'.BASE_HREF.'/manage?act=reorder&amp;gid='.$album['gid'].'">Reorder</a>] [<a href="'.BASE_HREF.'/manage?act=modify&amp;gid='.$album['gid'].'">Modify</a>] [<a href="'.BASE_HREF.'/manage?act=publish&amp;gid='.$album['gid'].'">Publish</a>] [<a href="'.BASE_HREF.'/manage?act=delete&amp;gid='.$album['gid'].'">Delete</a>]</td>
	<td class="gtc6"><input type="checkbox" id="f'.$album['gid'].'" name="f'.$album['gid'].'"></td>
</tr>';
}


function getHTMLHeader($pageTitle){
    require_once 'pages/periph/navHeader.php';
    return '<!doctype html>
<html lang="en"><head>
<meta charset="utf-8">
<title>'.SITE_NAME.' - '.$pageTitle.'</title>
<link rel="stylesheet" type="text/css" href="'.BASE_HREF.'/css/g.css">
<style type="text/css">

td.gtc1{height:22px; padding-left:10px;text-align:left}
td.gtc2{}
td.gtc3{padding-right:10px; text-align:right}
td.gtc4{text-align:center}
td.gtc5{}
td.gtc6{text-align:center}

</style>
<meta name="robots" content="noarchive">
<style id="__web-inspector-hide-shortcut-style__" type="text/css">
.__web-inspector-hide-shortcut__, .__web-inspector-hide-shortcut__ *, .__web-inspector-hidebefore-shortcut__::before, .__web-inspector-hideafter-shortcut__::after
{
    visibility: hidden !important;
}
</style></head>
<body style="text-align:center">
<script type="text/javascript">
function popUp(URL,w,h) { 
	window.open(URL,"_pu"+(Math.random()+"").replace(/0\./,""),"toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width="+w+",height="+h+",left="+((screen.width-w)/2)+",top="+((screen.height-h)/2));
	return false;
}
</script>'.getHeader();
}
function getHTMLFooter(){
    return '
<script type="text/javascript">
var ss = "d";
var sd = "d";
var base_url = "'.BASE_HREF.'/";
var manage_url = "'.BASE_HREF.'/manage";
var apiuid = 1637569;
var apikey = "8f566e3bf225c41d8c67";
</script>


</form>

<script type="text/javascript" src="'.BASE_HREF.'/js/gallery.js"></script>
<script type="text/javascript" src="'.BASE_HREF.'/js/manage.js"></script>
</div>
<p class="ip">[<a href="'.BASE_HREF.'">Front Page</a>]</p>


</body></html>';
}