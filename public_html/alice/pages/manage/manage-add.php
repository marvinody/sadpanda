<?php
if(!defined('SITE_NAME')) {
   die('Direct access not permitted');
}
$FILE_INPUT_PREFIX = "file";
$debug = false;
error_reporting(-1);
function manageAddPage($mysqli){
    //$albums = getGalleriesForUser($_SESSION['user']['id'],$mysqli);
    $gid = !empty($_GET['gid']) ? $_GET['gid'] : 0;
    if($gid==0)return;
    $errors = [];
    if(isset($_FILES['file01'])){
        
      
        list($errors,$pagesSuccessful) = getPOSTResponse($mysqli,$gid);
        if($pagesSuccessful > 0){
            $act = "preview";
            if($_POST['ulact']=='ulmore'){
                $act = 'add';
            } else if ($_POST['ulact']=='uldone'){
                $act = 'reorder';
            }
            header('Location: '.BASE_HREF.'/manage?act='.$act.'&gid='.$gid);
        }
    }
    getHTMLResponse($mysqli, $gid, $errors);
    
}

/*
POST response below
*/
function getPaddedNum($i){return sprintf('%02d',$i);}

function getNextPageNumbers($mysqli,$gid){
  $query = "SELECT coalesce(MAX(page),0) + 1 AS nextPage, 
    coalesce(MAX(actualPageOrder),0) + 1 AS nextActualPage FROM `pages` WHERE gid=?";
  $nextPage = -1;
  $nextActualPage = -1;
  if(!$stmt = $mysqli->prepare($query)){
      throw new RuntimeException('Database error ghpn1. Inform Admin');
  }
  if(!$stmt->bind_param('i',$gid)){
      throw new RuntimeException('Database error ghpn2. Inform Admin');
  }
  if(!$stmt->bind_result($nextPage,$nextActualPage)){
      throw new RuntimeException('Database error ghpn3. Inform Admin');
  }
  if(!$stmt->execute()){
      throw new RuntimeException('Database error ghpn4. Inform Admin');
  }
  if(!$stmt->fetch()){
      throw new RuntimeException('Database error ghpn5. Inform Admin');
  }
  return array($nextPage,$nextActualPage);
}

function getPOSTResponse($mysqli,$gid){
    list($nextPage,$nextActualPage) = getNextPageNumbers($mysqli,$gid);
    $totalPagesSuccessful = 0;
    global $debug;
    if($debug){
        echo $nextPage."<br>";
        echo $nextActualPage."<br>";
    }
    $errors = [];
    for($i=1;$i<15;$i++){
    
        try{
            $val = checkFormFile($i);
            #$val = 0;
            if(fileAlreadyExistsOnDrive($mysqli,$gid,$i)){
                //do something here but don't care about dupes rn
            }
            if($val==0 || $val==1){
                uploadFormFile($mysqli,$gid,$i,$nextPage,$nextActualPage,$val);
            } else {
                throw new RuntimeException('Invalid value found when checking file',5);
            }
            $nextPage++;
            $nextActualPage++;
            $totalPagesSuccessful++;


        } catch (mysqli_sql_exception $e){
            if (isset($debug) && $debug){
                echo $e->getMessage();
            }
            error_log("manage-add MYSQL ERROR");
            error_log($e->getMessage());

        } catch (RuntimeException $e) {
            
            
            if($e->getCode()==1)continue;//no file present, so we don't really care
            error_log($e->getMessage());
            $errors[] = $e->getMessage();
        }
    }
    return array($errors,$totalPagesSuccessful);
  
  
  
}

function uploadFormFile($mysqli,$gid,$i,$nextPage,&$nextActualPage,$val){
    global $FILE_INPUT_PREFIX;//this is defined above and is a result of html
    $file_string = $FILE_INPUT_PREFIX.getPaddedNum($i);
    $file = $_FILES[$file_string];
    //we just make something up to get it out of tmp and treat it more dynamic
    //so we can use process for other stuff
    $stagedFile = sprintf("%s/%s_%s",
                          IMAGE_STAGING_DIR, "tmp", $i);
    
    
    if (!move_uploaded_file(
        $file['tmp_name'],
        $stagedFile
    )) {
        throw new RuntimeException('Failed to move original image to server!',10);
    }
    $name = $file['name'];
    if($val == 0){
        upload_image($mysqli,$gid,$stagedFile,$name,$nextPage,$nextActualPage);
    } else if ($val==1){
        upload_zip($mysqli,$gid,$stagedFile,$nextPage,$nextActualPage);
    }
    
    
    //before we forget
    unlink($stagedFile);
    
    
    
}

function upload_image($mysqli,$gid,$stagedFile,$name,$nextPage,$nextActualPage){
    //This is hardcoded in the htaccess file. So if you change this, change the other
    $SHORT_URL_LENGTH = 8;
    
    $rawFilesize = 1.0*1024*1024;//1meg
    $maxWidth = 1280;
    
    $info = process_image($stagedFile, $maxWidth, $rawFilesize);
    
    
    //and this is how we access it online. TODO make random because it's pretty
    //simple to rev. eng. and see I use sha256 for id
    //does that matter tho?
    //just means that same url for same hash image. Only matters if user uploads 2 different images with same hash
    //results in first image(?) being always displayed. Can fix later if needed
    
    /*
    $b64hash = base64_encode($info['hash']);
    

    //get shortURL that we use to locate it using web
    $shortID = substr($b64hash,0,$SHORT_URL_LENGTH);
    */
    //READ ABOVE COMMENTS TO SEE THE REASONING
    $shortID = substr($info['hash'],0,$SHORT_URL_LENGTH);
    
    //just does regular insert with supplied info
    insert_page_to_db($mysqli, $gid, $shortID, $name, $info, $nextPage, $nextActualPage);
    
    move_files_to_upload_dir($info);
}

function move_files_to_upload_dir($info){
    //get the final uploaded filenames
    $server_uploaded_file = sprintf(
        '%s/%s',
        IMAGE_UPLOAD_DIR,
        $info['hash']
    );
    //thumbnail filename
    $server_uploaded_thumbnail = sprintf(
        '%s/%s%s',
        IMAGE_UPLOAD_DIR,
        $info['hash'],
        THUMBNAIL_SUFFIX
    );
    //only use this if needed
    $server_uploaded_raw = sprintf(
        '%s/%s%s',
        IMAGE_UPLOAD_DIR,
        $info['hash'],
        RAW_SUFFIX
    );
    
    //now we move all the files. Main, thumbnail and then raw if we have
    if (!rename(
      $info['main'],
      $server_uploaded_file
    )) {
        throw new RuntimeException('Failed to move main image from stage',11);
    }
    if (!rename(
      $info['thumbnail'],
      $server_uploaded_thumbnail
    )) {
        throw new RuntimeException('Failed to move thumb image from stage',12);
    }
    if ($info['hasRaw'] && !rename(
      $info['raw'],
      $server_uploaded_raw
    )) {
        throw new RuntimeException('Failed to move raw image from stage',13);
    }
}

function insert_page_to_db($mysqli, $gid, $shortID, $name, $info,$nextPage,$nextActualPage){
    //turn on error reporting because I had issues before and catching all now
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $query = "INSERT INTO pages 
    (gid,shortID,name,page,width,height,thumb_width,thumb_height,filesize,hash,hasRaw,actualPageOrder) VALUES 
    (?,  ?,      ?,   ?,   ?,    ?,     ?,          ?,           ?,       ?,  ?,     ?)";
    if(!$stmt = $mysqli->prepare($query)){
        throw new RuntimeException('Database error fu1. Inform Admin');
    }
    
    $kb_filesize = round($info['filesize']/1024);
    if(!$stmt->bind_param('issiiiiiisii',
                        $gid,$shortID,$name,
                          $nextPage,$info['width'],$info['height'],
                          $info['thumb_width'],$info['thumb_height'],
                          $kb_filesize,$info['hash'],
                          $info['hasRaw'],$nextActualPage)){
        throw new RuntimeException('Database error fu2. Inform Admin');
    }

    if(!$stmt->execute()){
        throw new RuntimeException('Database error fu4. Inform Admin');
    }
    $pageId = $stmt->insert_id;
    $stmt->close();
    
    $query = "INSERT INTO pageHashes (pageId,isRaw,filehash,width,height,filesize) VALUES (?,?,?,?,?,?)";
    if(!$stmt = $mysqli->prepare($query)){
        throw new RuntimeException('Database error phi1. Inform Admin');
    }
    
    $isRaw = 0;
    $filehash = $info['hash'];
    $width = $info['width'];
    $height = $info['height'];
    $kb_filesize = round($info['filesize']/1024);
    if(!$stmt->bind_param('iisiii',$pageId,$isRaw,$filehash,$width,$height,$kb_filesize)){
        throw new RuntimeException('Database error phi2. Inform Admin');
    }
    if(!$stmt->execute()){
        throw new RuntimeException('Database error phi3. Inform Admin');
    }
    if($info['hasRaw']){
        $isRaw = 1;
        $filehash = $info['raw_hash'];
        $width = $info['raw_width'];
        $height = $info['raw_height'];
        $kb_filesize = round($info['raw_filesize']/1024);
        if(!$stmt->execute()){
            throw new RuntimeException('Database error phi3. Inform Admin');
        }
    }
    
}

function process_image($fullFilePath, $maxWidth, $maxFilesize) {
    /*
    file must bethe path+filename. 
    Path should always be staged folder (defined in config)
    So one must move it (move_uploaded_file) if needed before calling this func
    if passed file (must be in staged folder) is (any of following)
        width > 1280
        filesize > maxfilesize
        not jpg
    then we process it and try to create an alternate copy
    
    we also always create a thumbnail to go with it. 
        thumbnail will be of the 'alternate' if exists
        
    return is array with different things
        hasRaw := if we processed it somehow
        thumbnail := name of staged thumbnail
        main := name of staged processed image
        raw := name of original if processed (staged)
    
    */
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    $mimeType = $finfo->file($fullFilePath);
    
    $sha256 = hash_file("sha256",$fullFilePath);
    
    $return = array(
        'hasRaw'=>0,
        'thumbnail'=>sprintf(
            '%s/%s%s',
            IMAGE_STAGING_DIR,
            $sha256,
            THUMBNAIL_SUFFIX
        ),
        'main'=>sprintf(
            '%s/%s',
            IMAGE_STAGING_DIR,
            $sha256
            
        ),
        'hash'=>$sha256,
        'filesize'=>filesize($fullFilePath),
    );
    
    
    list($width, $height) = getimagesize($fullFilePath);
    $return['width'] = $width;
    $return['height'] = $height;
    $filesize = $return['filesize'];
    if( $width > $maxWidth 
       || $filesize > $maxFilesize
       || $mimeType == 'image/png'){
        //create alternate in here
        $raw = sprintf(
            '%s/%s%s',
            IMAGE_STAGING_DIR,
            $sha256,
            RAW_SUFFIX
            
        );
        switch($mimeType){
            case "image/png":
                $src = imagecreatefrompng($fullFilePath);
            break;
            case "image/jpeg":
                $src = imagecreatefromjpeg($fullFilePath);

        }
        $newWidth = $maxWidth;
        $newHeight = round($newWidth / $width * $height);
        $dst = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
        imagejpeg($dst, $return['main'], JPG_QUALITY);
        copy($fullFilePath, $raw);
        $return['hasRaw'] = 1;
        $return['raw'] = $raw;
        $return['raw_hash'] = $return['hash'];
        $return['raw_width'] = $return['width'];
        $return['raw_height'] = $return['height'];
        $return['raw_filesize'] = $return['filesize'];
        $return['hash'] = hash_file("sha256",$return['main']);
        $return['width'] = $newWidth;
        $return['height'] = $newHeight;
        $return['filesize'] = filesize($return['main']);
        
    } else {
        //everything should be fine, so we'll just copy the file to correct name
        copy($fullFilePath, $return['main']);
    }
    list($return['thumb_width'],$return['thumb_height']) 
        = makeJPEGThumbnail($return['main'],$return['thumbnail'],$return['width'],$return['height']);
    
    return $return;
    
}


//takes $in file, and $out location and returns new dimensions
//does not touch original
function makeJPEGThumbnail($in,$out,$width,$height){

    $myImage = imagecreatefromjpeg($in);
    $newWidth = 200;
    $newHeight = round($newWidth*$height/$width);

    $image_p = imagecreatetruecolor($newWidth, $newHeight);
    imagecopyresampled($image_p, $myImage, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
    imagejpeg($image_p, $out, 100);
    return array($newWidth,$newHeight);
}

function fileAlreadyExistsOnDrive($mysqli,$gid,$i){
  return false;
  //don't care about duplicates right now
  $file_string = $FILE_INPUT_PREFIX.getPaddedNum($i);
  $file = $_FILES[$file_string];
  $file_md5 = md5_file($file['tmp_name']);
  $query = "SELECT id FROM files,shortURL WHERE files.userId = shortURL.userId AND shortURL.url=? AND files.shortFileHash=?";
  if(!$stmt = $mysqli->prepare($query)){
      throw new RuntimeException('Database error fav1. Inform Admin');
  }
  if(!$stmt->bind_param('ss',$user,$fileHash)){
      throw new RuntimeException('Database error fav2. Inform Admin');
  }
  if(!$stmt->bind_result()){
      throw new RuntimeException('Database error fav3. Inform Admin');
  }
  if(!$stmt->execute()){
      throw new RuntimeException('Database error fav4. Inform Admin');
  }
  if(!$stmt->fetch()){
      throw new RuntimeException('Database error fav5. Inform Admin');
  }
  
}
/*
RETURN CODES:
0: default, everything ok, do nothing special
1: file is a zip file

ERROR CODES
1: no file sent
2: exceeded filesize limit (either form or hardcoded limit)
3: some other error cause modifying data
4: File format not allowed
*/
function checkFormFile($index){
  global $FILE_INPUT_PREFIX;
  $file_string = $FILE_INPUT_PREFIX.getPaddedNum($index);
  $file = $_FILES[$file_string];
  
  switch ($file['error']) {
    case UPLOAD_ERR_OK:
      break;
    case UPLOAD_ERR_NO_FILE:
      throw new RuntimeException('No file sent.',1);
    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
      throw new RuntimeException('Exceeded form filesize limit.',2);
    default:
      throw new RuntimeException('Unknown errors.',3);
    
        
  }
  return checkFile($file['tmp_name']);
          
}
/*
RETURN CODES:
0: default, everything ok, do nothing special
1: file is a zip file
*/
function checkFile($filename){
    /*CONFIG FOR ALLOWED DATA*/
    $maxFileSize = 5*1024*1024;
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    $possExts = array(
        'jpg' => 'image/jpeg',
        'png' => 'image/png',
        'zip' => 'application/zip',
        //'gif' => 'image/gif',
    );
    
    $filesize = filesize($filename);
    
    $mime = $finfo->file($filename);
    $ext = array_search(
        $mime,
        $possExts,
        true
    );
    if (false === $ext) {
        throw new RuntimeException('Invalid file format.',4);
    }
    if($ext == 'zip'){
        if ($filesize < MAX_SIZE_PER_ZIP){
            return 1;
        } else {
            throw new RuntimeException('Exceeded SadPanda\'s filesize limit.',2);
        }
    }

    if( $filesize > $maxFileSize ){
        throw new RuntimeException('Exceeded SadPanda\'s filesize limit.',2);
    }
    
    return 0;
    
}
function zip_file_is_folder($filename){
    return $filename[strlen($filename)-1]=='/';
}
function extract_single_file($zipFile,$wantedFile,$dst){
    return copy(sprintf("zip://%s#%s",$zipFile,$wantedFile),$dst);
}



function get_top_level_size($zipResource){
    $total = $zipResource->numFiles;
    $size = 0;
    for($i=0;$i<$total;$i++){
        $size += $zipResource->statIndex($i)['size'];
    }
    return $size;
}

function check_zip($zipResource){
    $files = $zipResource->numFiles;
    if($files > MAX_FILES_PER_ZIP){
        throw new RuntimeException('Too many files in zip. Max:'.MAX_FILES_PER_ZIP);
    }
    $size = get_top_level_size($zipResource);
    if($size > MAX_SIZE_PER_ZIP){
        throw new RuntimeException('Uncompressed zip file is too large. Max:'.MAX_SIZE_PER_ZIP);
    }
    
}


function upload_zip($mysqli,$gid,$stagedFile,&$nextPage,&$nextActualPage){
    
    
    $zip = new ZipArchive;
    if($zip->open($stagedFile) !== true){
        throw new RuntimeException('Error opening zip archive');
    }
    check_zip($zip);
    
    $totalFiles = $zip->numFiles;
    for($i=0;$i<$totalFiles;$i++){
        try{
            $tmp_file = tempnam(IMAGE_STAGING_DIR,'tmp_zip_file_');
            $cur_file_data = $zip->statIndex($i);
            if(zip_file_is_folder($cur_file_data['name'])){
                continue;
            }
            //now we know we have a file
            extract_single_file($stagedFile,$cur_file_data['name'],$tmp_file);
            
            $val = checkFile($tmp_file);
            if($val==0){//is image type?
                $name = pathinfo($cur_file_data['name'])['basename'];
                upload_image($mysqli, $gid, $tmp_file, $name, $nextPage, $nextActualPage);
                $nextPage++;
                $nextActualPage++;
            } else {
                continue;
            }
            
            
        } catch (Exception $e){
            throw $e;
        } finally {
            unlink($tmp_file);
        }

        
    }

    
    
    
}


/*
POST response above
Base html response down here
*/
function getHTMLResponse($mysqli, $gid,$errors){
    $pages = getAlbumPages($mysqli,$gid);
    $title = getGalleryTitle($mysqli,$gid);
    echo getHTMLHeader("Create new gallery");
    echo getHTMLBody($pages,$gid,$title,$errors);
    echo getHTMLFooter($pages);
}
function getHTMLBody($pages,$gid,$galleryTitle,$errors){
  /*
    div.ui and everything inside
  */
  //add link in
  $link = BASE_HREF."/manage?act=add&gid=$gid";
  $retVal = "";
  $retVal.= '<div class="ui">';
    $retVal.='<div class="stuffbox" style="width:980px; margin:10px auto 5px auto; padding:3px">';
    $retVal.='<form id="uploadform" action="'.$link.'" method="post" enctype="multipart/form-data" class="nopm">';
      $retVal.='<div style="text-align:right; float:left; width:480px; height:350px; padding-right:5px; position:relative; z-index:2; border-right:1px solid #5C0D12">';
        $retVal.='<span style="font-weight:bold">Upload new files to gallery: '.$galleryTitle.'</span>';
        $start = 1;$numFileInputs = 14;$endRange = $start+$numFileInputs+1;
        $lastPicCount = count($pages);
        for($i = $start;$i<$endRange;$i++){
          $retVal.= getFileInputHTML($i, $i + $lastPicCount);
        }
      $retVal.='</div>';
      $retVal.='<div style="text-align:left; float:left; width:480px; height:350px; padding-left:5px; overflow:auto; position:relative; z-index:1">';
        $retVal.='<span style="font-weight:bold">Files uploaded so far</span>:';
        for($i=0;$i<$lastPicCount;$i++){
          $retVal.="<br>".getFileInfoHTML($pages[$i]);
          
        }
        $retVal.="<br>";
        #$retVal.="<pre>".print_r($_POST)."</pre>";
      $retVal.='</div>';
      
      $retVal.=getUploadFooter($errors);
  
    $retVal.='</form>';
  $retVal.='</div>';    
  
           
    return $retVal;
    
}
function getUploadFooter($errors){
  $retVal = "";
  $retVal.= '<div style="clear:both; width:980px; margin:auto; border-top:1px solid #5C0D12">
                    <table style="margin:auto">
                        <tbody>
                            <tr>
                                <td colspan="2" style="text-align:center">
                                    <input type="hidden" name="MAX_FILE_SIZE" value="1258291200" class="nopm">
                                    <input type="hidden" id="ulact" name="ulact" value="fillme" class="nopm">
                                    <span style="font-weight:bold">Accepted Image Formats: JPG, PNG<!--, GIF-->. Accepted Archive Formats: <!--RAR,--> ZIP. Max File Size: 5MB per Image. Max Resolution: 20000 x 20000.<br>
  Max 2,000 files per gallery. You should upload no more than 100MB at a time, less if you have a slow connection.<br>
                                    Do not refresh while uploading or you might end up with half of the images processed and more work for you.
                                    </span>
                                    <br>
                                    <br> To upload the files listed above and return here to continue adding files, hit Upload and Add More.
                                    <br> To upload these files and continue to the next step, hit Upload and Continue.
                                    <br> If you need to remove or reorder files, you can do so at the next step.
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right">
                                    <input style="width:150px" type="button" id="ulmore" name="ulmore" onclick="submit_upload(\'ulmore\'); return false" value="Upload and Add More" class="stdbtn">
                                </td>
                                <td style="text-align:left">
                                    <input style="width:150px" type="button" id="uldone" name="uldone" onclick="submit_upload(\'uldone\'); return false" value="Upload and Continue" class="stdbtn">
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>';
  return $retVal;
  
}
function getFileInfoHTML($page){
  $str = "";
  $str.= $page['name'].', '.$page['width'].'x'.$page['height'].', '.$page['filesize'].' KB';
  return $str;
}
function getFileInputHTML($inputNumber = 1, $tagNumber = 1){
  $paddedInputNum = sprintf('%02d',$inputNumber);
  $paddedTagNum = sprintf('%04d',$tagNumber);
  $retVal = "";
  $retVal.= "<p class=\"nopm\">$paddedTagNum";
    $retVal.= '<input type="file" name="file'.$paddedInputNum.'" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">';
  $retVal.="</p>";
  
  return $retVal;
  
}

function getGalleryTitle($mysqli,$gid){
    $query = "SELECT name FROM albums WHERE gid=?";
    if($stmt = $mysqli = $mysqli->prepare($query)){
        $stmt->bind_param('i',$gid);
        $stmt->execute();
        $stmt->bind_result($name);
        $stmt->fetch();
        $stmt->close();
        return $name;
    } else {
        error_log("Error getting gallery title for $gid in manage-add");
        return "N/A";
    }
    
}

function getAlbumPages($mysqli,$gid){
    $query = "SELECT hash,shortId,p.name,width,height,filesize FROM pages p, albums a  WHERE p.gid=? AND a.uploaderId=? AND a.gid=p.gid AND p.hidden=0 ORDER BY actualPageOrder DESC LIMIT 14";
    #only get 14 and most recent

    if ($stmt = $mysqli->prepare($query)) {
        $stmt->bind_param("ii",$gid,$_SESSION['user']['id']);
        $stmt->execute();
        $stmt->bind_result($fileHash,$shortId,$name,$width,$height,$filesize);
        $pages = [];
        $i = 0;
        while ($stmt->fetch()) {
            $link = BASE_HREF."/".IMAGE_DIR_URL."/$gid/$shortId.jpg";
            $thumb_link = BASE_HREF."/".THUMBNAIL_DIR_URL."/$gid/$shortId.jpg";
            $page = array(
                "fileHash"=>$fileHash,
                "link"=>$link,
                "thumb_link"=>$thumb_link,
                "shortId"=>$shortId,
                "name"=>$name,
                "width"=>$width,
                "height"=>$height,
                "filesize"=>$filesize
            );
            
            $pages[] = $page;
        
        }
        
        return $pages;
    } else {
        error_log("error in adding, getting pages, album pages");
    }
    
}






function getHTMLHeader($pageTitle){
    require_once 'pages/periph/navHeader.php';
    return '<!doctype html>
<html lang="en"><head>
<meta charset="utf-8">
<title>'.SITE_NAME.' - '.$pageTitle.'</title>
<link rel="stylesheet" type="text/css" href="'.BASE_HREF.'/css/g.css">
<meta name="robots" content="noarchive">
</head>
<body style="text-align:center">
<script type="text/javascript">
function popUp(URL,w,h) { 
	window.open(URL,"_pu"+(Math.random()+"").replace(/0\./,""),"toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width="+w+",height="+h+",left="+((screen.width-w)/2)+",top="+((screen.height-h)/2));
	return false;
}
</script>'.getHeader();
}
function getHTMLFooter($pages){
  $div = "";
  $div .=
    '<script type="text/javascript">
      var submitted = false;

      function submit_upload(act) {
        if (!submitted) {
          submitted = true;
          document.getElementById("ulmore").disabled = "disabled";
          document.getElementById("ulmore").value = "Uploading...";
          document.getElementById("uldone").disabled = "disabled";
          document.getElementById("uldone").value = "Uploading...";
          document.getElementById("ulact").value = act;
          document.getElementById("uploadform").submit();
        }
      }
    </script>';
  $div .= '<p class="ip">[<a href="'.BASE_HREF.'">Front Page</a>]</p>';
  $div .= '</body></html>';
  
  return $div;
    
}

/*
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <title>E-Hentai Galleries - The Free Hentai Doujinshi, Manga and Image Gallery System</title>
    <link rel="stylesheet" type="text/css" href="https://upload.e-hentai.org/z/0320/g.css">
    <meta name="description" content="With more than 200,000 absolutely free hentai doujinshi, manga, cosplay and CG galleries, E-Hentai Galleries is the world's largest free Hentai archive.">
    <meta name="RATING" content="RTA-5042-1996-1400-1577-RTA">
    <meta name="robots" content="noarchive">
    <meta name="juicyads-site-verification" content="0f3e47704e352bf534e98d4d45411fda">
    <link rel="alternate" type="application/atom+xml" title="E-Hentai Galleries Content Feed" href="http://g.e-hentai.org/rss/ehg.xml">
    <link rel="alternate" type="application/atom+xml" title="EHTracker Torrent Feed" href="http://g.e-hentai.org/rss/ehtracker.xml">
    <link rel="search" href="http://ehgt.org/g/opensearchdescription.xml" type="application/opensearchdescription+xml" title="E-Hentai Galleries Search">

    <style type="text/css">
        :root .itd[colspan="4"] {
            display: none !important;
        }
    </style>
</head>

<body style="text-align:center">
    <script type="text/javascript">
        function popUp(URL, w, h) {
            window.open(URL, "_pu" + (Math.random() + "").replace(/0\./, ""), "toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=" + w + ",height=" + h + ",left=" + ((screen.width - w) / 2) + ",top=" + ((screen.height - h) / 2));
            return false;
        }
    </script>
    <p id="nb"><img src="http://ehgt.org/g/mr.gif" alt=""> <a href="http://g.e-hentai.org/">Front Page</a><img src="http://ehgt.org/g/mr.gif" alt=""> <a href="http://g.e-hentai.org/torrents.php">Torrents</a><img src="http://ehgt.org/g/mr.gif" alt=""> <a href="http://g.e-hentai.org/favorites.php">Favorites</a><img src="http://ehgt.org/g/mr.gif" alt=""> <a href="http://g.e-hentai.org/home.php">My Home</a><img src="http://ehgt.org/g/mr.gif" alt=""> <a href="https://upload.e-hentai.org/manage.php">My Galleries</a><img src="http://ehgt.org/g/mr.gif" alt=""> <a href="http://g.e-hentai.org/toplist.php">Toplists</a><img src="http://ehgt.org/g/mr.gif" alt=""> <a href="http://g.e-hentai.org/bounty.php">Bounties</a><img src="http://ehgt.org/g/mr.gif" alt=""> <a href="http://e-hentai.org/">News</a><img src="http://ehgt.org/g/mr.gif" alt=""> <a href="https://forums.e-hentai.org/">Forums</a><img src="http://ehgt.org/g/mr.gif" alt=""> <a href="https://ehwiki.org/wiki/Category:E-Hentai_Galleries">Wiki</a><img src="http://ehgt.org/g/mr.gif" alt=""> <a href="http://hentaiverse.org/" onclick="popUp('http://hentaiverse.org/',1250,720); return false">HentaiVerse</a>
    </p>
    <!-- HEADER returns up to that part -->
    
    <div class="ui">
        <div class="stuffbox" style="width:980px; margin:10px auto 5px auto; padding:3px">
            <form id="uploadform" action="https://upload.e-hentai.org/manage.php?act=add&amp;gid=968305" method="post" enctype="multipart/form-data" class="nopm">
                <div style="text-align:right; float:left; width:480px; height:350px; padding-right:5px; position:relative; z-index:2; border-right:1px solid #5C0D12">
                    <span style="font-weight:bold">Upload new files to gallery: test</span>
                    <p class="nopm">0007
                        <input type="file" name="file01" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">
                    </p>
                    <p class="nopm">0008
                        <input type="file" name="file02" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">
                    </p>
                    <p class="nopm">0009
                        <input type="file" name="file03" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">
                    </p>
                    <p class="nopm">0010
                        <input type="file" name="file04" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">
                    </p>
                    <p class="nopm">0011
                        <input type="file" name="file05" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">
                    </p>
                    <p class="nopm">0012
                        <input type="file" name="file06" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">
                    </p>
                    <p class="nopm">0013
                        <input type="file" name="file07" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">
                    </p>
                    <p class="nopm">0014
                        <input type="file" name="file08" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">
                    </p>
                    <p class="nopm">0015
                        <input type="file" name="file09" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">
                    </p>
                    <p class="nopm">0016
                        <input type="file" name="file10" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">
                    </p>
                    <p class="nopm">0017
                        <input type="file" name="file11" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">
                    </p>
                    <p class="nopm">0018
                        <input type="file" name="file12" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">
                    </p>
                    <p class="nopm">0019
                        <input type="file" name="file13" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">
                    </p>
                    <p class="nopm">0020
                        <input type="file" name="file14" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">
                    </p>
                    <p class="nopm">0021
                        <input type="file" name="file15" size="60" style="font-size:8pt; height:19px; margin:1px; padding:0">
                    </p>
                </div>
                <div style="text-align:left; float:left; width:480px; height:350px; padding-left:5px; overflow:auto; position:relative; z-index:1">
                    <span style="font-weight:bold">Files uploaded so far</span>:
                    <br> 1477019077917.jpg, 2240x4000, 2.83 MB
                    <br>DSC_0256.jpg, 4000x6016, 5.99 MB
                    <br>1477019077917.jpg, 2240x4000, 2.83 MB
                    <br>Ice9HBhQ54eITE1EUC4iLo7y4PgBEx6Aq8uhJnTjOGs.jpg, 543x768, 113.7 KB
                    <br>L4_9OUS10Wh0h9bDq74sk176JLQVsb0Yg4hojtef9jI.jpg, 640x768, 79.58 KB
                    <br>VHoPzgOemEMsH6TkXRPkr6JRLIR5nRsonMfVUz7i1y4.jpg, 543x768, 121.1 KB
                    <br>
                </div>
                <div style="clear:both; width:980px; margin:auto; border-top:1px solid #5C0D12">
                    <table style="margin:auto">
                        <tbody>
                            <tr>
                                <td colspan="2" style="text-align:center">
                                    <input type="hidden" name="MAX_FILE_SIZE" value="1258291200" class="nopm">
                                    <input type="hidden" id="ulact" name="ulact" value="fillme" class="nopm">
                                    <span style="font-weight:bold">Accepted Image Formats: JPG, PNG, GIF. Accepted Archive Formats: RAR, ZIP. Max File Size: 10MB per Image. Max Resolution: 20000 x 20000.<br>
  Max 2,000 files per gallery. You should upload no more than 500MB at a time, less if you have a slow connection.</span>
                                    <br>
                                    <br> To upload the files listed above and return here to continue adding files, hit Upload and Add More.
                                    <br> To upload these files and continue to the next step, hit Upload and Continue.
                                    <br> If you need to remove or reorder files, you can do so at the next step.
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right">
                                    <input style="width:150px" type="button" id="ulmore" name="ulmore" onclick="submit_upload('ulmore'); return false" value="Upload and Add More" class="stdbtn">
                                </td>
                                <td style="text-align:left">
                                    <input style="width:150px" type="button" id="uldone" name="uldone" onclick="submit_upload('uldone'); return false" value="Upload and Continue" class="stdbtn">
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
                var submitted = false;

                function submit_upload(act) {
                    if (!submitted) {
                        submitted = true;

                        document.getElementById("ulmore").disabled = "disabled";
                        document.getElementById("ulmore").value = "Uploading...";
                        document.getElementById("uldone").disabled = "disabled";
                        document.getElementById("uldone").value = "Uploading...";
                        document.getElementById("ulact").value = act;
                        document.getElementById("uploadform").submit();
                    }
                }
            </script>
    <p class="ip">[<a href="http://g.e-hentai.org/">Front Page</a>]</p>


</body>

</html>
*/