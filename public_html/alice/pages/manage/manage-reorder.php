<?php
if(!defined('SITE_NAME')) {
   die('Direct access not permitted');
}

function manageReorderPage($mysqli){
    //$albums = getGalleriesForUser($_SESSION['user']['id'],$mysqli);
    $gid = !empty($_GET['gid']) ? $_GET['gid'] : 0;
    if($gid==0)return;
    if(isset($_POST["reorder_confirm"])){
      $retVal = getPOSTReponse($mysqli,$gid);
      
      if($retVal > 0){//error
        require 'manage-redirect.php';
        echoRedirectPage(SITE_NAME,
                      "The gallery has encountered an error. Try later or message an admin",
                       "You will be returned to the Management Interface momentarily",
                       "manage?act=preview&gid=$gid"
                      );
      } else {//we good
        require 'manage-redirect.php';
        echoRedirectPage(SITE_NAME,
                      "The gallery has been successfully reordered",
                       "You will be returned to the Management Interface momentarily",
                       "manage?act=preview&gid=$gid"
                      );
      }
      return;
    } else if(isset($_POST["reorder_cancel"])){
        require 'manage-redirect.php';
        echoRedirectPage(SITE_NAME,
                      "The gallery has been untouched",
                       "You will be returned to the Management Interface momentarily",
                       "manage?act=preview&gid=$gid"
                      );
        return;
    }
    getHTMLResponse($mysqli, $gid);
    
}

/*WE CHANGE DATABASE BETWEEN HERE AND THE OTHER COMMENT SECTION*/
function getPOSTReponse($mysqli,$gid){
  
  $totalPages = getNumTotalPages($mysqli,$gid);
  $pageOrderResult = checkPageOrder($mysqli,$gid,$totalPages);
  if($pageOrderResult > 0){
    return $pageOrderResult;
  }
  return updatePageOrder($mysqli,$gid,$totalPages);
  
}
function updatePageOrder($mysqli,$gid,$totalPages){
  $query = "UPDATE pages SET actualPageOrder=?,hidden=? WHERE gid=? AND page=?";
  if(!$stmt = $mysqli->prepare($query)){
    error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
    die;
  }
  $actualPageOrder = 0;
  $page = 0;
  $hidden = 0;
  if(!$stmt->bind_param('iiii',$actualPageOrder,$hidden,$gid,$page)){
    error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
    die;
  }
  $hasAtLeastOneVisiblePage = false;
  for($i=1;$i<=$totalPages;$i++){
    $page = $i;
    $actualPageOrder = intval($_POST["pagesel_$i"]);
    $hidden = $actualPageOrder == 0 ? 1 : 0;
    //if we have nonhidden page, set this for a bit later
    if(!$hidden)$hasAtLeastOneVisiblePage=true;
    if(!$stmt->execute()){
      error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
      die;
    }
  }
  
  $stmt->close();
  
  
  //if we have at least 1 page, we wanna set it to cover page.
  
  if($hasAtLeastOneVisiblePage){
    $res = updateCoverPage($mysqli,$gid);
      
    if($res==0){
      return 0;
    } else{
      return $res;
    }
  }
    

  return 0;
}
function updateCoverPage($mysqli,$gid){
  //UPDATE albums SET cover_thumb=(SELECT shortID FROM pages WHERE gid=40728330 AND actualPageOrder=1) WHERE gid=40728330
  $query = "UPDATE albums SET cover_thumb=(SELECT shortID FROM pages WHERE gid=? AND actualPageOrder=1) WHERE gid=?";
  if(!$stmt = $mysqli->prepare($query)){
    //throw new RuntimeException('Database error ucp1. Inform Admin');
    error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
    die;
  }
  if(!$stmt->bind_param('ii',$gid,$gid)){
    //throw new RuntimeException('Database error ucp2. Inform Admin');
    error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
    die;
  }
  if(!$stmt->execute()){
    //throw new RuntimeException('Database error ucp3. Inform Admin');
    error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
    die;
  }
  
  return 0;
}
function checkPageOrder($mysqli,$gid,$totalPages){
  /**
    return codes:
      0: Success
      1: Less pages that on record have been submitted
      2: Negative number detected
      3: Number larger than number of pages
      4: Duplicate wanted page number found
      5: Page skip detected
  */
  /*
  so the basic idea here is we don't know how many pages we're getting sent
  could be malicious so could be messing with us.
  first, get the actual page count from db.
  then check the post data to see if each one is set.
  if there's one missing, we'll present the other page again
  if they're all  set, let's update all of them and set the corresponding hidden pages.
  we're gonna change the actualpageorder only and the hidden field!
  */
  
  $elPrefix = "pagesel_";
  $hasAllPagesInPost = true;
  for($i=1;$i<=$totalPages;$i++){
    $hasAllPagesInPost = $hasAllPagesInPost 
      && isset($_POST[$elPrefix.$i]) 
      && is_numeric($_POST[$elPrefix.$i]);
    
    
  }
  if(!$hasAllPagesInPost)return 1;
  
  //now let's check that we go in order from 1->max in steps of 1
  /*
  if you have collection of integers, S
  only duplicates of value 0 are allowed, every other duplicate is invalid
  # of zeros + max(S) == S.length iff the above
  
  so, iterate over the whole thing, counting zeros, biggestnum, and throwing if invalid nums
    invalid if:
      duplicate found
      less than 0
      val > totalPages
  at the end, check the max and return 
  */
  $nums = [];
  $highestSeen = 0;
  $numZeros = 0;
  for($i=1;$i<=$totalPages;$i++){
    $num = intval($_POST[$elPrefix.$i]);
    
    if($num < 0)return 2;
    if($num > $totalPages)return 3;
    if(isset($nums[$num]))return 4;
    
    if($num==0){$numZeros++;}
    else {//not zero
      if($num > $highestSeen){
        $highestSeen = $num;//update
      }
      $nums[$num] = true;//set to seen it
    }
  }
  if($numZeros + $highestSeen != $totalPages)return 5;
  return 0;
  
}
function getNumTotalPages($mysqli,$gid){
  $query = "SELECT COUNT(id) as totalPages FROM pages where gid=?";
  if(!$stmt = $mysqli->prepare($query)){
    error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
    die;
  }
  if(!$stmt->bind_param('i',$gid)){
    error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
    die;
  }

  if(!$stmt->execute()){
    error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
    die;
  }
  if(!$stmt->bind_result($totalPages)){
    error_log(sprintf('errno: %d, error: %s', $mysqli->errno, $mysqli->error));
    die;
  }
  $stmt->fetch();
  $stmt->close();
  return $totalPages;
  
}




/*
MOST OF THE STUFF ABOVE DEALS WITH MODIFYING THE DATA ONCE WE GOT IT

EVERYTHING BELOW DOES THE FIRST PAGE VISIT STUFF, LIKE DISPLAYING ORDER
*/
function getHTMLResponse($mysqli, $gid){
    $pages = getAlbumPages($mysqli,$gid);
    echo getHTMLHeader("Reorder Gallery");
    echo getHTMLBody($pages,$gid);
    echo getHTMLFooter($pages);
}
function getHTMLBody($pages,$gid){
    $retVal = "";
    $retVal.= '<div class="ui" style="width:1230px">';
        $retVal.="<noscript><p>The reorderer relies on JavaScript for much of its functionality,and while it will work without it, it is recommended that you enable it.</p></noscript>";
        $retVal.='<div class="stuffbox" style="width:1110px; margin:10px auto; padding:3px;">';
            $retVal.='<h1 style="font-size:10pt; font-weight:bold; margin:3px; text-align:center">Reorder Gallery</h1>';
            $retVal.='<form action="'.BASE_HREF."/manage?act=reorder&gid=$gid\" method=\"post\">";
                $retVal.=getHTMLAlbumPages($pages);
                
                $retVal.='<div style="clear:both; margin-top:10px">';
                    $retVal.='<input name="return" type="hidden" value="preview">';
                    $retVal.='<input class="stdbtn" name="reorder_cancel" onclick="hide_dropbox()" type="submit" value="Cancel and Return">';
                    $retVal.='<input class="stdbtn" name="reorder_confirm" onclick="hide_dropbox()" type="submit" value="Confirm Changes">';
                $retVal.="</div>";
    
            $retVal.="</form>";
    
    
    $retVal.='</div>';
    return $retVal;
    
}

function getAlbumPages($mysqli,$gid){
    $query = "SELECT hash,shortId,p.name,page,thumb_width,thumb_height,actualPageOrder,p.hidden FROM pages p, albums a  WHERE p.gid=? AND a.uploaderId=? AND a.gid=p.gid";
    if ($stmt = $mysqli->prepare($query)) {
        $stmt->bind_param("ii",$gid,$_SESSION['user']['id']);
        $stmt->execute();
        $stmt->bind_result($fileHash,$shortId,$name,$pageNum,$thumb_width,$thumb_height,$actualPageOrder,$hidden);
        $pages = [];
        $i = 0;
        while ($stmt->fetch()) {
            $link = BASE_HREF."/".IMAGE_DIR_URL."/$gid/$shortId.jpg";
            $thumb_link = BASE_HREF."/".THUMBNAIL_DIR_URL."/$gid/$shortId.jpg";
            $page = array(
                "fileHash"=>$fileHash,
                "link"=>$link,
                "thumb_link"=>$thumb_link,
                "shortId"=>$shortId,
                "name"=>$name,
                "page"=>$pageNum,
                "thumb_width"=>$thumb_width,
                "thumb_height"=>$thumb_height,
                "actualPageOrder"=>$actualPageOrder,
                "deletePos"=>-1,
            );
            if($hidden==1){
                $page['deletePos'] = $i++;
            }
            $pages[] = $page;
        
        }
        
        return $pages;
    } else {
        error_log("error in reordering album pages");
    }
    
}

function getHTMLAlbumPages($pages){
    $height = 355 *  ceil( count($pages) / 5 );
    $retVal = '<div style="position:relative; height:'.$height.'px">';
    $numPages = count($pages);
    for($i=0;$i<$numPages;$i++){
        $retVal .= getHTMLAlbumPage($pages[$i],$numPages);
    }
    /*for($i=0;$i<count($pages['hidden']);$i++){
        $retVal .= getHTMLAlbumPage($pages['hidden'][$i],$total,$i);
    }*/
    
    $retVal.='</div>';
    return $retVal;
}

function getHTMLAlbumPage($page,$total){
    $x = 0;$y = 0;
    list($x,$y) = getXYforPage($page['actualPageOrder'],$total,$page['deletePos']);
    $n = $page['page'];
    $div = '<div id="cell_'.$n.'" style="position:absolute; left:'.$x.'px; top:'.$y.'px; width:220px; height:350px; margin:1px; word-wrap:break-word; overflow:hidden">';
    $val = $page['deletePos'] == -1 ? $page['actualPageOrder'] : $page['deletePos'];
    $div.='Pg: <span id="inspan_'.$n.'"><input id="pagesel_'.$n.'" name="pagesel_'.$n.'" type="text" value="'.$val.'" class="stdinput" style="width:58px; height:14px; padding-top:3px; padding-left:7px" onmouseover="show_dropbox('.$n.')" onfocus="show_dropbox('.$n.')" /></span>';
    $div.='<a style="text-decoration:none" href="'.$page['link'].'"   target="_new"><img id="thumb'.$n.'" class="th" alt="'.$page['name'].'" title="'.$page['name'].'"     src="'.$page['thumb_link'].'" />
        <br />
        </a>';

    $div.=$page['name'];
    $div.='</div>';
    return $div;
    /*
    <div id="cell_1" style="position:absolute; left:0px; top:0px; width:220px; height:350px; margin:1px; word-wrap:break-word; overflow:hidden">
Pg: <span id="inspan_1"><input id="pagesel_1" name="pagesel_1" type="text" value="1" class="stdinput" style="width:58px; height:14px; padding-top:3px; padding-left:7px" onmouseover="show_dropbox(1)" onfocus="show_dropbox(1)" /></span>

<a style="text-decoration:none" href="http://g.e-hentai.org/s/665d99b87f8dff89c0c795b33f75316901af1727-6277119-4000-6016-jpg/968305-1" target="_new"><img id="thumb1" class="th" alt="DSC_0256.jpg" title="DSC_0256.jpg" src="http://ul.ehgt.org/66/5d/665d99b87f8dff89c0c795b33f75316901af1727-6277119-4000-6016-jpg_l.jpg" />
    <br />
</a>
DSC_0256.jpg
</div>
    
    */
}

function getXYforPage($num,$total,$deletePos){
    /*
    function recompute_divpos(c) {
    if (nofancy) {
        return
    }
    var b = document.getElementById("cell_" + c);
    var d = parseInt(document.getElementById("pagesel_" + c).value);
    var a = 0;
    if (d == 0) {
        d = (Math.ceil(pages / 5) * 5) + 1 + delete_pos[c];
        a = 70
    }
    b.style.left = 222 * ((d - 1) % 5) + "px";
    b.style.top = (355 * Math.floor((d - 1) / 5) + a) + "px"
    }
    
    */
    $a = 0;
    $d = $num;
    if(isset($deletePos) && $deletePos > -1){
        $d = (ceil($total / 5) * 5) + 1 + $deletePos;
        $a = 70;
    }
    
    $x = 0;
    $y = 0;
    
    $x = 222 * (($d - 1) % 5);
    $y = (355 * floor(($d - 1) / 5) + $a);
    
    return(array($x,$y));
    
}

function getHTMLHeader($pageTitle){
    require_once 'pages/periph/navHeader.php';
    return '<!doctype html>
<html lang="en"><head>
<meta charset="utf-8">
<title>'.SITE_NAME.' - '.$pageTitle.'</title>
<link rel="stylesheet" type="text/css" href="'.BASE_HREF.'/css/g.css">
<meta name="robots" content="noarchive">
</head>
<body style="text-align:center">
<script type="text/javascript">
function popUp(URL,w,h) { 
	window.open(URL,"_pu"+(Math.random()+"").replace(/0\./,""),"toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width="+w+",height="+h+",left="+((screen.width-w)/2)+",top="+((screen.height-h)/2));
	return false;
}
</script>'.getHeader();
}
function getHTMLFooter($pages){
    $total = count($pages);
    $delete_count = 0;
    $delete_pos = "[undefined";
    $delete_pos_sep = ", ";
    for($i=0;$i<$total;$i++){
        if($pages[$i]['deletePos'] > -1){
            $delete_pos .= $delete_pos_sep . $pages[$i]['deletePos'];
            $delete_count++;
            
        } else {
            $delete_pos .= $delete_pos_sep . "undefined";
        }
    }
    $delete_pos .= "]";
    $div = "";
    $div .= "<script type='text/javascript'>
        var pages=$total;
        var nofancy = false;
        var delete_pos=$delete_pos;
        var delete_count=$delete_count;
    </script>";
    $div .= 
        '<script type="text/javascript" src="'.BASE_HREF.'/js/gallery.js"></script>
         <script type="text/javascript" src="'.BASE_HREF.'/js/manage.js"></script>
        </div>
        <p class="ip">[<a href="'.BASE_HREF.'">Front Page</a>]</p>
        </body></html>';
    return $div;
}




/*
host self manage.js and change delete vars to be dynamic.


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>E-Hentai Galleries - The Free Hentai Doujinshi, Manga and Image Gallery System</title>
<link rel="stylesheet" type="text/css" href="https://upload.e-hentai.org/z/0320/g.css" />
<meta name="description" content="With more than 200,000 absolutely free hentai doujinshi, manga, cosplay and CG galleries, E-Hentai Galleries is the world's largest free Hentai archive." />
<meta name="RATING" content="RTA-5042-1996-1400-1577-RTA" />
<meta name="robots" content="noarchive" />
<meta name="juicyads-site-verification" content="0f3e47704e352bf534e98d4d45411fda" />
<link rel="alternate" type="application/atom+xml" title="E-Hentai Galleries Content Feed" href="http://g.e-hentai.org/rss/ehg.xml" />
<link rel="alternate" type="application/atom+xml" title="EHTracker Torrent Feed" href="http://g.e-hentai.org/rss/ehtracker.xml" />
<link rel="search" href="http://ehgt.org/g/opensearchdescription.xml" type="application/opensearchdescription+xml" title="E-Hentai Galleries Search" />

</head>
<body style="text-align:center">
<script type="text/javascript">
function popUp(URL,w,h) { 
	window.open(URL,"_pu"+(Math.random()+"").replace(/0\./,""),"toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width="+w+",height="+h+",left="+((screen.width-w)/2)+",top="+((screen.height-h)/2));
	return false;
}
</script>
<p id="nb"><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://g.e-hentai.org/">Front Page</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://g.e-hentai.org/torrents.php">Torrents</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://g.e-hentai.org/favorites.php">Favorites</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://g.e-hentai.org/home.php">My Home</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="https://upload.e-hentai.org/manage.php">My Galleries</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://g.e-hentai.org/toplist.php">Toplists</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://g.e-hentai.org/bounty.php">Bounties</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://e-hentai.org/">News</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://forums.e-hentai.org/">Forums</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://ehwiki.org/wiki/Category:E-Hentai_Galleries">Wiki</a><img src="http://ehgt.org/g/mr.gif" alt="" /> <a href="http://hentaiverse.org/" onclick="popUp('http://hentaiverse.org/',1250,720); return false">HentaiVerse</a></p><div class="ui" style="width:1230px">
<noscript><p>The reorderer relies on JavaScript for much of its functionality, and while it will work without it, it is recommended that you enable it.</p></noscript>
<div class="stuffbox" style="width:1110px; margin:10px auto; padding:3px">
<h1 style="font-size:10pt; font-weight:bold; margin:3px; text-align:center">Reorder Gallery</h1>
<form action="https://upload.e-hentai.org/manage.php?act=reorder&amp;gid=968305" method="post">
<div style="position:relative; height:355px">

<div id="cell_1" style="position:absolute; left:0px; top:0px; width:220px; height:350px; margin:1px; word-wrap:break-word; overflow:hidden">
Pg: <span id="inspan_1"><input id="pagesel_1" name="pagesel_1" type="text" value="1" class="stdinput" style="width:58px; height:14px; padding-top:3px; padding-left:7px" onmouseover="show_dropbox(1)" onfocus="show_dropbox(1)" /></span>
<a style="text-decoration:none" href="http://g.e-hentai.org/s/665d99b87f8dff89c0c795b33f75316901af1727-6277119-4000-6016-jpg/968305-1" target="_new"><img id="thumb1" class="th" alt="DSC_0256.jpg" title="DSC_0256.jpg" src="http://ul.ehgt.org/66/5d/665d99b87f8dff89c0c795b33f75316901af1727-6277119-4000-6016-jpg_l.jpg" /><br /></a>
DSC_0256.jpg
</div>

</div>
<div style="clear:both; margin-top:10px">
	<input type="hidden" name="return" value="preview" />
	<input type="submit" name="reorder_cancel" value="Cancel and Return" class="stdbtn" onclick="hide_dropbox()" />
	<input type="submit" name="reorder_confirm" value="Confirm Changes" class="stdbtn" onclick="hide_dropbox()" />
</div>
</form>
<script type="text/javascript"><!--
var pages = 1;
var nofancy = false;
--></script>
<script type="text/javascript" src="https://upload.e-hentai.org/z/0320/ehg_gallery.c.js"></script>
<script type="text/javascript" src="https://upload.e-hentai.org/z/0320/ehg_manage.c.js"></script>
</div>
</div><p class="ip">[<a href="http://g.e-hentai.org/">Front Page</a>]</p>
</body>
</html>


*/