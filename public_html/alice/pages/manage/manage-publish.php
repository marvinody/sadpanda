<?php
if(!defined('SITE_NAME')) {
   die('Direct access not permitted');
}

function managePublishPage($mysqli){
    //$albums = getGalleriesForUser($_SESSION['user']['id'],$mysqli);
    $gid = !empty($_GET['gid']) ? $_GET['gid'] : 0;
    if($gid==0)return;
    if(isset($_GET["act"])){
      
        $res = getPOSTResponse($mysqli,$gid);
     
        require 'manage-redirect.php';
        echoRedirectPage(SITE_NAME,
                      "The gallery has been successfully $res.",
                       "You will be returned to the Management Interface momentarily",
                       "manage"
                      );
      
        return;
    }//should never happen cause act should always be set
    
    
}

function getPOSTResponse($mysqli,$gid){
    $action = 0;
    $res = "unpublished";
    if($_GET['act']=='publish'){
        $action = 1;
        $res = "published";
    }
    $query = 'UPDATE albums SET published=? WHERE gid=?';
    
    if(!$stmt = $mysqli->prepare($query)){
        error_log(sprintf('gpr1: errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        die;
    }
    if(!$stmt->bind_param('ii',$action,$gid)){
        error_log(sprintf('gpr2: errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        die;
    }
    if(!$stmt->execute()){
        error_log(sprintf('gpr3: errno: %d, error: %s', $mysqli->errno, $mysqli->error));
        die;    
    }
    return $res;
    
    
    
}




