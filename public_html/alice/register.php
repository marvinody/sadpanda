<?php
require_once 'core/config.php';
session_start();
$submitted = false;
if (isset($_POST) && ( 
    isset($_POST['UserName']) 
  ||isset($_POST['PassWord']) 
  ||isset($_POST['EMail']) 
  ||isset($_POST['captcha'])
    )
){
    $submitted = true;
    require_once 'core/connect.php';
    $errors = [];
    $errors[] = checkUserName($_POST['UserName'],$mysqli);
    $errors[] = checkPassWord($_POST['PassWord'],$_POST['PassWord2']);
    $errors[] = checkEmail($_POST['EMail']);
    $errors[] = checkCaptcha($_POST['captcha']);
    
    
    //returning "null" in the functions adds a bunch of blank stuff in arr
    //so let's get rid of it
    $errors = array_values(array_filter($errors));
    if(count($errors)>0){
        //do nothing
        /*
        echo "<pre>";
        print_r($errors);
        echo "</pre>";
        */
    } else {
        //no errors detected
        $code = sha1(uniqid(rand(),true));
        $ip = $_SERVER['REMOTE_ADDR'];
        $options = array('cost' => 11);
        $b = createUser(
            $mysqli,
            $_POST['UserName'],
            password_hash($_POST['PassWord'], PASSWORD_BCRYPT, $options),
            $_POST['EMail'],
            $code,
            $ip
        );
        if ($b!=false){//error
            error_log("REGISTER ERROR:".$b);
        } else {
            require_once 'core/loadUserPrefs.php';
            savePrefs($mysqli->insert_id,$mysqli);
            echo "SUCCESS!";
            die;
            sendEmail($_POST['EMail'],$code,$_POST['UserName']);
        }

    }
}
function createUser($mysqli,$user,$pass,$email,$verif,$ip){
    $stmt=$mysqli->prepare("INSERT INTO users(user,email,passHash,ip,activationCode) VALUES (?,?,?,?,?)");
    if(!($stmt->bind_param('sssss',$user,$email,$pass,$ip,$verif))){
        return (htmlspecialchars($stmt->error));
    }
    if (!($stmt->execute() )){
        return (htmlspecialchars($stmt->error));
    }
    $stmt->close();
    return null;
}

function sendEmail($email,$code,$user){
    $to      = $email;
    $subject = SITE_NAME.' Verification';
    $message = 'Hello '.$user.':\n\n';
    $message.= 'Your ';
    $headers = 'From: no-reply@sadpanda.moe' . "\r\n" .
        'Reply-To: no-reply@sadpanda.moe' . "\r\n";

    mail($to, $subject, $message, $headers);
    
}
function checkCaptcha($captcha){
    if(empty($captcha)){return "Captcha cannot be empty";}
    if($_SESSION['captcha']!=$captcha){return "Captcha is not correct. Please try again";}
    return null;
}
function checkEmail($email){
    if(empty($email)){return "Email cannot be empty";}
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){return "Email seems incorrect";}
    return null;
}
function checkPassWord($pass1,$pass2){
    if(empty($pass1) || empty($pass2)){return "Password cannot be empty";}
    $pass1 = (string)$pass1;
    $pass2 = (string)$pass2;
    if(strlen($pass1)<8){return "Password must be a minimum of 8 characters";}
    if(strcmp($pass1,$pass2)!==0){return "Passwords are not equal";}
    return null;
}
function checkUserName($user,$mysqli){
    if(empty($user)){return "Username cannot be empty";}
    if(strlen($user)>64){return "Username can only be max 64 characters";}
    if(strlen($user)<4){return "Username must be a minimum of 4 characters";}
    //if(is_string($user)){return "Username must be a letters only";}
    if(preg_match('/[a-zA-Z]{4,64}/i',$user)===0){return "Username can only consist of letters [NO NUMBERS]";}
    if (checkForExistingUserName($user,$mysqli)>0){return "Username is already taken";}
    return null;
}
function checkForExistingUserName($user,$mysqli){
    $query = "SELECT id FROM `users` WHERE user=?";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param('s',$user);
    $stmt->bind_result($hasUser);
    $stmt->execute();
    $stmt->fetch();
    return $hasUser;
}

function silly_captcha(){
    $class = 'captcha-text';
    $numbers = array(
        '1'=>['&#49;','&#x2460;','&#x2474;','&#x2488;','&#x24F5;'],
        '2'=>['&#50;','&#x2461;','&#x2475;','&#x2489;','&#x24F6;'],
        '3'=>['&#51;','&#x2462;','&#x2476;','&#x248A;','&#x24F7;'],
        '4'=>['&#52;','&#x2463;','&#x2477;','&#x248B;','&#x24F8;'],
        '5'=>['&#53;','&#x2464;','&#x2478;','&#x248C;','&#x24F9;'],
        '6'=>['&#54;','&#x2465;','&#x2479;','&#x248D;','&#x24FA;'],
        '7'=>['&#55;','&#x2466;','&#x247A;','&#x248E;','&#x24FB;'],
        '8'=>['&#56;','&#x2467;','&#x247B;','&#x248F;','&#x24FC;'],
        '9'=>['&#57;','&#x2468;','&#x247C;','&#x2490;','&#x24FD;'],
    );
    $num1 = mt_rand(1,9);
    $num1CharInd = mt_rand(0,count($numbers[$num1])-1);
    $num1Char = $numbers[$num1][$num1CharInd];
    $num1Span = "<span class='".(($num1CharInd==1)?$class:"")."'>".$num1Char."</span>";
    $num2 = mt_rand(1,9);
    $num2CharInd = mt_rand(0,count($numbers[$num2])-1);
    $num2Char = $numbers[$num2][$num2CharInd];
    $num2Span = "<span class='".(($num2CharInd==1)?$class:"")."'>".$num2Char."</span>";
    $operation = mt_rand(0,2);
    $str = "nothing";
    switch($operation){
        case 0:
            $str = $num1Span." + ".$num2Span;
            $result = $num1+$num2;
            break;
        case 1:
            $str = $num1Span." - ".$num2Span;
            $result = $num1-$num2;
            break;
        case 2:
            $str = $num1Span." * ".$num2Span;
            $result = $num1*$num2;
            break;
    }
    return array(
        'result'=>$result,
        'str'=>$str,
        'style'=>'<style>span.captcha-text{font-size:1.5em;}</style>',
    );
    
}
$res = silly_captcha();
$_SESSION['captcha'] = $res['result'];
?>
<!DOCTYPE html
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title><?php echo BASE_HREF; ?> Register</title>
	<link rel="stylesheet" type="text/css" href="<?php echo BASE_HREF; ?>/css/g.css" />
	<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex,nofollow" />
    <style>
        body {
            padding:30px;
            font:normal 12px/1.5 Arial, sans-serif;
        }

        /* Hover tooltips */
        .field-tip {
            position:relative;
            cursor:help;
        }
        .field-tip:before{
            content: "\FFFD";
        }
        .field-tip .tip-content {
            position:absolute;
            top:-10px; /* - top padding */
            right:9999px;
            width:200px;
            margin-right:-220px; /* width + left/right padding */
            padding:10px;
            
            background: #E3E0D1;
            color: #5C0D11;
            border: 2px solid #5C0D11;
            -webkit-box-shadow:2px 2px 5px #aaa;
               -moz-box-shadow:2px 2px 5px #aaa;
                    box-shadow:2px 2px 5px #aaa;
            opacity:0;
            -webkit-transition:opacity 250ms ease-out;
               -moz-transition:opacity 250ms ease-out;
                -ms-transition:opacity 250ms ease-out;
                 -o-transition:opacity 250ms ease-out;
                    transition:opacity 250ms ease-out;
        }
        .field-tip:hover .tip-content {
            right:-20px;
            opacity:1;
        }
        .tip-content.user-tip{
            top:-53px;
        }
        .tip-content.pass1-tip{
            top: -43px;
            width: 211px;
        }
        .field-tip:hover .tip-content.pass1-tip{
            right: -31px;
        }
        .tip-content.email-tip{
            top: -44px;
        }
        ul{
            list-style-type:circle;
        }
        /* <http://css-tricks.com/snippets/css/css-triangle/> */
        .field-tip .tip-content:before {
            content:' '; /* Must have content to display */
            position:absolute;
            top:50%;
            left:-16px; /* 2 x border width */
            width:0;
            height:0;
            margin-top:-8px; /* - border width */
            border:8px solid transparent;
            border-right-color:#5C0D11;
        }
        
    </style>
    <?php echo $res['style'];?>
</head>

<body style="text-align:center">
	<div class="d">
		<p>Please enter your info to register.</p>
        <?php
            if($submitted){
                echo "<p>Some errors found in your form</p>";
                echo "<p>Please fix the following:</p>";
                for($i=0;$i<count($errors);$i++){
                    echo "<p style='margin-left:10px'>$errors[$i]</p>";
                }
            }
        ?>
		<form action="<?php echo BASE_HREF; ?>/register" method="post" name="register-form">
			<!--<input type="hidden" name="CookieDate" value="1" />-->
			<!--<input type="hidden" name="b" value="d" />-->
			<table style="margin:auto; text-align:left">
				<tr>
                    <td>User:</td>
                    <td>
                        <input class="stdinput" type="text" name="UserName" size="12" maxlength="64" value="<?php echo htmlentities($_POST['UserName']); ?>" style="width:160px" />
                        <span class="field-tip user-tip">
                            <span class="tip-content user-tip">
                                <ul>
                                    <li>Must be unique</li>
                                    <li>Minimum of 8 Characters</li>
                                    <li>Maximum of 64</li>
                                    <li>Only A-Z (or a-z)</li>
                                </ul>
                            </span>
                        </span>    
                    </td>
                </tr>
				<tr>
                    <td>Pass:</td>
                    <td>
                        <input class="stdinput" type="password" name="PassWord" size="12" maxlength="256" value="<?php echo htmlentities($_POST['PassWord']); ?>" style="width:160px" />
                        <span class="field-tip pass1-tip">
                            <span class="tip-content pass1-tip">
                                <ul>
                                    <li>Minimum of 8 Characters</li>
                                    <li>Maximum of whatever you want</li>
                                    <li>Must match field below</li>
                                </ul>
                            </span>
                        </span> 
                    </td>
                </tr>
				<tr>
                    <td>Confirm Pass:</td>
                    <td><input class="stdinput" type="password" name="PassWord2" size="12" maxlength="256" value="<?php echo htmlentities($_POST['PassWord2']); ?>" style="width:160px" />
                    
                    </td>
                </tr>
				<tr>
                    <td>Email:</td>
                    <td>
                        <input class="stdinput" type="text" name="EMail" size="12" maxlength="64" value="<?php echo htmlentities($_POST['EMail']); ?>" style="width:160px" />
                        <span class="field-tip email-tip">
                            <span class="tip-content email-tip">
                                <ul>
                                    <li>Don't actually need real email</li>
                                    <li>unless you want to upload</li>
                                    <li>Put garbage if you don't care</li>
                                </ul>
                            </span>
                        </span>
                    </td>
                </tr>
                <tr><td>Enter Captcha:</td><td><input class="stdinput" type="text" name="captcha" size="2" maxlength="2" value="" style="width:80px" /> <?php echo $res['str'] ?></td></tr>
                <tr><td colspan="2" style="padding-top:5px;text-align:center;"></td></tr>
				<tr><td colspan="2" style="padding-top:5px; text-align:center; vertical-align:middle"><input class="stdbtn" style="width:60px" type="submit" name="ipb_login_submit" value="Register!" /></td></tr>
			</table>
		</form>
	</div>
</body>
</html>