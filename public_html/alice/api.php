<?php
$data = json_decode(file_get_contents('php://input'), true);
if(empty($data)){
    //check post data to see if anything?
    if(!empty($_POST['method'])){
        $data = $_POST; 
    } else {
        die(json_encode(array('error'=>"No data sent")));
    }
    
}
if(!isset($data['method']) || empty($data['method'])){
    die(json_encode(array('error'=>"No method sent")));
}
require_once 'core/config.php';
switch($data['method']){
    case "imagedispatch":
        imagedispatch($data);
        break;
    case "votecomment":
        votecomment($data);
        break;
    case "showpage":
        showpage($data);
        break;
    case "login":
        loginUser($data);
        break;
    case "rategallery":
        apirategallery($data);
        break;
    case "taggallery":
        apitaggallery($data);
        break;
    case "creategallery":
        apicreategallery($data);
        break;
	   default:
		      defaultResponse($data);
}
function apitaggallery($data){
    require_once 'core/connect.php';
    require_once 'core/authenticate.php';
    if( !(isset($_SESSION['gallery']) && isset($_SESSION['gallery']['gid']) && $data['gid']==$_SESSION['gallery']['gid']) ){
        die(json_encode(array(
            'error'=>"Authentication error. Please relog and go back to the gallery",
        )));
    }
    if(authenticate($mysqli,true)){
        require_once 'features/taggallery.php';
        $res = taggallery($mysqli,$data['gid'],$data['tags'],$data['vote']);
        sendJSONResponse($res);
    } else {
        sendJSONResponse(array('error'=>"Authentication error. Please relog"));
    }
    
}
function loginUser($data){
    if(empty($data['UserName']) || empty($data['PassWord'])){die(json_encode(array('error'=>"Need both username and password to login")));}
    require_once 'core/connect.php';
    require_once 'core/authenticate.php';
    $res = login($mysqli,$data['UserName'],$data['PassWord']);
    if(isset($res['error'])){
        die(json_encode(array('error'=>"Could not verify with the supplied username/password")));
    } else {
        loadPrefs($res['id'],$mysqli);
        loadUserVote($mysqli);
        
        loadPageSession();
        setcookie("session", $_SESSION['user']['sessionId'], time()+3600);
    }
    
}

function showpage($data){
    //this code is in spv as well because I'm silly
    //TODO grab this and the other and refactor them
    require_once 'core/connect.php';
    require_once 'core/galleryInfo.php';
    $gallery = getGallery($data['gid'],$mysqli);
    $page = max(min($data['page'],count($gallery['pages'])), 1);
    $total = count($gallery['pages']);
    $d = array('i'=>'<div>'.$gallery['pages'][$page-1]['name'].' :: '.$gallery['pages'][$page-1]['width'].' x '.$gallery['pages'][$page-1]['height'].' :: '.$gallery['pages'][$page-1]['filesize'].' KB</div>');
    $d['i3'] = '<a onclick="return load_image('.min($page+1,$total).', \''.$gallery['gid'].'\')" href="'.$gallery['pages'][$page]['f_link'].'"><img id="img" src="'.$gallery['pages'][$page-1]['f_link'].'"style="height:'.$gallery['pages'][$page-1]['height'].'px;width:'.$gallery['pages'][$page-1]['width'].'px" /></a>';
    $d['i5'] = '<div class="sb"><a href="'.$gallery['link'].'"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/b.png" referrerpolicy="no-referrer" /></a></div>';
    $d['i6'] = '&nbsp; <img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" class="mr" /> <a href="#implementMD5SearchHere">Show all galleries with this file</a>&nbsp; <img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" class="mr" /> <a href="#" id="loadfail" onclick="return nl(\''.$gallery['gid'].','.$page.'\')">Click here if the image fails loading</a>';
    $d['i7'] = '';
    if($page['hasRaw']){
        $d['i7'] = '&nbsp; <img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/mr.gif" class="mr" /> <a href="'.BASE_HREF.'/'.LARGE_IMAGE_DIR_URL.'/'.$gallery['gid'].'/'.$page.'">Download original source image</a>';
    }
    
    $d['n']  = '<div class="sn"><a onclick="return load_image(1, \''.$gallery['gid'].'\')" href="'.$gallery['singlePageViewer'].'1"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/f.png" /></a><a id="prev" onclick="return load_image('.max($page-1,1).', \''.$gallery['gid'].'\')" href="'.$gallery['singlePageViewer'].max($page-1,1).'"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/p.png" /></a><div><span>'.$page.'</span> / <span>'.$total.'</span></div><a id="next" onclick="return load_image('.min($page+1,$total).', \''.$gallery['gid'].'\')" href="'.$gallery['singlePageViewer'].min($page+1,$total).'"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/n.png" /></a><a onclick="return load_image('.$total.', \''.$gallery['gid'].'\')" href="'.$gallery['singlePageViewer'].$total.'"><img src="'.BASE_HREF.'/'.SMALL_ICONS_DIR.'/l.png" /></a></div>';
    $d['p'] = $page;
    $d['s'] = 's/'.$gallery['gid'].'/'.$page;

    $d['x'] = $gallery['pages'][$page-1]['width'];
    $d['y'] = $gallery['pages'][$page-1]['height'];
    echo json_encode($d);
}

function defaultResponse($data){
	header('Content-Type: application/json');
    //print_r($data);
	echo json_encode(array("error"=>"This method is not supported"));
}

function votecomment($data){
/*
received data
apikey:"8f566e3bf225c41d8c67" <- no fking clue
apiuid:1637569 <- user id, random gen?
comment_id:1250995 <- comment id, random gen?
comment_vote:1 <- 1=upvote,0=no vote, -1 = downvote
gid:917148 <-gid for gallery
method:"votecomment" <- api call method
token:"294e5a30f5" <- generated on login? time basis?


sent data
{"comment_id":1250995,"comment_score":14,"comment_vote":1}
*/
    defaultResponse($data);
}

function apirategallery($data){
    /*
    received data
    apikey:"341ba9d027ae772a1d4c"
    apiuid:-1
    gid:40732230
    method:"rategallery"
    rating:10
    token:"294e5a30f5"
    
    1<=r<=4
    5<=g<=8
    9<=b<=10
    
    return 
    {"rating_avg":4.67,
    "rating_usr":5,
    "rating_cnt":334,
    "rating_cls":"ir irb"}
    */
    require_once 'core/connect.php';
    require_once 'core/authenticate.php';
    if(authenticate($mysqli,true)){
        require_once 'features/rategallery.php';
        $res = rategallery($data['gid'],$mysqli,$data['rating']);
        sendJSONResponse($res);
    } else {
        sendJSONResponse(array('error'=>"Authentication error. Please relog"));
    }
    
}

function apicreategallery($data){
    require_once 'core/connect.php';
    require_once 'core/authenticate.php';
    if(authenticate($mysqli,true)){
        
        function getCategories($mysqli){
            #TODO move this caching somewhere else?
            if(isset($_SESSION['g_categories']))
                return $_SESSION['g_categories'];
            $query = "SELECT id,category FROM `categories` WHERE id<>1 ORDER BY priority DESC";

            if(!$stmt = $mysqli->prepare($query)){
                throw new RuntimeException('Database error mngc1. Inform Admin');
            }
            if(!$stmt->bind_result($id,$category)){
                throw new RuntimeException('Database error mngc3. Inform Admin');
            }
            if(!$stmt->execute()){
                throw new RuntimeException('Database error mngc4. Inform Admin');
            }

            $categories = [];
            while($stmt->fetch()){
                $categories[] = array(
                    'id'=>$id,
                    'category'=>$category
                );
            }
            $_SESSION['g_categories'] = $categories;
            return $categories;
        }

        
        require_once 'features/creategallery.php';
        $gname = !empty($data['gname']) ? $data['gname'] : null;
        $gname_jpn = !empty($data['gname_jpn']) ? $data['gname_jpn'] : null;
        $public = !empty($data['public']) ? $data['public'] : null;
        $publiccat = !empty($data['publiccat']) ? $data['publiccat'] : null;
        $comment = !empty($data['comment']) ? $data['comment'] : null;
        $tos = !empty($data['tos']) ? $data['tos'] : null;
        list($errors,$gid) = creategallery(
            $mysqli,
            getCategories($mysqli),
            $gname,
            $gname_jpn,
            $public,
            $publiccat,
            $comment,
            $tos
        );
        if(count($errors) == 0){
            sendJSONResponse(array('gid'=>$gid));
            
        } else {
            sendJSONResponse(array('error'=>$errors));
        }
        
    } else {
        sendJSONResponse(array('error'=>"Authentication error. Please relog"));
    }
}

function sendJSONResponse($arr){
    header('Content-Type: application/json');
    echo json_encode($arr);
}
function getImagePath($gid,$imgkey){
		return "c/".$gid."/".$imgkey.".jpg";
		
}


function get_raw_image_data($mysqli,$pageId){
    $query = "SELECT width, height, filesize FROM pageHashes WHERE pageId=? AND isRaw=1";
    $stmt = $mysqli->prepare($query);
    $stmt->bind_param('i',$pageId);
    $stmt->bind_result($width,$height,$filesize);
    $stmt->execute();
    $stmt->fetch();
    $stmt->close();
    return array(
        'width'=>$width,
        'height'=>$height,
        'filesize'=>$filesize
    );
}


function imagedispatch($data){
    require_once 'core/connect.php';
    require_once 'core/galleryInfo.php';
    $gallery = getGallery($data['gid'],$mysqli);
    
	$data['i'] = BASE_HREF."/".getImagePath($data['gid'],$data['imgkey']);
	
    $page = $gallery['pages'][$data['page']-1];
    if($page['hasRaw']){
        $data['lf'] = sprintf("%s/%s/%s",LARGE_IMAGE_DIR_URL,$data['gid'],$page['shortID']);
        $raw_data = get_raw_image_data($mysqli, $page['id']);
        $data['o'] = sprintf('Download original %s x %s %s KB source',
                             $raw_data['width'], $raw_data['height'],$raw_data['filesize']);
    } else {
        $data['lf'] = sprintf("%s/%s/%s",IMAGE_DIR_URL,$data['gid'],$page['shortID']);
        $data['o'] = "org";
    }
	$data['size'] = $page['filesize'];
	$data['d'] = $data['xres']." x ".$data['yres']." :: ".$data['size']." KB";
    $data['lo'] = "s/".$data['gid']."/".$data['page'];
	/*
	need to return:
		height x width :: size KB
		full image url
		xres
		yres
		
	*/
	/*
	received data:
	key (hash of file?) shortid
	thumbnail
		method: "imagedispatch",
		gid: gid, 
		page: b, {page useful for?}
		imgkey: imagelist[b - 1].k, {hash?}
		mpvkey: mpvkey, {multipage viewer key?}
		nl: imagelist[b - 1].s
	*/


	header('Content-Type: application/json');
	echo json_encode($data);
	
	
}



