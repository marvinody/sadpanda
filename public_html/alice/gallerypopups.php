<?php
if(isset($_GET)){
    //we have a get request
    if(!isset($_GET['gid']) || empty($_GET['gid']) ){
        require_once 'pages/general_404_noredirect.php';
        die;
    }
    
    if(!isset($_GET['act']) || empty($_GET['act']) ){
        require_once 'pages/general_404_noredirect.php';
        die;
    }
    require_once 'core/config.php';
    require_once 'core/connect.php';
    require_once 'core/authenticate.php';
    require_once ROOT_DIR.'core/authenticate.php';
    if(authenticate($mysqli,true)){
        //we logged in
    } else {
        die('please login before trying to access this page');
    }
    if(!isset($_SESSION['gallery']) ||  empty($_SESSION['gallery']) || $_SESSION['gallery']['gid']!=$_GET['gid']){
        require_once 'core/galleryInfo.php';
        getGallery($_GET['gid'],$mysqli);   
    }
    
    
    switch($_GET['act']){
        case "addfav":
            if(isset($_POST['favcat'])){
                require_once 'features/favorites.php';
                $ret = 0;
                if($_POST['favcat']=="favdel"){$ret = deleteFavorite($_GET['gid'],$mysqli);}
                else{$ret = addFavorite($mysqli);}
                
                if(empty($ret))return;
                echo "<pre>";
                print_r($ret);
            } else {
                echo getFavHTML();
            }
            break;
        default:
            require_once 'pages/general_404_noredirect.php';
    }
    $mysqli->close();
    
    
    
}
    
    
    
    



function getFavDivListHTML($num,$chosen){
    $retVal =  '<div style="height:25px; cursor:pointer">
	<div style="float:left"><input type="radio" name="favcat" value="'.$num.'" id="fav'.$num.'" style="position:relative; top:-1px" '.($chosen==$num ? ' checked="checked"' : '').'onclick="clicked_fav(this)" /></div>
	<div style="float:left; padding:1px 1px 0 4px; height:15px; width:15px; background-repeat:no-repeat; background-image:url(images/fav.png); background-position:0px '.((-1)-(19*$num)).'px" onclick="document.getElementById(\'fav'.$num.'\').click()"></div>
	<div style="float:left; padding-top:1px" onclick="document.getElementById(\'fav'.$num.'\').click()">Favorites '.$num.'</div>
	<div class="c"></div>
    </div>';
    return $retVal;
}
function getFavHTML(){
    $retVal =  '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
    <title>'.BASE_HREF.'</title>
    <link rel="stylesheet" type="text/css" href="css/g.css" />

    </head>
    <body style="text-align:center">
    <script type="text/javascript">
    function popUp(URL,w,h) { 
        window.open(URL,"_pu"+(Math.random()+"").replace(/0\./,""),"toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width="+w+",height="+h+",left="+((screen.width-w)/2)+",top="+((screen.height-h)/2));
        return false;
    }
    </script>

    <div class="stuffbox" style="width:584px; height:350px; margin:2px auto; text-align:left; padding:8px; font-size:9pt">
    <div style="width:100%; height:40px; font-size:11pt; font-weight:bold; margin:3px; text-align:center; overflow:hidden">'.$_SESSION["gallery"]["title"].'</div>
    <form id="galpop" action="" method="post">

    <p style="text-align:center">Please choose a color to file this favorite gallery under. You can also add a note to it if you wish.</p>
    <div style="margin:8px auto 5px">
    <div class="nosel" style="float:left; width:240px">';
    $cat = 0;
    $note = "";
    $includeDelete = false;
    if(isset($_SESSION['gallery']['favorites']['userFav']) && isset($_SESSION['gallery']['favorites']['userFav']['favCat'])){
        $cat = $_SESSION['gallery']['favorites']['userFav']['favCat'];
        $note = $_SESSION['gallery']['favorites']['userFav']['favNote'];		
        $includeDelete = true;
    }
    for($i = 0;$i<10;$i++){
        $retVal.= getFavDivListHTML($i,$cat);
    }
    if($includeDelete){
        $retVal.='<div style="height:25px; cursor:pointer">
        <div style="float:left"><input type="radio" name="favcat" value="favdel" id="favdel" style="position:relative; top:-1px" onclick="clicked_fav(this)" /></div>
        <div style="float:left; padding-left:5px" onclick="document.getElementById(\'favdel\').click()">Remove from Favorites</div>
        <div class="c"></div>
    </div>';
    }
    $retVal.= '
    </div>
    <div style="float:left; width:340px; text-align:center">
    <p style="font-weight:bold">Favorite Note (Max 200 Characters)</p>
    <textarea name="favnote" cols="50" rows="4" class="stdinput" style="width:325px; height:160px; font-size:9pt">'.$note.'</textarea><br />
    <!--<div style="margin:20px auto; text-align:center">
    1 / 10000 favorite note slots used. [<a href="https://ehwiki.org/wiki/Favorite" target="ehwiki">?</a>]<br />-->
    <input type="submit" name="apply" value="Add to Favorites" class="stdbtn" />
    <input type="hidden" name="update" value="1" />
    <script type="text/javascript">
    var selected = "fav'.$cat.'";
    function clicked_fav(fav) {
        console.debug("selected=" + selected + " selecting=" + fav.id);

        if( selected == fav.id ) {
            document.getElementById("galpop").submit();
        }
        else {
            selected = fav.id;
        }
    }
    </script>
    </div>
    </div>
    </div>

    </form>
    </div>

    </body>
    </html>
    ';
    return $retVal;
}
