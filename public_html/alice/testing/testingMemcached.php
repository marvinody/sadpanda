<?php 
error_reporting(E_ALL & ~E_NOTICE); 

$mc = new Memcached(); 
$mc->addServer("localhost", 11211); 

$mc->set("foo", array('test'=>'yo','test1'=>'double yo')); 
$mc->set("bar", "Memcached..."); 

$arr = array( 
    $mc->get("foo"), 
    $mc->get("bar") 
); 
echo "<pre>";
var_dump($arr); 
?> 
