<?php

define('IMAGE_UPLOAD_DIR','/var/www/sadpanda.moe/public_html/alice/testing');
define('IMAGE_STAGING_DIR','/var/www/sadpanda.moe/public_html/alice/testing');

define('THUMBNAIL_SUFFIX','_t');

define('RAW_SUFFIX','_r');

define('JPG_QUALITY',65);

/*CONFIG FOR ALLOWED DATA*/
$maxFileSize = 5*1024*1024;
$rawFilesize = 1.5*1024*1024;
$finfo = new finfo(FILEINFO_MIME_TYPE);
$possExts = array(
    'jpg' => 'image/jpeg',
    'png' => 'image/png',
//'gif' => 'image/gif', //leave this out for now cause idk how to optimize
);


$file = "marisa_nyc.jpg";
echo "<pre>";
print_r(process_image($file, 1280, $rawFilesize));

function process_image($fullFilePath, $maxWidth, $maxFilesize) {
    /*
    file must bethe path+filename. 
    Path should always be staged folder (defined in config)
    So one must move it (move_uploaded_file) if needed before calling this func
    if passed file (must be in staged folder) is (any of following)
        width > 1280
        filesize > maxfilesize
        not jpg
    then we process it and try to create an alternate copy
    
    we also always create a thumbnail to go with it. 
        thumbnail will be of the 'alternate' if exists
        
    return is array with different things
        hasRaw := if we processed it somehow
        thumbnail := name of staged thumbnail
        main := name of staged processed image
        raw := name of original if processed (staged)
    
    */
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    $mimeType = $finfo->file($fullFilePath);
    
    $sha256 = hash_file("sha256",$fullFilePath);
    
    $return = array(
        'hasRaw'=>0,
        'thumbnail'=>sprintf(
            '%s/%s%s',
            IMAGE_STAGING_DIR,
            $sha256,
            THUMBNAIL_SUFFIX
        ),
        'main'=>sprintf(
            '%s/%s',
            IMAGE_STAGING_DIR,
            $sha256
            
        ),
        'hash'=>$sha256,
        'filesize'=>filesize($fullFilePath),
    );
    
    
    list($width, $height) = getimagesize($fullFilePath);
    $return['width'] = $width;
    $return['height'] = $height;
    $filesize = $return['filesize'];
    if( $width > $maxWidth 
       || $filesize > $maxFilesize
       || $mimeType == 'image/png'){
        //create alternate in here
        $raw = sprintf(
            '%s/%s%s',
            IMAGE_STAGING_DIR,
            $sha256,
            RAW_SUFFIX
            
        );
        switch($mimeType){
            case "image/png":
                $src = imagecreatefrompng($fullFilePath);
            break;
            case "image/jpeg":
                $src = imagecreatefromjpeg($fullFilePath);

        }
        $newWidth = $maxWidth;
        $newHeight = round($newWidth / $width * $height);
        $dst = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
        imagejpeg($dst, $return['main'], JPG_QUALITY);
        copy($fullFilePath, $raw);
        $return['hasRaw'] = 1;
        $return['raw'] = $raw;
        $return['width'] = $newWidth;
        $return['height'] = $newHeight;
        
    } else {
        //everything should be fine, so we'll just copy the file to correct name
        copy($fullFilePath, $return['main']);
    }
    list($return['thumb_width'],$return['thumb_height']) 
        = makeJPEGThumbnail($return['main'],$return['thumbnail'],$return['width'],$return['height']);
    
    return $return;
    
    
    
}


//takes $in file, and $out location and returns new dimensions
//does not touch original
function makeJPEGThumbnail($in,$out,$width,$height){

    $myImage = imagecreatefromjpeg($in);
    $newWidth = 200;
    $newHeight = round($newWidth*$height/$width);

    $image_p = imagecreatetruecolor($newWidth, $newHeight);
    imagecopyresampled($image_p, $myImage, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
    imagejpeg($image_p, $out, 100);
    return array($newWidth,$newHeight);
}