<?php
function silly_captcha(){
    $class = 'captcha-text';
    $numbers = array(
        '1'=>['&#49;','&#x2460;','&#x2474;','&#x2488;','&#x24F5;'],
        '2'=>['&#50;','&#x2461;','&#x2475;','&#x2489;','&#x24F6;'],
        '3'=>['&#51;','&#x2462;','&#x2476;','&#x248A;','&#x24F7;'],
        '4'=>['&#52;','&#x2463;','&#x2477;','&#x248B;','&#x24F8;'],
        '5'=>['&#53;','&#x2464;','&#x2478;','&#x248C;','&#x24F9;'],
        '6'=>['&#54;','&#x2465;','&#x2479;','&#x248D;','&#x24FA;'],
        '7'=>['&#55;','&#x2466;','&#x247A;','&#x248E;','&#x24FB;'],
        '8'=>['&#56;','&#x2467;','&#x247B;','&#x248F;','&#x24FC;'],
        '9'=>['&#57;','&#x2468;','&#x247C;','&#x2490;','&#x24FD;'],
    );
    $num1 = mt_rand(1,9);
    $num1CharInd = mt_rand(0,count($numbers[$num1])-1);
    $num1Char = $numbers[$num1][$num1CharInd];
    $num1Span = "<span class='".(($num1CharInd==1)?$class:"")."'>".$num1Char."</span>";
    echo "<br>num1:".$num1Char;
    $num2 = mt_rand(1,9);
    $num2CharInd = mt_rand(0,count($numbers[$num2])-1);
    $num2Char = $numbers[$num2][$num2CharInd];
    $num2Span = "<span class='".(($num2CharInd==1)?$class:"")."'>".$num2Char."</span>";
    echo "<br>num2:".$num2Char."<br>";
    $operation = mt_rand(0,2);
    $str = "nothing";
    switch($operation){
        case 0:
            $str = $num1Span." + ".$num2Span;
            $result = $num1+$num2;
            break;
        case 1:
            $str = $num1Span." - ".$num2Span;
            $result = $num1-$num2;
            break;
        case 2:
            $str = $num1Span." * ".$num2Span;
            $result = $num1*$num2;
            break;
    }
    return array(
        'result'=>$result,
        'str'=>$str,
    );
    
}
echo "<head><style>span.captcha-text{font-size:1.5em;}</style></head>";
echo "<pre>";
print_r(silly_captcha());