<?php


define('IMAGE_UPLOAD_DIR','/var/www/sadpanda.moe/public_html/alice/testing');
define('IMAGE_STAGING_DIR','/var/www/sadpanda.moe/public_html/alice/testing');

define('THUMBNAIL_SUFFIX','_t');

define('RAW_SUFFIX','_r');

define('JPG_QUALITY',65);

define('MAX_SIZE_PER_ZIP',100*1024*1024);//100MB
define('MAX_FILES_PER_ZIP',100);

$zip_file = 'Marisa\'s trip to New York-min.zip';
$mysqli = null;
$gid = 1;
$nextPage = 0;
$nextActualPage = 0;
$finfo = new finfo(FILEINFO_MIME_TYPE);
$mime = $finfo->file($zip_file);
echo $mime;
//upload_zip($mysqli,$gid,$zip_file,$nextPage,$nextActualPage);

function test_chain(){
    echo "<pre>";
    print_r(glob("*"));
    try{
        throw new RuntimeException("yo");
    } catch (Exception $e){
        echo "before thrown\n";
        throw $e;
        echo "after thrown\n";
    } finally{
        echo "close stuff\n";
    }
    //output
    //before thrown close stuff
}
function test_extract($zipFile){
    $dst = sprintf("%s/%s",IMAGE_STAGING_DIR,'tmp');
    $dst = tempnam(IMAGE_STAGING_DIR,'tmp_zip_file_');
    $zip = new ZipArchive;
    if($zip->open($zipFile) !== true){
        throw new RuntimeException('Error opening zip archive');
    }
    echo "\ntmp:".$dst."\n";
    extract_single_file($zipFile,$zip->statIndex(2)['name'],$dst);
    echo "size:".filesize($dst)."\n";
    unlink($dst);
    
    $dst = tempnam(IMAGE_STAGING_DIR,'tmp_zip_file_');
    echo "\ntmp:".$dst."\n";
    echo extract_single_file($zipFile,$zip->statIndex(1)['name'],$dst) ? "success\n" : "failure\n";
    echo "size:".filesize($dst)."\n";
    
    unlink($dst);
}
function zip_file_is_folder($filename){
    return $filename[strlen($filename)-1]=='/';
}
function extract_single_file($zipFile,$wantedFile,$dst){
    return copy(sprintf("zip://%s#%s",$zipFile,$wantedFile),$dst);
}



function get_top_level_size($zipResource){
    $total = $zipResource->numFiles;
    $size = 0;
    for($i=0;$i<$total;$i++){
        $size += $zipResource->statIndex($i)['size'];
    }
    return $size;
}

function check_zip($zipResource){
    $files = $zipResource->numFiles;
    if($files > MAX_FILES_PER_ZIP){
        throw new RuntimeException('Too many files in zip. Max:'.MAX_FILES_PER_ZIP);
    }
    $size = get_top_level_size($zipResource);
    if($size > MAX_SIZE_PER_ZIP){
        throw new RuntimeException('Uncompressed zip file is too large. Max:'.MAX_SIZE_PER_ZIP);
    }
    
}


function upload_zip($mysqli,$gid,$stagedFile,&$nextPage,&$nextActualPage){
    
    
    $zip = new ZipArchive;
    if($zip->open($stagedFile) !== true){
        throw new RuntimeException('Error opening zip archive');
    }
    check_zip($zip);
    
    $totalFiles = $zip->numFiles;
    for($i=0;$i<$totalFiles;$i++){
        try{
            $tmp_file = tempnam(IMAGE_STAGING_DIR,'tmp_zip_file_');
            $cur_file_data = $zip->statIndex($i);
            if(zip_file_is_folder($cur_file_data['name'])){
                continue;
            }
            //now we know we have a file
            extract_single_file($stagedFile,$cur_file_data['name'],$tmp_file);
            
            $val = checkFile($tmp_file);
            if($val==0 || $val == 1){//is image type?
                upload_image($mysqli, $gid, $tmp_file, $cur_file_data['name'], $nextPage, $nextActualPage);
            }
            $nextPage++;
            $nextActualPage++;
            
        } catch (Exception $e){
            throw $e;
        } finally {
            unlink($tmp_file);
        }

        
    }

    
    
    
}

function upload_image($mysqli,$gid,$stagedFile,$name,$nextPage,$nextActualPage){
    //This is hardcoded in the htaccess file. So if you change this, change the other
    $SHORT_URL_LENGTH = 8;
    
    $rawFilesize = 1.0*1024*1024;//1meg
    $maxWidth = 1280;
    
    $info = process_image($stagedFile, $maxWidth, $rawFilesize);
    
    
    //and this is how we access it online. TODO make random because it's pretty
    //simple to rev. eng. and see I use sha256 for id
    //does that matter tho?
    //just means that same url for same hash image. Only matters if user uploads 2 different images with same hash
    //results in first image(?) being always displayed. Can fix later if needed
    $b64hash = base64_encode($info['hash']);

    //get shortURL that we use to locate it using web
    $shortID = substr($b64hash,0,$SHORT_URL_LENGTH);
    
    
    //just does regular insert with supplied info
    //insert_page_to_db($mysqli, $gid, $shortID, $name, $info, $nextPage, $nextActualPage);
    
    move_files_to_upload_dir($info);
}

function move_files_to_upload_dir($info){
    //get the final uploaded filenames
    $server_uploaded_file = sprintf(
        '%s/%s',
        IMAGE_UPLOAD_DIR,
        $info['hash']
    );
    //thumbnail filename
    $server_uploaded_thumbnail = sprintf(
        '%s/%s%s',
        IMAGE_UPLOAD_DIR,
        $info['hash'],
        THUMBNAIL_SUFFIX
    );
    //only use this if needed
    $server_uploaded_raw = sprintf(
        '%s/%s%s',
        IMAGE_UPLOAD_DIR,
        $info['hash'],
        RAW_SUFFIX
    );
    
    //now we move all the files. Main, thumbnail and then raw if we have
    if (!rename(
      $info['main'],
      $server_uploaded_file
    )) {
        throw new RuntimeException('Failed to move main image from stage',11);
    }
    if (!rename(
      $info['thumbnail'],
      $server_uploaded_thumbnail
    )) {
        throw new RuntimeException('Failed to move thumb image from stage',12);
    }
    if ($info['hasRaw'] && !rename(
      $info['raw'],
      $server_uploaded_raw
    )) {
        throw new RuntimeException('Failed to move raw image from stage',13);
    }
}

function checkFile($filename){
    /*CONFIG FOR ALLOWED DATA*/
    $maxFileSize = 5*1024*1024;
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    $possExts = array(
        'jpg' => 'image/jpeg',
        'png' => 'image/png',
        //'gif' => 'image/gif',
    );

    $filesize = filesize($filename);
    if( $filesize > $maxFileSize ){
        throw new RuntimeException('Exceeded SadPanda\'s filesize limit.',2);
    }
    $mime = $finfo->file($filename);
    $ext = array_search(
        $mime,
        $possExts,
        true
    );
    if (false === $ext) {
        throw new RuntimeException('Invalid file format.',4);
    }

    return 0;
    
}

function insert_page_to_db($mysqli, $gid, $shortID, $name, $info,$nextPage,$nextActualPage){
    //turn on error reporting because I had issues before and catching all now
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $query = "INSERT INTO pages 
    (gid,shortID,name,page,width,height,thumb_width,thumb_height,filesize,hash,hasRaw,actualPageOrder) VALUES 
    (?,  ?,      ?,   ?,   ?,    ?,     ?,          ?,           ?,       ?,  ?,     ?)";
    if(!$stmt = $mysqli->prepare($query)){
        throw new RuntimeException('Database error fu1. Inform Admin');
    }
    
    $kb_filesize = round($info['filesize']/1024);
    if(!$stmt->bind_param('issiiiiiisii',
                        $gid,$shortID,$name,
                          $nextPage,$info['width'],$info['height'],
                          $info['thumb_width'],$info['thumb_height'],
                          $kb_filesize,$info['hash'],
                          $info['hasRaw'],$nextActualPage)){
        throw new RuntimeException('Database error fu2. Inform Admin');
    }

    if(!$stmt->execute()){
        throw new RuntimeException('Database error fu4. Inform Admin');
    }
    $stmt->close();
}

function process_image($fullFilePath, $maxWidth, $maxFilesize) {
    /*
    file must bethe path+filename. 
    Path should always be staged folder (defined in config)
    So one must move it (move_uploaded_file) if needed before calling this func
    if passed file (must be in staged folder) is (any of following)
        width > 1280
        filesize > maxfilesize
        not jpg
    then we process it and try to create an alternate copy
    
    we also always create a thumbnail to go with it. 
        thumbnail will be of the 'alternate' if exists
        
    return is array with different things
        hasRaw := if we processed it somehow
        thumbnail := name of staged thumbnail
        main := name of staged processed image
        raw := name of original if processed (staged)
    
    */
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    $mimeType = $finfo->file($fullFilePath);
    
    $sha256 = hash_file("sha256",$fullFilePath);
    
    $return = array(
        'hasRaw'=>0,
        'thumbnail'=>sprintf(
            '%s/%s%s',
            IMAGE_STAGING_DIR,
            $sha256,
            THUMBNAIL_SUFFIX
        ),
        'main'=>sprintf(
            '%s/%s',
            IMAGE_STAGING_DIR,
            $sha256
            
        ),
        'hash'=>$sha256,
        'filesize'=>filesize($fullFilePath),
    );
    
    
    list($width, $height) = getimagesize($fullFilePath);
    $return['width'] = $width;
    $return['height'] = $height;
    $filesize = $return['filesize'];
    if( $width > $maxWidth 
       || $filesize > $maxFilesize
       || $mimeType == 'image/png'){
        //create alternate in here
        $raw = sprintf(
            '%s/%s%s',
            IMAGE_STAGING_DIR,
            $sha256,
            RAW_SUFFIX
            
        );
        switch($mimeType){
            case "image/png":
                $src = imagecreatefrompng($fullFilePath);
            break;
            case "image/jpeg":
                $src = imagecreatefromjpeg($fullFilePath);

        }
        $newWidth = $maxWidth;
        $newHeight = round($newWidth / $width * $height);
        $dst = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
        imagejpeg($dst, $return['main'], JPG_QUALITY);
        copy($fullFilePath, $raw);
        $return['hasRaw'] = 1;
        $return['raw'] = $raw;
        $return['width'] = $newWidth;
        $return['height'] = $newHeight;
        $return['filesize'] = filesize($return['main']);
        
    } else {
        //everything should be fine, so we'll just copy the file to correct name
        copy($fullFilePath, $return['main']);
    }
    list($return['thumb_width'],$return['thumb_height']) 
        = makeJPEGThumbnail($return['main'],$return['thumbnail'],$return['width'],$return['height']);
    
    return $return;
    
}


//takes $in file, and $out location and returns new dimensions
//does not touch original
function makeJPEGThumbnail($in,$out,$width,$height){

    $myImage = imagecreatefromjpeg($in);
    $newWidth = 200;
    $newHeight = round($newWidth*$height/$width);

    $image_p = imagecreatetruecolor($newWidth, $newHeight);
    imagecopyresampled($image_p, $myImage, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
    imagejpeg($image_p, $out, 100);
    return array($newWidth,$newHeight);
}
