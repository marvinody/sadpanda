<?php
if (isset($_POST['key'])){
    $key = "charlie";
    $SHORT_URL_LENGTH = 8;
    $maxFileSize = 4 * 1024*1024;
	$rawFileSize = 1.5*1024*1024;
	require_once 'core/config.php';
	require_once 'core/connect.php';
    try{
        if (!!empty($_POST['key'])){
            throw new RuntimeException("No Key Supplied");
        }    
        if ($_POST['key'] !== $key ){
            throw new RuntimeException("Wrong key entered");
        }
        if (!!empty($_POST['title'])){
            throw new RuntimeException("No Title Supplied");
        }
        if (!!empty($_POST['uploader'])){
            throw new RuntimeException("No Username Supplied");
        }
        if (!isset($_FILES['userfile'])){
            throw new RuntimeException("Try Uploading a file");
        }
        if (!isset($_POST['section'])){
            throw new RuntimeException("No Section Supplied");
        }
        
        
        
        $myFile = $_FILES['userfile'];
        $fileCount = count($myFile["name"]);
        $possExts = array(
                'jpg' => 'image/jpeg',
                'png' => 'image/png',
                //'gif' => 'image/gif',
        );
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $gid = 0;
        $id = 0;
        $section = $_POST['section'];
        $pages = [];
        for ($i = 0; $i < $fileCount; $i++) {
            #$myFile["name"][$i]
            #$myFile["tmp_name"][$i]
            switch ($myFile['error'][$i]) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }
			 $mime = $finfo->file($myFile['tmp_name'][$i]);
			$ext = array_search(
                $mime,
                $possExts,
                true
            );
            if (false === $ext) {
                throw new RuntimeException('Invalid file format.');
            }
			
            if ( filesize($myFile["tmp_name"][$i]) > $maxFileSize) {
                throw new RuntimeException('Exceeded filesize limit. Max:'.$maxFileSize.', and was given: '.filesize($myFile["tmp_name"][$i]));
            } elseif ( filesize($myFile["tmp_name"][$i]) > $rawFileSize || $mime==="image/png") {
				if(!isset($_SESSION['qualityReduced']) ){
					$_SESSION['qualityReduced'] = true;//flag to check for large files and convert
				}
			}
            
           
            
            
            $sha = sha1_file($myFile['tmp_name'][$i]);
			$md5 = md5_file($myFile['tmp_name'][$i]);
            $b64sha = base64_encode ($sha);
            if ($section == 0 && $i == 0){
				//outdated info.
                //pretty much will never collide but I add the random incase some use the same first page
				//and I only realized that after a good portion of the code was in place.
				//TODO PREVENT OVERWRITE OF GID
				$isOk = false;
				$halfL = $SHORT_URL_LENGTH/2;
				while(!$isOk){
					$gid = substr(hexdec(substr($sha,0,$halfL-1).mt_rand(pow(10,$halfL),pow(10,$halfL+1))),0,$SHORT_URL_LENGTH);
					$results = $mysqli->query("SELECT id from ".DB_TABLE_ALBUMS." where gid=".$gid);
					$isOk = $results->num_rows===0;
					
				}
				$_SESSION['gid'] = $gid;
				
            } else {
                if ($section != 0){
                    $gid = $_SESSION['gid'];
                }
            }
            $uploaded_file = sprintf('%s/%s,%s.%s',
                IMAGE_DIR,
                $gid,
                $b64sha,
                ".jpg"//$ext, //should never be .png or w/e since I check for png up there and convert to jpg below
            );
            $percent = 100;
            if(isset($_SESSION['qualityReduced']) && $_SESSION['qualityReduced']){
				//if large files detected, do something
				//upload raw to defined config location
				$rawUploadedFile = sprintf('%s/%s,%s.%s',
					LARGE_IMAGE_DIR,
					$gid,
					$b64sha,
					$ext
				);
				if (!move_uploaded_file(
					$myFile['tmp_name'][$i],
					$rawUploadedFile
				)) {
					throw new RuntimeException('Failed to move raw uploaded file.');
				}
				
				$percent = rescale($rawUploadedFile,$uploaded_file,$rawFileSize,$mime,90);//throws error if found
				
				
			} else {
				if (!move_uploaded_file(
					$myFile['tmp_name'][$i],
					$uploaded_file
				)) {
					throw new RuntimeException('Failed to move uploaded file.');
				}
			}
			$short_id = substr($b64sha,0,$SHORT_URL_LENGTH);
			
			
			
			$width = 0;
			$height = 0;
			$newWidth = 0;
			$newHeight = 0;
			if ($ext === "jpg"){
				list($width,$height) = getimagesize($uploaded_file);
				
				$thumbnail = sprintf('%s/%s,%s.%s',
					THUMBNAIL_DIR,
					$gid,
					$b64sha,
					$ext
				);
				
				//generate thumbnails for later
				$myImage = imagecreatefromjpeg($uploaded_file);
				$newWidth = 200;
				$newHeight = round(200*$height/$width);
				
				$image_p = imagecreatetruecolor($newWidth, $newHeight);
				imagecopyresampled($image_p, $myImage, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
				imagejpeg($image_p, $thumbnail, 100);
				
				
			}
			
			$pages[] = array(
				'shortID'=>$short_id,
				'page'=>($i+1),
				'name'=>$myFile['name'][$i],
				'percent'=>$percent,
				//'tempName'=>$myFile['tmp_name'][$i],
				'width'=>$width,
				'height'=>$height,
				'thumb_width'=>$newWidth,
				'thumb_height'=>$newHeight,
				'md5'=>$md5,
				'filesize'=>round(filesize($uploaded_file)/1024),
				
			);
                
		}
            
            
        
        $data = array(
            'gid'=>$gid,
            'pages'=>$pages,
            'uploader'=>$_POST['uploader'],
            'name'=>$_POST['title'],
            'cover_thumb'=>$pages[0]['shortID'],
			'rawFileSize'=>$rawFileSize/1024,
			'PNG2JPG'=>isset($_SESSION['qualityReduced']) ? true : false,
            
        );
        
        /*MYSQL PART*/
		/*TODO, FIX THE UPLOADER PART ONCE LOGIN STARTS WORKING*/
        mysqli_report(MYSQLI_REPORT_STRICT);
        if (!isset($_POST['id'])){
            $stmt=$mysqli->prepare("INSERT INTO ".DB_TABLE_ALBUMS."(name,gid,uploaderId,cover_thumb) VALUES (?,?,?,?)");
			$uploaderId = 1;
            if(!($stmt->bind_param('siis',$data['name'],$data['gid'],$uploaderId,$data['cover_thumb']))){
                echo (htmlspecialchars($stmt->error));
            }
            if (!($stmt->execute() )){
                echo (htmlspecialchars($stmt->error));
            }
            $id = $stmt->insert_id;
            $stmt->close();
        } else {
            $id = $_POST['id'];
        }
        
        
        $numPages = count($pages);
        $stmt=$mysqli->prepare("INSERT INTO ".DB_TABLE_PAGES."(gid,shortID,name,page,width,height,thumb_width,thumb_height,filesize,md5,hasRaw,actualPageOrder) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
        $shortID = 'a1';
        $page = 0;
        $name = '';
        $width = 0;
        $height = 0;
        $thumb_width = 0;
        $thumb_height = 0;
		$filesize = 0;
		$md5 = "";
		$hasRaw = $_SESSION['qualityReduced'] ? 1 : 0;
		//holy shit, this has gotten bad. it started with a few things and then kept building on...
        if(!($stmt->bind_param('issiiiiiisii',$data['gid'],$shortID,$name,$page,$width,$height,$thumb_width,$thumb_height,$filesize,$md5,$hasRaw,$page))){
            echo (htmlspecialchars($stmt->error));
        }
        for ($i = 0;$i<$numPages;$i++){
            $shortID = $data['pages'][$i]['shortID'];
            $name = $data['pages'][$i]['name'];
            $page = $data['pages'][$i]['page']+$section*10;
            $width = $data['pages'][$i]['width'];
            $height = $data['pages'][$i]['height'];
            $thumb_width = $data['pages'][$i]['thumb_width'];
            $thumb_height = $data['pages'][$i]['thumb_height'];
			$filesize = $data['pages'][$i]['filesize'];
            $md5 = $data['pages'][$i]['md5'];
            
            
                if (!($stmt->execute() )){
                echo (htmlspecialchars($stmt->error));
            }
        }
        
        header('Content-Type: application/json');
        print_r(json_encode($data));
        
    } catch (mysqli_sql_exception $e){
        if ($debug){
            echo $e->errorMessage();
        }
    } catch (RuntimeException $e) {
        echo $e->getMessage();
    }

    
    
}
function rescale($filePath,$desiredPath,$desiredFileSizeLimit,$mime,$percent){
	try{
		if($mime==='image/png'){
			$image = imagecreatefrompng($filePath);
			if($image===false){throw new Exception ('Error creating from png');}
			$bg = imagecreatetruecolor(imagesx($image), imagesy($image));
			imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
			imagealphablending($bg, TRUE);
			imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
			imagedestroy($image);
			//$quality = 50; // 0 = worst / smaller file, 100 = better / bigger file 
			imagejpeg($bg, $desiredPath, $percent);
			imagedestroy($bg);
		} elseif ($mime==='image/jpeg' || $meme==='image/jpg'){
			$image = imagecreatefromjpeg($filePath);
			if($image===false){throw new Exception ('Error creating from jpg');}
			imagejpeg($image,$desiredPath,$percent);
			imagedestroy($image);
		}
		if (filesize($desiredPath) < $desiredFileSizeLimit || $percent <= 50){
			return $percent;
		} else {
			rescale($filePath,$desiredPath,$desiredFileSizeLimit,$mime,$percent-10);
		}
		
		//get filePath ext. png/jpg
		//if png
		
		
		return $percent;
	} catch (Exception $e) {
		echo 'Caught PNG2JPG exception: ',  $e->getMessage(), "\n";
	}
}


?>