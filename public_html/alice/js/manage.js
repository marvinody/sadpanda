var divyoff = 0;

function saveScrollPos() {
    if (typeof(window.pageYOffset) == "number") {
        divyoff = window.pageYOffset
    } else {
        if (document.body && document.body.scrollTop) {
            divyoff = document.body.scrollTop
        } else {
            if (document.documentElement && document.documentElement.scrollTop) {
                divyoff = document.documentElement.scrollTop
            } else {
                divyoff = 0
            }
        }
    }
}

function restoreScrollPos() {
    if (divyoff != 0) {
        window.scrollTo(0, divyoff)
    }
}

function check_folder(a, b) {
    changecheck_folder(a, b, "checked")
}

function uncheck_folder(a, b) {
    changecheck_folder(a, b, "")
}

function changecheck_folder(state, fid, tostate) {
    rowset = eval("frow_" + state + "_" + fid);
    for (row in rowset) {
        var r = rowset[row];
        document.getElementById("f" + r).checked = tostate
    }
}

function array_pos(b, a) {
    for (i in a) {
        if (a[i] == b) {
            return i
        }
    }
    return -1
}

function unserialize_fidcookie(b) {
    var c = readCookie("fv" + b).split("-");
    do {
        var a = array_pos("", c);
        if (a >= 0) {
            c.splice(a, 1)
        }
    } while (a >= 0);
    return c
}

function serialize_fidcookie(b, a) {
    createCookie("fv" + b, a.join("-"), 365)
}
var ajaxreq = undefined;
var tempr = undefined;

function folder_load(c, d) {
    if (ajaxreq != undefined) {
        return
    }
    ajaxreq = new XMLHttpRequest();
    var b = {
        method: "managefolders",
        apiuid: apiuid,
        apikey: apikey,
        state: c,
        fid: d,
        ss: ss,
        sd: sd
    };
    api_call(ajaxreq, b, folder_update);
    var a = document.createElement("td");
    a.className = "gtc1";
    a.colSpan = "6";
    a.innerHTML = "Loading folder, please wait...";
    tempr = document.getElementById("gtable" + c).insertRow(document.getElementById("frow_" + c + "_" + d).rowIndex + 1);
    tempr.className = "gtr1";
    tempr.appendChild(a);
    restoreScrollPos()
}

function folder_update() {
    var l = api_response(ajaxreq);
    if (l != false) {
        if (l.error != undefined) {
            alert("Could not load folder: " + l.error)
        }
        if (l.state != undefined) {
            var k = 0;
            var j = document.getElementById("frow_" + l.state + "_" + l.fid).rowIndex;
            for (var h = 0; h < l.rows.length; h++) {
                var m = l.rows[h];
                var g = document.createElement("td");
                var f = document.createElement("td");
                var e = document.createElement("td");
                var d = document.createElement("td");
                var c = document.createElement("td");
                var b = document.createElement("td");
                g.className = "gtc1";
                f.className = "gtc2";
                e.className = "gtc3";
                d.className = "gtc4";
                c.className = "gtc5";
                b.className = "gtc6";
                g.innerHTML = m.c1;
                f.innerHTML = m.c2;
                e.innerHTML = m.c3;
                d.innerHTML = m.c4;
                c.innerHTML = m.c5;
                b.innerHTML = m.c6;
                var a = document.getElementById("gtable" + l.state).insertRow(++j);
                a.id = "gr" + m.gid;
                a.className = "gtr" + (++k % 2);
                a.appendChild(g);
                a.appendChild(f);
                a.appendChild(e);
                a.appendChild(d);
                a.appendChild(c);
                a.appendChild(b)
            }
            document.getElementById("gtable" + l.state).deleteRow(tempr.rowIndex);
            tempr = undefined;
            ajaxreq = undefined;
            restoreScrollPos();
            setTimeout("restoreScrollPos()", 10);
            setTimeout("restoreScrollPos()", 100)
        }
    }
}

function toggle_view(a, c, h) {
    if (ajaxreq != undefined) {
        return
    }
    saveScrollPos();
    var f = document.getElementById("ft_" + a + "_" + c);
    var b = f.innerHTML == "[+]";
    f.innerHTML = (b ? "[-]" : "[+]");
    if (b) {
        folder_load(a, c)
    } else {
        var j = document.getElementById("gtable" + a);
        var e = document.getElementById("frow_" + a + "_" + c).rowIndex + 1;
        while ((e < j.rows.length) && (j.rows[e].className != "gtr")) {
            j.deleteRow(e)
        }
    }
    document.getElementById("grs1_" + a + "_" + c).style.display = (b ? "" : "none");
    document.getElementById("grs2_" + a + "_" + c).style.display = (b ? "" : "none");
    var d = unserialize_fidcookie(a);
    var g = array_pos(c, d);
    if (b) {
        if (g >= 0) {
            d.splice(g, 1)
        }
    } else {
        if (g < 0) {
            d.splice(0, 0, c)
        }
    }
    serialize_fidcookie(a, d);
    if (h) {
        restoreScrollPos();
        setTimeout("restoreScrollPos()", 10);
        setTimeout("restoreScrollPos()", 100)
    }
}

function toggle_hidden_all(state) {
    var fids = eval("fidlist_" + state);
    for (i in fids) {
        if (document.getElementById("ft_" + state + "_" + fids[i]).innerHTML == "[-]") {
            toggle_view(state, fids[i], false)
        }
    }
}

function createCookie(c, d, e) {
    var b = new Date();
    b.setTime(b.getTime() + (e * 24 * 60 * 60 * 1000));
    var a = "; expires=" + b.toGMTString();
    document.cookie = c + "=" + d + a + "; path=/"
}

function readCookie(b) {
    var e = b + "=";
    var a = document.cookie.split(";");
    for (var d = 0; d < a.length; d++) {
        var f = a[d];
        while (f.charAt(0) == " ") {
            f = f.substring(1, f.length)
        }
        if (f.indexOf(e) == 0) {
            return f.substring(e.length, f.length)
        }
    }
    return ""
}
var current_dropbox = undefined;
var current_selbox = undefined;
var current_orgpage = undefined;
var dropbox_timeout = undefined;
/*
this is now defined inside the reorder page to allow to keep stuff around
var delete_pos = new Array();
var delete_count = 0;
*/
function show_dropbox(c) {
    if (current_orgpage == c) {
        return
    }
    hide_dropbox();
    current_orgpage = c;
    current_selbox = document.getElementById("pagesel_" + c);
    current_selbox.style.display = "none";
    var a = document.createElement("select");
    a.className = "stdinput";
    a.style.width = "70px";
    a.onchange = function() {
        hide_dropbox()
    };
    var e = pages - delete_count + (delete_pos[current_orgpage] != undefined ? 1 : 0);
    for (var d = 0; d <= e; d++) {
        option = document.createElement("option");
        option.text = d == 0 ? "Delete" : d;
        option.value = d;
        if (d == current_selbox.value) {
            option.selected = "selected"
        }
        try {
            a.add(option, null)
        } catch (b) {
            a.add(option)
        }
    }
    current_dropbox = document.getElementById("inspan_" + c).appendChild(a);
    current_dropbox.focus();
    current_dropbox.value = current_selbox.value
}

function hide_dropbox_delayed() {
    dropbox_timeout = setTimeout("hide_dropbox()", 200)
}

function hide_dropbox_cancel() {
    if (dropbox_timeout != undefined) {
        clearTimeout(dropbox_timeout)
    }
}

function hide_dropbox() {
    if (current_dropbox != undefined) {
        var a = parseInt(current_selbox.value);
        var c = parseInt(current_dropbox.value);
        current_selbox.value = current_dropbox.value;
        document.getElementById("inspan_" + current_orgpage).removeChild(current_dropbox);
        current_dropbox = undefined;
        current_selbox.style.display = "";
        current_selbox = undefined;
        if (a > 0 && c == 0) {
            delete_pos[current_orgpage] = delete_count++
        } else {
            if (a == 0 && c > 0) {
                var f = false;
                for (b in delete_pos) {
                    if (f) {
                        if(delete_pos[b]==undefined || delete_pos[b] == 0){
                            console.log("delete_pos["+b+"] is undefined. Trying to sub 1 = NaN");
                        } else {
                            delete_pos[b] -= 1;
                        }
                        recompute_divpos(b)
                    } else {
                        if (b == current_orgpage) {
                            f = true
                        }
                    }
                }
                delete_pos[current_orgpage] = undefined;
                --delete_count
            }
        }
        recompute_divpos(current_orgpage);
        for (var b = 1; b <= pages; b++) {
            if (b != current_orgpage) {
                var d = document.getElementById("pagesel_" + b);
                var e = parseInt(d.value);
                if (c == 0) {
                    if (a > 0 && e > a) {
                        d.value = e - 1;
                        recompute_divpos(b)
                    }
                } else {
                    if (a == 0) {
                        if (c > 0 && e >= c) {
                            d.value = e + 1;
                            recompute_divpos(b)
                        }
                    } else {
                        if ((a < c) && (e >= a) && (e <= c)) {
                            d.value = e - 1;
                            recompute_divpos(b)
                        } else {
                            if ((c < a) && (e >= c) && (e <= a)) {
                                d.value = e + 1;
                                recompute_divpos(b)
                            }
                        }
                    }
                }
            }
        }
        current_orgpage = undefined
    }
}

function recompute_divpos(c) {
    if (nofancy) {
        return
    }
    var b = document.getElementById("cell_" + c);
    var d = parseInt(document.getElementById("pagesel_" + c).value);
    var a = 0;
    if (d == 0) {
        d = (Math.ceil(pages / 5) * 5) + 1 + delete_pos[c];
        a = 70
    }
    b.style.left = 222 * ((d - 1) % 5) + "px";
    b.style.top = (355 * Math.floor((d - 1) / 5) + a) + "px"
};