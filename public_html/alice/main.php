<?php
//use htacces to pull key from url
//check database

require_once 'core/config.php';

$gid = !empty($_GET['gid']) ? $_GET['gid'] : 0;
$picture_dir = IMAGE_DIR+"/";
$thumb_dir = THUMBNAIL_DIR+"/";
$base_url = BASE_HREF."/";
$base_url_no_http = BASE_HREF_NO_HTTP."/";
#$base_url = "http://catbo.at/alice/";
#$base_url_no_http = "catbo.at/alice/";
/*
check for valid gid
check for hidden gid

check for what type of page viewer
$_GET['asdf']
    = g   -> single page viewer, regular
    = mpv -> multi page viewer
    = main-> main gallery page
*/
if( isset( $_GET['asdf'] ) ){
    //get gid
    
    if($gid > 0 && $_GET['asdf']!=="manage"){
        require_once 'core/connect.php';
        if (galleryExists($gid,$mysqli)){
            //gallery stuff
            switch($_GET['asdf']){
                case 'g':
                    require_once 'pages/gallery.php';
                    break;
                case 'mpv':
                    require_once 'pages/mpv.php';
                    break;
                    
                case 'raw':
                    die("Raw image getter not yet implemented");
                case 's':
                    require_once 'pages/spv.php';
                    #die('single page viewer not yet implemented');
                    break;
                default:
                    die('Shouldn\'t be here');
            }
            
        } else {
            require_once 'pages/gallery_404.php';
        }
    } else {
		switch($_GET['asdf']){
        case 'home':
				    die("Home not done yet, sorry");
            break;
        case 'favorites':
				    die("Favorites list not done yet, sorry");
				    break;
        case 'manage':
				    require_once 'pages/manage/manage-main.php';
            break;
			case 'tag':
          die("Tag searching not done yet, sorry");
				  break;
      case 'login':
          require_once 'pages/login.php';
          break;
      case 'logout':
          require_once 'pages/logout.php';
          break;
      case 'tos':
          require_once 'pages/tos.php';
          break;
      
			default:
        error_log("404: ".$_GET['asdf']);
				require_once 'pages/general_404.php';
		}
		
        
    }
    
} else {
    die('how did you get here?');
}
function galleryExists($gid,$mysqli){
	//$query = "SELECT id FROM albums WHERE gid=? AND hidden=0 AND published=1 AND expunged=0";
    //expunged just means unsearchable. But we still want to show it
    //published can be checked against logged in user to see if owner
    $query = "SELECT id,uploaderId, published FROM albums WHERE gid=? AND hidden=0";
	$stmt=$mysqli->prepare($query);
	$stmt->bind_param("i", $gid);
    $stmt->execute();
    $stmt->bind_result($id,$uploaderId,$published);

    $stmt->fetch();
    $stmt->close();
    if(empty($id)){
        return false;
    }
    if($published){
       return true; 
    }
    if(isset($_SESSION['user']) && isset($_SESSION['user']['id'])){
        return $_SESSION['user']['id']==$uploaderId;
    }

}

?>